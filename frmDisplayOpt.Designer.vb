<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDisplayOpt
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents optFullScreen As System.Windows.Forms.RadioButton
	Public WithEvents optTriple As System.Windows.Forms.RadioButton
	Public WithEvents optDouble As System.Windows.Forms.RadioButton
	Public WithEvents optDoubleHeight As System.Windows.Forms.RadioButton
	Public WithEvents optDoubleWidth As System.Windows.Forms.RadioButton
	Public WithEvents optNormal As System.Windows.Forms.RadioButton
	Public WithEvents fraDisplaySize As System.Windows.Forms.GroupBox
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDisplayOpt))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdOK = New System.Windows.Forms.Button
		Me.fraDisplaySize = New System.Windows.Forms.GroupBox
		Me.optFullScreen = New System.Windows.Forms.RadioButton
		Me.optTriple = New System.Windows.Forms.RadioButton
		Me.optDouble = New System.Windows.Forms.RadioButton
		Me.optDoubleHeight = New System.Windows.Forms.RadioButton
		Me.optDoubleWidth = New System.Windows.Forms.RadioButton
		Me.optNormal = New System.Windows.Forms.RadioButton
		Me.fraDisplaySize.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Display Options"
		Me.ClientSize = New System.Drawing.Size(272, 153)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDisplayOpt"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(192, 44)
		Me.cmdCancel.TabIndex = 7
		Me.cmdCancel.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(73, 25)
		Me.cmdOK.Location = New System.Drawing.Point(192, 11)
		Me.cmdOK.TabIndex = 6
		Me.cmdOK.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.fraDisplaySize.Text = "&Display Size"
		Me.fraDisplaySize.Size = New System.Drawing.Size(173, 143)
		Me.fraDisplaySize.Location = New System.Drawing.Point(8, 4)
		Me.fraDisplaySize.TabIndex = 0
		Me.fraDisplaySize.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.fraDisplaySize.BackColor = System.Drawing.SystemColors.Control
		Me.fraDisplaySize.Enabled = True
		Me.fraDisplaySize.ForeColor = System.Drawing.SystemColors.ControlText
		Me.fraDisplaySize.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.fraDisplaySize.Visible = True
		Me.fraDisplaySize.Padding = New System.Windows.Forms.Padding(0)
		Me.fraDisplaySize.Name = "fraDisplaySize"
		Me.optFullScreen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optFullScreen.Text = "Full screen (auto size)"
		Me.optFullScreen.Size = New System.Drawing.Size(153, 19)
		Me.optFullScreen.Location = New System.Drawing.Point(12, 115)
		Me.optFullScreen.TabIndex = 8
		Me.optFullScreen.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optFullScreen.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optFullScreen.BackColor = System.Drawing.SystemColors.Control
		Me.optFullScreen.CausesValidation = True
		Me.optFullScreen.Enabled = True
		Me.optFullScreen.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optFullScreen.Cursor = System.Windows.Forms.Cursors.Default
		Me.optFullScreen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optFullScreen.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optFullScreen.TabStop = True
		Me.optFullScreen.Checked = False
		Me.optFullScreen.Visible = True
		Me.optFullScreen.Name = "optFullScreen"
		Me.optTriple.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optTriple.Text = "&Triple size (768 x 576)"
		Me.optTriple.Size = New System.Drawing.Size(153, 17)
		Me.optTriple.Location = New System.Drawing.Point(12, 96)
		Me.optTriple.TabIndex = 5
		Me.optTriple.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optTriple.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optTriple.BackColor = System.Drawing.SystemColors.Control
		Me.optTriple.CausesValidation = True
		Me.optTriple.Enabled = True
		Me.optTriple.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optTriple.Cursor = System.Windows.Forms.Cursors.Default
		Me.optTriple.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optTriple.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optTriple.TabStop = True
		Me.optTriple.Checked = False
		Me.optTriple.Visible = True
		Me.optTriple.Name = "optTriple"
		Me.optDouble.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optDouble.Text = "&Double size (512 x 384)"
		Me.optDouble.Size = New System.Drawing.Size(145, 17)
		Me.optDouble.Location = New System.Drawing.Point(12, 77)
		Me.optDouble.TabIndex = 4
		Me.optDouble.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optDouble.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optDouble.BackColor = System.Drawing.SystemColors.Control
		Me.optDouble.CausesValidation = True
		Me.optDouble.Enabled = True
		Me.optDouble.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optDouble.Cursor = System.Windows.Forms.Cursors.Default
		Me.optDouble.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optDouble.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optDouble.TabStop = True
		Me.optDouble.Checked = False
		Me.optDouble.Visible = True
		Me.optDouble.Name = "optDouble"
		Me.optDoubleHeight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optDoubleHeight.Text = "Double &height (256 x 384)"
		Me.optDoubleHeight.Size = New System.Drawing.Size(153, 17)
		Me.optDoubleHeight.Location = New System.Drawing.Point(12, 58)
		Me.optDoubleHeight.TabIndex = 3
		Me.optDoubleHeight.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optDoubleHeight.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optDoubleHeight.BackColor = System.Drawing.SystemColors.Control
		Me.optDoubleHeight.CausesValidation = True
		Me.optDoubleHeight.Enabled = True
		Me.optDoubleHeight.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optDoubleHeight.Cursor = System.Windows.Forms.Cursors.Default
		Me.optDoubleHeight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optDoubleHeight.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optDoubleHeight.TabStop = True
		Me.optDoubleHeight.Checked = False
		Me.optDoubleHeight.Visible = True
		Me.optDoubleHeight.Name = "optDoubleHeight"
		Me.optDoubleWidth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optDoubleWidth.Text = "Double &width (512 x 192)"
		Me.optDoubleWidth.Size = New System.Drawing.Size(149, 17)
		Me.optDoubleWidth.Location = New System.Drawing.Point(12, 39)
		Me.optDoubleWidth.TabIndex = 2
		Me.optDoubleWidth.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optDoubleWidth.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optDoubleWidth.BackColor = System.Drawing.SystemColors.Control
		Me.optDoubleWidth.CausesValidation = True
		Me.optDoubleWidth.Enabled = True
		Me.optDoubleWidth.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optDoubleWidth.Cursor = System.Windows.Forms.Cursors.Default
		Me.optDoubleWidth.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optDoubleWidth.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optDoubleWidth.TabStop = True
		Me.optDoubleWidth.Checked = False
		Me.optDoubleWidth.Visible = True
		Me.optDoubleWidth.Name = "optDoubleWidth"
		Me.optNormal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optNormal.Text = "&Normal size (256 x 192)"
		Me.optNormal.Size = New System.Drawing.Size(149, 17)
		Me.optNormal.Location = New System.Drawing.Point(12, 20)
		Me.optNormal.TabIndex = 1
		Me.optNormal.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optNormal.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optNormal.BackColor = System.Drawing.SystemColors.Control
		Me.optNormal.CausesValidation = True
		Me.optNormal.Enabled = True
		Me.optNormal.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optNormal.Cursor = System.Windows.Forms.Cursors.Default
		Me.optNormal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optNormal.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optNormal.TabStop = True
		Me.optNormal.Checked = False
		Me.optNormal.Visible = True
		Me.optNormal.Name = "optNormal"
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(fraDisplaySize)
		Me.fraDisplaySize.Controls.Add(optFullScreen)
		Me.fraDisplaySize.Controls.Add(optTriple)
		Me.fraDisplaySize.Controls.Add(optDouble)
		Me.fraDisplaySize.Controls.Add(optDoubleHeight)
		Me.fraDisplaySize.Controls.Add(optDoubleWidth)
		Me.fraDisplaySize.Controls.Add(optNormal)
		Me.fraDisplaySize.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class