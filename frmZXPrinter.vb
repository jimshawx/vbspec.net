Option Strict Off
Option Explicit On
Friend Class frmZXPrinter
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmZXPrinter.frm within vbSpec.vbp
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2002 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	Private iWidth As Integer
	
	
	Private Sub InitZXPrinterBitmap()
		bmiZXPrinter.bmiHeader.biSize = Len(bmiZXPrinter.bmiHeader)
		bmiZXPrinter.bmiHeader.biWidth = 256
		bmiZXPrinter.bmiHeader.biHeight = 1152
		bmiZXPrinter.bmiHeader.biPlanes = 1
		bmiZXPrinter.bmiHeader.biBitCount = 1
		bmiZXPrinter.bmiHeader.biCompression = BI_RGB
		bmiZXPrinter.bmiHeader.biSizeImage = 0
		bmiZXPrinter.bmiHeader.biXPelsPerMeter = 200
		bmiZXPrinter.bmiHeader.biYPelsPerMeter = 200
		bmiZXPrinter.bmiHeader.biClrUsed = 2
		bmiZXPrinter.bmiHeader.biClrImportant = 2
		
		If optZX.Checked = True Then
			bmiZXPrinter.bmiColors(0).rgbRed = 192
			bmiZXPrinter.bmiColors(0).rgbGreen = 192
			bmiZXPrinter.bmiColors(0).rgbBlue = 192
			bmiZXPrinter.bmiColors(1).rgbRed = 0
			bmiZXPrinter.bmiColors(1).rgbGreen = 0
			bmiZXPrinter.bmiColors(1).rgbBlue = 0
		Else
			bmiZXPrinter.bmiColors(0).rgbRed = 255
			bmiZXPrinter.bmiColors(0).rgbGreen = 255
			bmiZXPrinter.bmiColors(0).rgbBlue = 255
			bmiZXPrinter.bmiColors(1).rgbRed = 64
			bmiZXPrinter.bmiColors(1).rgbGreen = 64
			bmiZXPrinter.bmiColors(1).rgbBlue = 192
		End If
		
		ReDim gcZXPrinterBits(36864) ' // 1152 * 32
		glZXPrinterBMPHeight = 1152
	End Sub
	
	
	Private Sub SaveMonoBitmap(ByRef sFile As String)
		Dim X, lSize, h, l, d As Integer
		Dim b() As Byte
		
		h = FreeFile
		FileOpen(h, sFile, OpenMode.Binary, OpenAccess.Write)
		
		lSize = 14 + Len(bmiZXPrinter.bmiHeader) + 8 + lZXPrinterY * 32
		
		' // BITMAPFILEHEADER - 14
		
		bmiZXPrinter.bmiHeader.biHeight = lZXPrinterY
		bmiZXPrinter.bmiHeader.biSizeImage = lZXPrinterY * 32
		
		' // Flip the stored image as RGB bitmaps are stored bottom-left to top-right
		d = lZXPrinterY - 1
		ReDim b(lZXPrinterY * 32)
		For l = 0 To lZXPrinterY - 1
			For X = 0 To 31
				b(X + l * 32) = gcZXPrinterBits(X + d * 32)
			Next X
			d = d - 1
		Next l
		
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, "BM") ' // bitmap ident
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, lSize) ' // size
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, CInt(0))
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, CInt(62))
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiHeader)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(0).rgbBlue)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(0).rgbGreen)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(0).rgbRed)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(0).rgbReserved)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(1).rgbBlue)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(1).rgbGreen)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(1).rgbRed)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, bmiZXPrinter.bmiColors(1).rgbReserved)
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(h, b)
		FileClose(h)
	End Sub
	
	Private Sub cmdClear_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdClear.Click
		lZXPrinterY = 0
		vs.Minimum = 0
		vs.Maximum = (0 + vs.LargeChange - 1)
		'UPGRADE_ISSUE: PictureBox method picView.Cls was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'picView.Cls()
		InitZXPrinterBitmap()
	End Sub
	
	Private Sub cmdFF_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles cmdFF.MouseDown
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		tmrFormFeed.Interval = 50
		tmrFormFeed.Enabled = True
	End Sub
	
	
	Private Sub cmdFF_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles cmdFF.MouseUp
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		tmrFormFeed.Enabled = False
	End Sub
	
	
	Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click
		err.Clear()

		'On Error Resume Next

		'dlgSaveOpen.DefaultExt = "bmp"
		'dlgSaveSave.DefaultExt = "bmp"
		'dlgSaveOpen.FileName = ""
		'dlgSaveSave.FileName = ""
		''UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'dlgSaveOpen.CheckPathExists = True
		'dlgSaveSave.CheckPathExists = True
		''UPGRADE_WARNING: MSComDlg.CommonDialog property dlgSave.Flags was upgraded to dlgSaveSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'dlgSaveSave.OverwritePrompt = True
		''UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		''UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgSave.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
		''UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		''UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgSave.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
		''UPGRADE_WARNING: MSComDlg.CommonDialog property dlgSave.Flags was upgraded to dlgSaveOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		''UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'dlgSaveOpen.ShowReadOnly = False
		''UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		''UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgSave.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgSave.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
		''UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'dlgSave.CancelError = True
		'dlgSaveSave.ShowDialog()
		'dlgSaveOpen.FileName = dlgSaveSave.FileName
		''UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'If Err.Number = DialogResult.Cancel Then
		'	Exit Sub
		'End If

		'If dlgSaveOpen.FileName <> "" Then
		'	SaveMonoBitmap((dlgSaveOpen.FileName))

		'	cmdClear_Click(cmdClear, New System.EventArgs())
		'End If
	End Sub
	
	Private Sub frmZXPrinter_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim y, X, h As Integer
		
		InitZXPrinterBitmap()
		iWidth = Me.Width

		X = Val(GetSetting("Grok", "vbSpec", "ZXPrnWndX", "-1"))
		y = Val(GetSetting("Grok", "vbSpec", "ZXPrnWndY", "-1"))
		h = Val(GetSetting("Grok", "vbSpec", "ZXPrnWndHeight", "-1"))

		If X >= 0 And X <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - 16 Then
			Me.Left = X
		End If
		If y >= 0 And y <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - 16 Then
			Me.Top = y
		End If
		If h >= 0 And h <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - Me.Top - 16 Then
			Me.Height = h
		End If
	End Sub
	
	'UPGRADE_WARNING: Event frmZXPrinter.Resize may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub frmZXPrinter_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
		Dim l As Integer

		Me.Width = iWidth

		l = Me.ClientRectangle.Height - picView.Top - picLogo.Height
		
		If l > 8 Then
			picView.Height = l
		Else
			picView.Height = 8
		End If
		
		vs.Height = picView.Height
		'UPGRADE_ISSUE: PictureBox method picView.Cls was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'picView.Cls()
		vs_Change(0)
	End Sub
	
	
	Private Sub frmZXPrinter_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		SaveSetting("Grok", "vbSpec", "ZXPrnWndX", CStr(Me.Left))
		SaveSetting("Grok", "vbSpec", "ZXPrnWndY", CStr(Me.Top))
		SaveSetting("Grok", "vbSpec", "ZXPrnWndHeight", CStr(Me.Height))

		frmMainWnd.mnuOptions(5).Checked = False
	End Sub
	
	'UPGRADE_WARNING: Event optAlphacom.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optAlphacom_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optAlphacom.CheckedChanged
		If eventSender.Checked Then
			imgAlpha.Visible = True
			imgSinclair.Visible = False

			picView.BackColor = System.Drawing.ColorTranslator.FromOle(RGB(255, 255, 255))
			bmiZXPrinter.Initialize()
			bmiZXPrinter.bmiColors(0).rgbRed = 255
			bmiZXPrinter.bmiColors(0).rgbGreen = 255
			bmiZXPrinter.bmiColors(0).rgbBlue = 255
			bmiZXPrinter.bmiColors(0).rgbReserved = 0
			bmiZXPrinter.bmiColors(1).rgbRed = 64
			bmiZXPrinter.bmiColors(1).rgbGreen = 64
			bmiZXPrinter.bmiColors(1).rgbBlue = 192
			bmiZXPrinter.bmiColors(1).rgbReserved = 0
			vs_Change(0)
		End If
	End Sub
	
	'UPGRADE_WARNING: Event optZX.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optZX_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optZX.CheckedChanged
		If eventSender.Checked Then
			imgSinclair.Visible = True
			imgAlpha.Visible = False

			picView.BackColor = System.Drawing.ColorTranslator.FromOle(RGB(192, 192, 192))
			bmiZXPrinter.Initialize()
			bmiZXPrinter.bmiColors(0).rgbRed = 192
			bmiZXPrinter.bmiColors(0).rgbGreen = 192
			bmiZXPrinter.bmiColors(0).rgbBlue = 192
			bmiZXPrinter.bmiColors(0).rgbReserved = 0
			bmiZXPrinter.bmiColors(1).rgbRed = 0
			bmiZXPrinter.bmiColors(1).rgbGreen = 0
			bmiZXPrinter.bmiColors(1).rgbBlue = 0
			bmiZXPrinter.bmiColors(1).rgbReserved = 0
			vs_Change(0)
		End If
	End Sub
	
	Private Sub picView_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles picView.Resize
		If lZXPrinterY > Me.picView.Height Then
			Me.vs.Minimum = Me.picView.Height \ 8
			Me.vs.Maximum = (lZXPrinterY \ 8 + Me.vs.LargeChange - 1)
			Me.vs.Value = lZXPrinterY \ 8
		Else
			Me.vs.Minimum = 0
			Me.vs.Maximum = (0 + Me.vs.LargeChange - 1)
		End If
	End Sub
	
	
	Private Sub tmrFormFeed_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrFormFeed.Tick
		lZXPrinterEncoder = 0
		lZXPrinterX = 0
		lZXPrinterY = lZXPrinterY + 1
		If lZXPrinterY >= glZXPrinterBMPHeight Then
			glZXPrinterBMPHeight = lZXPrinterY + 32
			ReDim Preserve gcZXPrinterBits(glZXPrinterBMPHeight * 32)
			bmiZXPrinter.bmiHeader.biHeight = glZXPrinterBMPHeight
		End If
		
		If Me.picView.Height > lZXPrinterY Then
			'UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'StretchDIBitsMono(picView.hdc, 0, picView.Height, 256, -lZXPrinterY - 1, 0, 0, 256, lZXPrinterY + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
		Else
			'UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'StretchDIBitsMono(picView.hdc, 0, picView.Height, 256, -picView.Height - 1, 0, lZXPrinterY - picView.Height, 256, picView.Height + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
		End If
		picView.Refresh()
		
		' // Set up the scroll bar properties for the visible display
		' // to allow scrolling back over the material printed so far
		If lZXPrinterY > Me.picView.Height Then
			Me.vs.Minimum = picView.Height \ 8
			Me.vs.Maximum = (lZXPrinterY \ 8 + Me.vs.LargeChange - 1)
			Me.vs.Value = lZXPrinterY \ 8
		Else
			Me.vs.Minimum = 0
			Me.vs.Maximum = (0 + Me.vs.LargeChange - 1)
		End If
	End Sub
	
	'UPGRADE_NOTE: vs.Change was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
	'UPGRADE_WARNING: VScrollBar event vs.Change has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub vs_Change(ByVal newScrollValue As Integer)
		If Me.picView.Height > lZXPrinterY Then
			'UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'StretchDIBitsMono(Me.picView.hdc, 0, Me.picView.Height, 256, -lZXPrinterY - 1, 0, 0, 256, lZXPrinterY + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
		Else
			'UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'StretchDIBitsMono(Me.picView.hdc, 0, Me.picView.Height, 256, -Me.picView.Height - 1, 0, newScrollValue * 8 - Me.picView.Height, 256, Me.picView.Height + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
		End If
		
		'    If frmZXPrinter.picView.Height > lZXPrinterY Then
		'        BitBlt frmZXPrinter.picView.hdc, 0, frmZXPrinter.picView.Height - lZXPrinterY, 256, frmZXPrinter.picView.Height, frmZXPrinter.picPrn.hdc, 0, 0, SRCCOPY
		'    Else
		'        BitBlt frmZXPrinter.picView.hdc, 0, 0, 256, frmZXPrinter.picView.Height, frmZXPrinter.picPrn.hdc, 0, vs.Value * 8, SRCCOPY
		'    End If
		
		Me.picView.Refresh()
	End Sub
	
	'UPGRADE_NOTE: vs.Scroll was changed from an event to a procedure. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="4E2DC008-5EDA-4547-8317-C9316952674F"'
	Private Sub vs_Scroll_Renamed(ByVal newScrollValue As Integer)
		vs_Change(0)
	End Sub
	Private Sub vs_Scroll(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.ScrollEventArgs) Handles vs.Scroll
		Select Case eventArgs.type
			Case System.Windows.Forms.ScrollEventType.ThumbTrack
				vs_Scroll_Renamed(eventArgs.newValue)
			Case System.Windows.Forms.ScrollEventType.EndScroll
				vs_Change(eventArgs.newValue)
		End Select
	End Sub
End Class