<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLoadBinary
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cboBase As System.Windows.Forms.ComboBox
	Public WithEvents txtAddr As System.Windows.Forms.TextBox
	Public WithEvents cmdOpen As System.Windows.Forms.Button
	Public WithEvents txtFile As System.Windows.Forms.TextBox
	Public dlgCommonOpen As System.Windows.Forms.OpenFileDialog
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLoadBinary))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdOK = New System.Windows.Forms.Button
		Me.cboBase = New System.Windows.Forms.ComboBox
		Me.txtAddr = New System.Windows.Forms.TextBox
		Me.cmdOpen = New System.Windows.Forms.Button
		Me.txtFile = New System.Windows.Forms.TextBox
		Me.dlgCommonOpen = New System.Windows.Forms.OpenFileDialog
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Load Binary Data"
		Me.ClientSize = New System.Drawing.Size(375, 94)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLoadBinary"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(288, 64)
		Me.cmdCancel.TabIndex = 1
		Me.cmdCancel.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(81, 25)
		Me.cmdOK.Location = New System.Drawing.Point(200, 64)
		Me.cmdOK.TabIndex = 0
		Me.cmdOK.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.cboBase.Size = New System.Drawing.Size(65, 21)
		Me.cboBase.Location = New System.Drawing.Point(168, 32)
		Me.cboBase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboBase.TabIndex = 7
		Me.cboBase.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboBase.BackColor = System.Drawing.SystemColors.Window
		Me.cboBase.CausesValidation = True
		Me.cboBase.Enabled = True
		Me.cboBase.ForeColor = System.Drawing.SystemColors.WindowText
		Me.cboBase.IntegralHeight = True
		Me.cboBase.Cursor = System.Windows.Forms.Cursors.Default
		Me.cboBase.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cboBase.Sorted = False
		Me.cboBase.TabStop = True
		Me.cboBase.Visible = True
		Me.cboBase.Name = "cboBase"
		Me.txtAddr.AutoSize = False
		Me.txtAddr.Size = New System.Drawing.Size(73, 21)
		Me.txtAddr.Location = New System.Drawing.Point(92, 32)
		Me.txtAddr.TabIndex = 6
		Me.txtAddr.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtAddr.AcceptsReturn = True
		Me.txtAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtAddr.BackColor = System.Drawing.SystemColors.Window
		Me.txtAddr.CausesValidation = True
		Me.txtAddr.Enabled = True
		Me.txtAddr.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAddr.HideSelection = True
		Me.txtAddr.ReadOnly = False
		Me.txtAddr.Maxlength = 0
		Me.txtAddr.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAddr.MultiLine = False
		Me.txtAddr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAddr.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAddr.TabStop = True
		Me.txtAddr.Visible = True
		Me.txtAddr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtAddr.Name = "txtAddr"
		Me.cmdOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOpen.Text = "..."
		Me.cmdOpen.Size = New System.Drawing.Size(25, 21)
		Me.cmdOpen.Location = New System.Drawing.Point(344, 4)
		Me.cmdOpen.TabIndex = 4
		Me.cmdOpen.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOpen.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOpen.CausesValidation = True
		Me.cmdOpen.Enabled = True
		Me.cmdOpen.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOpen.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOpen.TabStop = True
		Me.cmdOpen.Name = "cmdOpen"
		Me.txtFile.AutoSize = False
		Me.txtFile.Size = New System.Drawing.Size(249, 21)
		Me.txtFile.Location = New System.Drawing.Point(92, 4)
		Me.txtFile.TabIndex = 3
		Me.txtFile.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFile.AcceptsReturn = True
		Me.txtFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFile.BackColor = System.Drawing.SystemColors.Window
		Me.txtFile.CausesValidation = True
		Me.txtFile.Enabled = True
		Me.txtFile.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFile.HideSelection = True
		Me.txtFile.ReadOnly = False
		Me.txtFile.Maxlength = 0
		Me.txtFile.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFile.MultiLine = False
		Me.txtFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFile.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFile.TabStop = True
		Me.txtFile.Visible = True
		Me.txtFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFile.Name = "txtFile"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label2.Text = "Memory &Address:"
		Me.Label2.Size = New System.Drawing.Size(81, 13)
		Me.Label2.Location = New System.Drawing.Point(8, 36)
		Me.Label2.TabIndex = 5
		Me.Label2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label1.Text = "&Filename:"
		Me.Label1.Size = New System.Drawing.Size(53, 17)
		Me.Label1.Location = New System.Drawing.Point(36, 8)
		Me.Label1.TabIndex = 2
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(cboBase)
		Me.Controls.Add(txtAddr)
		Me.Controls.Add(cmdOpen)
		Me.Controls.Add(txtFile)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class