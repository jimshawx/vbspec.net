Option Strict Off
Option Explicit On
Module modTAP
	' /*******************************************************************************
	'   modTAP.bas within vbSpec.vbp
	'
	'   Handles loading of ".TAP" files (Spectrum tape images)
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)2001-2002 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	Private m_lChecksum As Integer
	
	Public Sub CloseTAPFile()
		If ghTAPFile > 0 Then
			FileClose(ghTAPFile)
			ghTAPFile = 0
		End If
	End Sub
	
	Public Function SaveTAP(ByRef lID As Integer, ByRef lStart As Integer, ByRef lLength As Integer) As Boolean
		Dim n, lChecksum As Integer
		
		On Error Resume Next
		' // Move to end of existing TAP data, if there is any
		If LOF(ghTAPFile) > 0 Then Seek(ghTAPFile, LOF(ghTAPFile) + 1)
		Debug.Print(lID & "  " & lStart & "  " & lLength)
		
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(ghTAPFile, CShort(lLength + 2))
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(ghTAPFile, CByte(lID))
		lChecksum = lID
		For n = lStart To lStart + lLength - 1
			'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FilePut(ghTAPFile, gRAMPage(glPageAt(glMemAddrDiv16384(n)), n And 16383))
			lChecksum = lChecksum Xor gRAMPage(glPageAt(glMemAddrDiv16384(n)), n And 16383)
		Next n
		'UPGRADE_WARNING: Put was upgraded to FilePut and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FilePut(ghTAPFile, CByte(lChecksum))
		
		SaveTAP = True
	End Function
	
	Public Function LoadTAP(ByRef lID As Integer, ByRef lStart As Integer, ByRef lLength As Integer) As Boolean
		Dim lBlockID, lBlockLen, lBlockChecksum As Integer
		Dim s(1) As Byte
		
		On Error Resume Next
		
		If gsTAPFileName = "" Then
			LoadTAP = False
		Else
			If Seek(ghTAPFile) > LOF(ghTAPFile) Then Seek(ghTAPFile, 1)
			If EOF(ghTAPFile) Then Seek(ghTAPFile, 1)
			'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			FileGet(ghTAPFile, s)
			lBlockLen = s(1) * 256 + s(0) - 2

			lBlockID = Asc(InputString16(ghTAPFile, 1))
			m_lChecksum = lBlockID ' // Initialize the checksum

			If lBlockID = lID Then
				' // This block type is the same as the requested block type
				If lLength <= lBlockLen Then
					' // There are enough bytes in the block to cover this request
					ReadTAPBlock(lStart, lLength)
					If lLength < lBlockLen Then
						' // Skip the rest of the bytes up to the end of the block
						SkipTAPBytes(lBlockLen - lLength)
					End If
					lBlockChecksum = Asc(InputString16(ghTAPFile, 1))
					regIX = (regIX + lLength) And &HFFFF
					regDE = 0
					If m_lChecksum = lBlockChecksum Then
						LoadTAP = True
					Else
						LoadTAP = False
					End If
				Else
					' // More bytes requested than there are in the block
					ReadTAPBlock(lStart, lBlockLen)
					lBlockChecksum = Asc(InputString16(ghTAPFile, 1))
					regIX = (regIX + lBlockLen) And &HFFFF
					regDE = regDE - lBlockLen
					LoadTAP = False
				End If
			Else
				' // Wrong block type -- skip this block
				SkipTAPBytes(lBlockLen)
				lBlockChecksum = Asc(InputString16(ghTAPFile, 1))
				LoadTAP = False
			End If
		End If
		initscreen()
		screenPaint()
	End Function
	Public Sub OpenTAPFile(ByRef sName As String)
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Dir(sName) = "" Then Exit Sub
		If ghTAPFile > 0 Then FileClose(ghTAPFile)

		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Dir(sName) = "" Then Exit Sub

		StopTape() ' // Stop the TZX tape player

		ghTAPFile = FreeFile()
		FileOpen(ghTAPFile, sName, OpenMode.Binary)

		If LOF(ghTAPFile) = 0 Then
			FileClose(ghTAPFile)
			Exit Sub
		End If

		gsTAPFileName = sName

		frmMainWnd.NewCaption = My.Application.Info.ProductName & " - " & GetFilePart(sName)
	End Sub
	Private Sub ReadTAPBlock(ByRef lStart As Integer, ByRef lLen As Integer)
		Dim s() As Byte
		Dim lCounter, a As Integer

		On Error Resume Next

		ReDim s((lLen - 1))
		'UPGRADE_WARNING: Get was upgraded to FileGet and has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		FileGet(ghTAPFile, s)

		For lCounter = 0 To lLen - 1
			a = lStart + lCounter
			gRAMPage(glPageAt(glMemAddrDiv16384(a)), a And 16383) = s(lCounter)
			m_lChecksum = m_lChecksum Xor s(lCounter)
		Next lCounter
	End Sub

	Public Sub SaveTAPFileDlg()
		On Error Resume Next

		If ghTAPFile < 1 Then
			Err.Clear()
			'UPGRADE_WARNING: CommonDialog variable was not upgraded Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="671167DC-EA81-475D-B690-7A40C7BF4A23"'
			'With frmMainWnd.dlgCommon
			'	.Title = "Select TAP file for saving"
			'	.DefaultExt = ".tap"
			'	.FileName = "*.tap"
			'	'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			'	.Filter = "Tape files (*.tap)|*.tap|All Files (*.*)|*.*"
			'	'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			'	'UPGRADE_WARNING: MSComDlg.CommonDialog property frmMainWnd.dlgCommon.Flags was upgraded to frmMainWnd.dlgCommonOpen.CheckPathExists which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			'	.CheckPathExists = True
			'	'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'	'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'	.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			'	'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'	'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'	.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			'	'UPGRADE_WARNING: MSComDlg.CommonDialog property frmMainWnd.dlgCommon.Flags was upgraded to frmMainWnd.dlgCommonOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			'	'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			'	.ShowReadOnly = False
			'	'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'	'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'	.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
			'	'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			'	.CancelError = True
			'	.ShowDialog()
			'	resetKeyboard()
			'	'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			'	If Err.Number = DialogResult.Cancel Then
			'		Exit Sub
			'	End If
			'End With
			If frmMainWnd.dlgCommonOpen.FileName <> "" Then
				ghTAPFile = FreeFile()
				FileOpen(ghTAPFile, frmMainWnd.dlgCommonOpen.FileName, OpenMode.Binary)

				gsTAPFileName = frmMainWnd.dlgCommonOpen.FileName
				gMRU.AddMRUFile(gsTAPFileName)
				frmMainWnd.NewCaption = My.Application.Info.ProductName & " - " & GetFilePart(gsTAPFileName)
			End If
		End If
		StopTape() ' // Stop the TZX tape player

		If SaveTAP(glMemAddrDiv256(regAF_), regIX, regDE) Then
			regIX = regIX + regDE
			regDE = 0
		End If

		resetKeyboard()
		regPC = 1342 ' RET
	End Sub

	Private Sub SkipTAPBytes(ByRef lLen As Integer)
		Dim s As String
		Dim lCounter As Integer

		s = InputString16(ghTAPFile, lLen)
		For lCounter = 1 To Len(s)
			m_lChecksum = m_lChecksum Xor Asc(Mid(s, lCounter, 1))
		Next lCounter
	End Sub
End Module