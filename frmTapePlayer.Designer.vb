<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTapePlayer
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents lstBlocks As System.Windows.Forms.ListBox
	Public WithEvents cmdNext As System.Windows.Forms.Button
	Public WithEvents cmdPrev As System.Windows.Forms.Button
	Public WithEvents cmdRewind As System.Windows.Forms.Button
	Public WithEvents cmdStop As System.Windows.Forms.Button
	Public WithEvents cmdPlay As System.Windows.Forms.Button
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTapePlayer))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.lstBlocks = New System.Windows.Forms.ListBox
		Me.cmdNext = New System.Windows.Forms.Button
		Me.cmdPrev = New System.Windows.Forms.Button
		Me.cmdRewind = New System.Windows.Forms.Button
		Me.cmdStop = New System.Windows.Forms.Button
		Me.cmdPlay = New System.Windows.Forms.Button
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
		Me.Text = "vbSpec TZX Tape Controls"
		Me.ClientSize = New System.Drawing.Size(254, 207)
		Me.Location = New System.Drawing.Point(3, 19)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmTapePlayer"
		Me.lstBlocks.Size = New System.Drawing.Size(253, 176)
		Me.lstBlocks.Location = New System.Drawing.Point(0, 32)
		Me.lstBlocks.TabIndex = 5
		Me.lstBlocks.TabStop = False
		Me.lstBlocks.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lstBlocks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lstBlocks.BackColor = System.Drawing.SystemColors.Window
		Me.lstBlocks.CausesValidation = True
		Me.lstBlocks.Enabled = True
		Me.lstBlocks.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lstBlocks.IntegralHeight = True
		Me.lstBlocks.Cursor = System.Windows.Forms.Cursors.Default
		Me.lstBlocks.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.lstBlocks.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lstBlocks.Sorted = False
		Me.lstBlocks.Visible = True
		Me.lstBlocks.MultiColumn = False
		Me.lstBlocks.Name = "lstBlocks"
		Me.cmdNext.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdNext.Size = New System.Drawing.Size(49, 25)
		Me.cmdNext.Location = New System.Drawing.Point(204, 4)
		Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
		Me.cmdNext.TabIndex = 4
		Me.ToolTip1.SetToolTip(Me.cmdNext, "Next Block")
		Me.cmdNext.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdNext.BackColor = System.Drawing.SystemColors.Control
		Me.cmdNext.CausesValidation = True
		Me.cmdNext.Enabled = True
		Me.cmdNext.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdNext.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdNext.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdNext.TabStop = True
		Me.cmdNext.Name = "cmdNext"
		Me.cmdPrev.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdPrev.Size = New System.Drawing.Size(49, 25)
		Me.cmdPrev.Location = New System.Drawing.Point(152, 4)
		Me.cmdPrev.Image = CType(resources.GetObject("cmdPrev.Image"), System.Drawing.Image)
		Me.cmdPrev.TabIndex = 3
		Me.ToolTip1.SetToolTip(Me.cmdPrev, "Previous Block")
		Me.cmdPrev.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdPrev.BackColor = System.Drawing.SystemColors.Control
		Me.cmdPrev.CausesValidation = True
		Me.cmdPrev.Enabled = True
		Me.cmdPrev.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdPrev.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdPrev.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdPrev.TabStop = True
		Me.cmdPrev.Name = "cmdPrev"
		Me.cmdRewind.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdRewind.Size = New System.Drawing.Size(53, 25)
		Me.cmdRewind.Location = New System.Drawing.Point(96, 4)
		Me.cmdRewind.Image = CType(resources.GetObject("cmdRewind.Image"), System.Drawing.Image)
		Me.cmdRewind.TabIndex = 2
		Me.ToolTip1.SetToolTip(Me.cmdRewind, "Rewind to start")
		Me.cmdRewind.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdRewind.BackColor = System.Drawing.SystemColors.Control
		Me.cmdRewind.CausesValidation = True
		Me.cmdRewind.Enabled = True
		Me.cmdRewind.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdRewind.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdRewind.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdRewind.TabStop = True
		Me.cmdRewind.Name = "cmdRewind"
		Me.cmdStop.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdStop.Size = New System.Drawing.Size(45, 25)
		Me.cmdStop.Location = New System.Drawing.Point(48, 4)
		Me.cmdStop.Image = CType(resources.GetObject("cmdStop.Image"), System.Drawing.Image)
		Me.cmdStop.TabIndex = 1
		Me.ToolTip1.SetToolTip(Me.cmdStop, "Stop/Pause")
		Me.cmdStop.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdStop.BackColor = System.Drawing.SystemColors.Control
		Me.cmdStop.CausesValidation = True
		Me.cmdStop.Enabled = True
		Me.cmdStop.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdStop.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdStop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdStop.TabStop = True
		Me.cmdStop.Name = "cmdStop"
		Me.cmdPlay.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdPlay.Size = New System.Drawing.Size(45, 25)
		Me.cmdPlay.Location = New System.Drawing.Point(0, 4)
		Me.cmdPlay.Image = CType(resources.GetObject("cmdPlay.Image"), System.Drawing.Image)
		Me.cmdPlay.TabIndex = 0
		Me.ToolTip1.SetToolTip(Me.cmdPlay, "Play")
		Me.cmdPlay.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdPlay.BackColor = System.Drawing.SystemColors.Control
		Me.cmdPlay.CausesValidation = True
		Me.cmdPlay.Enabled = True
		Me.cmdPlay.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdPlay.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdPlay.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdPlay.TabStop = True
		Me.cmdPlay.Name = "cmdPlay"
		Me.Controls.Add(lstBlocks)
		Me.Controls.Add(cmdNext)
		Me.Controls.Add(cmdPrev)
		Me.Controls.Add(cmdRewind)
		Me.Controls.Add(cmdStop)
		Me.Controls.Add(cmdPlay)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class