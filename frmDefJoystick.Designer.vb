<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDefJoystick
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdLoad As System.Windows.Forms.Button
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public comdlgFileOpen As System.Windows.Forms.OpenFileDialog
	Public comdlgFileSave As System.Windows.Forms.SaveFileDialog
	Public WithEvents CancelButton_Renamed As System.Windows.Forms.Button
	Public WithEvents OKButton As System.Windows.Forms.Button
	Public WithEvents lvButtons As System.Windows.Forms.ListView
	Public WithEvents cmdUndefine As System.Windows.Forms.Button
	Public WithEvents cmdDefine As System.Windows.Forms.Button
	Public WithEvents fraButtonDefintions As System.Windows.Forms.GroupBox
	Public WithEvents txtValueDown As System.Windows.Forms.TextBox
	Public WithEvents txtPortDown As System.Windows.Forms.TextBox
	Public WithEvents txtValueLeft As System.Windows.Forms.TextBox
	Public WithEvents txtPortLeft As System.Windows.Forms.TextBox
	Public WithEvents txtValueRight As System.Windows.Forms.TextBox
	Public WithEvents txtPortRight As System.Windows.Forms.TextBox
	Public WithEvents txtValueUp As System.Windows.Forms.TextBox
	Public WithEvents txtPortUp As System.Windows.Forms.TextBox
	Public WithEvents txtRight As System.Windows.Forms.TextBox
	Public WithEvents txtLeft As System.Windows.Forms.TextBox
	Public WithEvents txtDown As System.Windows.Forms.TextBox
	Public WithEvents txtUp As System.Windows.Forms.TextBox
	Public WithEvents _lblOr_3 As System.Windows.Forms.Label
	Public WithEvents _lblOr_2 As System.Windows.Forms.Label
	Public WithEvents _lblOr_1 As System.Windows.Forms.Label
	Public WithEvents _lblOr_0 As System.Windows.Forms.Label
	Public WithEvents _lblKomma_3 As System.Windows.Forms.Label
	Public WithEvents _lblKomma_2 As System.Windows.Forms.Label
	Public WithEvents _lblKomma_1 As System.Windows.Forms.Label
	Public WithEvents _lblKomma_0 As System.Windows.Forms.Label
	Public WithEvents lblRight As System.Windows.Forms.Label
	Public WithEvents lblLeft As System.Windows.Forms.Label
	Public WithEvents lblDown As System.Windows.Forms.Label
	Public WithEvents lblUp As System.Windows.Forms.Label
	Public WithEvents fraDirectionDefintions As System.Windows.Forms.GroupBox
	Public WithEvents lblKomma As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents lblOr As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDefJoystick))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdLoad = New System.Windows.Forms.Button
		Me.cmdSave = New System.Windows.Forms.Button
		Me.comdlgFileOpen = New System.Windows.Forms.OpenFileDialog
		Me.comdlgFileSave = New System.Windows.Forms.SaveFileDialog
		Me.CancelButton_Renamed = New System.Windows.Forms.Button
		Me.OKButton = New System.Windows.Forms.Button
		Me.fraButtonDefintions = New System.Windows.Forms.GroupBox
		Me.lvButtons = New System.Windows.Forms.ListView
		Me.cmdUndefine = New System.Windows.Forms.Button
		Me.cmdDefine = New System.Windows.Forms.Button
		Me.fraDirectionDefintions = New System.Windows.Forms.GroupBox
		Me.txtValueDown = New System.Windows.Forms.TextBox
		Me.txtPortDown = New System.Windows.Forms.TextBox
		Me.txtValueLeft = New System.Windows.Forms.TextBox
		Me.txtPortLeft = New System.Windows.Forms.TextBox
		Me.txtValueRight = New System.Windows.Forms.TextBox
		Me.txtPortRight = New System.Windows.Forms.TextBox
		Me.txtValueUp = New System.Windows.Forms.TextBox
		Me.txtPortUp = New System.Windows.Forms.TextBox
		Me.txtRight = New System.Windows.Forms.TextBox
		Me.txtLeft = New System.Windows.Forms.TextBox
		Me.txtDown = New System.Windows.Forms.TextBox
		Me.txtUp = New System.Windows.Forms.TextBox
		Me._lblOr_3 = New System.Windows.Forms.Label
		Me._lblOr_2 = New System.Windows.Forms.Label
		Me._lblOr_1 = New System.Windows.Forms.Label
		Me._lblOr_0 = New System.Windows.Forms.Label
		Me._lblKomma_3 = New System.Windows.Forms.Label
		Me._lblKomma_2 = New System.Windows.Forms.Label
		Me._lblKomma_1 = New System.Windows.Forms.Label
		Me._lblKomma_0 = New System.Windows.Forms.Label
		Me.lblRight = New System.Windows.Forms.Label
		Me.lblLeft = New System.Windows.Forms.Label
		Me.lblDown = New System.Windows.Forms.Label
		Me.lblUp = New System.Windows.Forms.Label
		Me.lblKomma = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.lblOr = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.fraButtonDefintions.SuspendLayout()
		Me.fraDirectionDefintions.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.lblKomma, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.lblOr, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "User defined joystick"
		Me.ClientSize = New System.Drawing.Size(377, 271)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDefJoystick"
		Me.cmdLoad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdLoad.Text = "Load..."
		Me.cmdLoad.Size = New System.Drawing.Size(73, 25)
		Me.cmdLoad.Location = New System.Drawing.Point(299, 94)
		Me.cmdLoad.TabIndex = 32
		Me.ToolTip1.SetToolTip(Me.cmdLoad, "Loads a joystick configuration from a file")
		Me.cmdLoad.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdLoad.BackColor = System.Drawing.SystemColors.Control
		Me.cmdLoad.CausesValidation = True
		Me.cmdLoad.Enabled = True
		Me.cmdLoad.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdLoad.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdLoad.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdLoad.TabStop = True
		Me.cmdLoad.Name = "cmdLoad"
		Me.cmdSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdSave.Text = "Save..."
		Me.cmdSave.Size = New System.Drawing.Size(73, 25)
		Me.cmdSave.Location = New System.Drawing.Point(299, 66)
		Me.cmdSave.TabIndex = 31
		Me.ToolTip1.SetToolTip(Me.cmdSave, "Saves the current joystick configuration into a file")
		Me.cmdSave.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
		Me.cmdSave.CausesValidation = True
		Me.cmdSave.Enabled = True
		Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdSave.TabStop = True
		Me.cmdSave.Name = "cmdSave"
		Me.CancelButton_Renamed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.CancelButton_Renamed
		Me.CancelButton_Renamed.Text = "Cancel"
		Me.CancelButton_Renamed.Size = New System.Drawing.Size(73, 25)
		Me.CancelButton_Renamed.Location = New System.Drawing.Point(299, 38)
		Me.CancelButton_Renamed.TabIndex = 30
		Me.CancelButton_Renamed.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.CancelButton_Renamed.BackColor = System.Drawing.SystemColors.Control
		Me.CancelButton_Renamed.CausesValidation = True
		Me.CancelButton_Renamed.Enabled = True
		Me.CancelButton_Renamed.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CancelButton_Renamed.Cursor = System.Windows.Forms.Cursors.Default
		Me.CancelButton_Renamed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CancelButton_Renamed.TabStop = True
		Me.CancelButton_Renamed.Name = "CancelButton_Renamed"
		Me.OKButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.OKButton.Text = "OK"
		Me.AcceptButton = Me.OKButton
		Me.OKButton.Size = New System.Drawing.Size(73, 25)
		Me.OKButton.Location = New System.Drawing.Point(299, 10)
		Me.OKButton.TabIndex = 29
		Me.OKButton.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.OKButton.BackColor = System.Drawing.SystemColors.Control
		Me.OKButton.CausesValidation = True
		Me.OKButton.Enabled = True
		Me.OKButton.ForeColor = System.Drawing.SystemColors.ControlText
		Me.OKButton.Cursor = System.Windows.Forms.Cursors.Default
		Me.OKButton.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OKButton.TabStop = True
		Me.OKButton.Name = "OKButton"
		Me.fraButtonDefintions.Text = "Buttons"
		Me.fraButtonDefintions.Size = New System.Drawing.Size(290, 135)
		Me.fraButtonDefintions.Location = New System.Drawing.Point(3, 131)
		Me.fraButtonDefintions.TabIndex = 25
		Me.fraButtonDefintions.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.fraButtonDefintions.BackColor = System.Drawing.SystemColors.Control
		Me.fraButtonDefintions.Enabled = True
		Me.fraButtonDefintions.ForeColor = System.Drawing.SystemColors.ControlText
		Me.fraButtonDefintions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.fraButtonDefintions.Visible = True
		Me.fraButtonDefintions.Padding = New System.Windows.Forms.Padding(0)
		Me.fraButtonDefintions.Name = "fraButtonDefintions"
		Me.lvButtons.Size = New System.Drawing.Size(186, 108)
		Me.lvButtons.Location = New System.Drawing.Point(8, 17)
		Me.lvButtons.TabIndex = 26
		Me.lvButtons.LabelWrap = True
		Me.lvButtons.HideSelection = False
		Me.lvButtons.FullRowSelect = True
		Me.lvButtons.GridLines = True
		Me.lvButtons.ForeColor = System.Drawing.SystemColors.WindowText
		Me.lvButtons.BackColor = System.Drawing.SystemColors.Window
		Me.lvButtons.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lvButtons.LabelEdit = True
		Me.lvButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.lvButtons.Name = "lvButtons"
		Me.cmdUndefine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdUndefine.Text = "Undefine"
		Me.cmdUndefine.Size = New System.Drawing.Size(73, 25)
		Me.cmdUndefine.Location = New System.Drawing.Point(199, 45)
		Me.cmdUndefine.TabIndex = 28
		Me.cmdUndefine.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdUndefine.BackColor = System.Drawing.SystemColors.Control
		Me.cmdUndefine.CausesValidation = True
		Me.cmdUndefine.Enabled = True
		Me.cmdUndefine.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdUndefine.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdUndefine.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdUndefine.TabStop = True
		Me.cmdUndefine.Name = "cmdUndefine"
		Me.cmdDefine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdDefine.Text = "Define"
		Me.cmdDefine.Size = New System.Drawing.Size(73, 25)
		Me.cmdDefine.Location = New System.Drawing.Point(199, 17)
		Me.cmdDefine.TabIndex = 27
		Me.cmdDefine.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdDefine.BackColor = System.Drawing.SystemColors.Control
		Me.cmdDefine.CausesValidation = True
		Me.cmdDefine.Enabled = True
		Me.cmdDefine.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdDefine.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdDefine.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdDefine.TabStop = True
		Me.cmdDefine.Name = "cmdDefine"
		Me.fraDirectionDefintions.Text = "Directions"
		Me.fraDirectionDefintions.Size = New System.Drawing.Size(291, 125)
		Me.fraDirectionDefintions.Location = New System.Drawing.Point(3, 5)
		Me.fraDirectionDefintions.TabIndex = 0
		Me.fraDirectionDefintions.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.fraDirectionDefintions.BackColor = System.Drawing.SystemColors.Control
		Me.fraDirectionDefintions.Enabled = True
		Me.fraDirectionDefintions.ForeColor = System.Drawing.SystemColors.ControlText
		Me.fraDirectionDefintions.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.fraDirectionDefintions.Visible = True
		Me.fraDirectionDefintions.Padding = New System.Windows.Forms.Padding(0)
		Me.fraDirectionDefintions.Name = "fraDirectionDefintions"
		Me.txtValueDown.AutoSize = False
		Me.txtValueDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtValueDown.Size = New System.Drawing.Size(26, 21)
		Me.txtValueDown.Location = New System.Drawing.Point(258, 41)
		Me.txtValueDown.TabIndex = 12
		Me.txtValueDown.Text = "255"
		Me.txtValueDown.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtValueDown.AcceptsReturn = True
		Me.txtValueDown.BackColor = System.Drawing.SystemColors.Window
		Me.txtValueDown.CausesValidation = True
		Me.txtValueDown.Enabled = True
		Me.txtValueDown.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValueDown.HideSelection = True
		Me.txtValueDown.ReadOnly = False
		Me.txtValueDown.Maxlength = 0
		Me.txtValueDown.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValueDown.MultiLine = False
		Me.txtValueDown.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValueDown.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValueDown.TabStop = True
		Me.txtValueDown.Visible = True
		Me.txtValueDown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtValueDown.Name = "txtValueDown"
		Me.txtPortDown.AutoSize = False
		Me.txtPortDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtPortDown.Size = New System.Drawing.Size(38, 21)
		Me.txtPortDown.Location = New System.Drawing.Point(212, 41)
		Me.txtPortDown.TabIndex = 10
		Me.txtPortDown.Text = "65535"
		Me.txtPortDown.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPortDown.AcceptsReturn = True
		Me.txtPortDown.BackColor = System.Drawing.SystemColors.Window
		Me.txtPortDown.CausesValidation = True
		Me.txtPortDown.Enabled = True
		Me.txtPortDown.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPortDown.HideSelection = True
		Me.txtPortDown.ReadOnly = False
		Me.txtPortDown.Maxlength = 0
		Me.txtPortDown.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPortDown.MultiLine = False
		Me.txtPortDown.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPortDown.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPortDown.TabStop = True
		Me.txtPortDown.Visible = True
		Me.txtPortDown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPortDown.Name = "txtPortDown"
		Me.txtValueLeft.AutoSize = False
		Me.txtValueLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtValueLeft.Size = New System.Drawing.Size(26, 21)
		Me.txtValueLeft.Location = New System.Drawing.Point(258, 65)
		Me.txtValueLeft.TabIndex = 18
		Me.txtValueLeft.Text = "255"
		Me.txtValueLeft.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtValueLeft.AcceptsReturn = True
		Me.txtValueLeft.BackColor = System.Drawing.SystemColors.Window
		Me.txtValueLeft.CausesValidation = True
		Me.txtValueLeft.Enabled = True
		Me.txtValueLeft.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValueLeft.HideSelection = True
		Me.txtValueLeft.ReadOnly = False
		Me.txtValueLeft.Maxlength = 0
		Me.txtValueLeft.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValueLeft.MultiLine = False
		Me.txtValueLeft.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValueLeft.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValueLeft.TabStop = True
		Me.txtValueLeft.Visible = True
		Me.txtValueLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtValueLeft.Name = "txtValueLeft"
		Me.txtPortLeft.AutoSize = False
		Me.txtPortLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtPortLeft.Size = New System.Drawing.Size(38, 21)
		Me.txtPortLeft.Location = New System.Drawing.Point(212, 65)
		Me.txtPortLeft.TabIndex = 16
		Me.txtPortLeft.Text = "65535"
		Me.txtPortLeft.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPortLeft.AcceptsReturn = True
		Me.txtPortLeft.BackColor = System.Drawing.SystemColors.Window
		Me.txtPortLeft.CausesValidation = True
		Me.txtPortLeft.Enabled = True
		Me.txtPortLeft.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPortLeft.HideSelection = True
		Me.txtPortLeft.ReadOnly = False
		Me.txtPortLeft.Maxlength = 0
		Me.txtPortLeft.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPortLeft.MultiLine = False
		Me.txtPortLeft.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPortLeft.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPortLeft.TabStop = True
		Me.txtPortLeft.Visible = True
		Me.txtPortLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPortLeft.Name = "txtPortLeft"
		Me.txtValueRight.AutoSize = False
		Me.txtValueRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtValueRight.Size = New System.Drawing.Size(26, 21)
		Me.txtValueRight.Location = New System.Drawing.Point(258, 89)
		Me.txtValueRight.TabIndex = 24
		Me.txtValueRight.Text = "255"
		Me.txtValueRight.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtValueRight.AcceptsReturn = True
		Me.txtValueRight.BackColor = System.Drawing.SystemColors.Window
		Me.txtValueRight.CausesValidation = True
		Me.txtValueRight.Enabled = True
		Me.txtValueRight.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValueRight.HideSelection = True
		Me.txtValueRight.ReadOnly = False
		Me.txtValueRight.Maxlength = 0
		Me.txtValueRight.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValueRight.MultiLine = False
		Me.txtValueRight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValueRight.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValueRight.TabStop = True
		Me.txtValueRight.Visible = True
		Me.txtValueRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtValueRight.Name = "txtValueRight"
		Me.txtPortRight.AutoSize = False
		Me.txtPortRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtPortRight.Size = New System.Drawing.Size(38, 21)
		Me.txtPortRight.Location = New System.Drawing.Point(212, 89)
		Me.txtPortRight.TabIndex = 22
		Me.txtPortRight.Text = "65535"
		Me.txtPortRight.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPortRight.AcceptsReturn = True
		Me.txtPortRight.BackColor = System.Drawing.SystemColors.Window
		Me.txtPortRight.CausesValidation = True
		Me.txtPortRight.Enabled = True
		Me.txtPortRight.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPortRight.HideSelection = True
		Me.txtPortRight.ReadOnly = False
		Me.txtPortRight.Maxlength = 0
		Me.txtPortRight.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPortRight.MultiLine = False
		Me.txtPortRight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPortRight.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPortRight.TabStop = True
		Me.txtPortRight.Visible = True
		Me.txtPortRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPortRight.Name = "txtPortRight"
		Me.txtValueUp.AutoSize = False
		Me.txtValueUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtValueUp.Size = New System.Drawing.Size(26, 21)
		Me.txtValueUp.Location = New System.Drawing.Point(258, 17)
		Me.txtValueUp.TabIndex = 6
		Me.txtValueUp.Text = "255"
		Me.txtValueUp.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtValueUp.AcceptsReturn = True
		Me.txtValueUp.BackColor = System.Drawing.SystemColors.Window
		Me.txtValueUp.CausesValidation = True
		Me.txtValueUp.Enabled = True
		Me.txtValueUp.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValueUp.HideSelection = True
		Me.txtValueUp.ReadOnly = False
		Me.txtValueUp.Maxlength = 0
		Me.txtValueUp.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValueUp.MultiLine = False
		Me.txtValueUp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValueUp.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValueUp.TabStop = True
		Me.txtValueUp.Visible = True
		Me.txtValueUp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtValueUp.Name = "txtValueUp"
		Me.txtPortUp.AutoSize = False
		Me.txtPortUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtPortUp.Size = New System.Drawing.Size(38, 21)
		Me.txtPortUp.Location = New System.Drawing.Point(212, 17)
		Me.txtPortUp.TabIndex = 4
		Me.txtPortUp.Text = "65535"
		Me.txtPortUp.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPortUp.AcceptsReturn = True
		Me.txtPortUp.BackColor = System.Drawing.SystemColors.Window
		Me.txtPortUp.CausesValidation = True
		Me.txtPortUp.Enabled = True
		Me.txtPortUp.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPortUp.HideSelection = True
		Me.txtPortUp.ReadOnly = False
		Me.txtPortUp.Maxlength = 0
		Me.txtPortUp.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPortUp.MultiLine = False
		Me.txtPortUp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPortUp.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPortUp.TabStop = True
		Me.txtPortUp.Visible = True
		Me.txtPortUp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPortUp.Name = "txtPortUp"
		Me.txtRight.AutoSize = False
		Me.txtRight.Size = New System.Drawing.Size(29, 21)
		Me.txtRight.Location = New System.Drawing.Point(164, 89)
		Me.txtRight.TabIndex = 20
		Me.txtRight.Text = "XXX"
		Me.txtRight.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtRight.AcceptsReturn = True
		Me.txtRight.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtRight.BackColor = System.Drawing.SystemColors.Window
		Me.txtRight.CausesValidation = True
		Me.txtRight.Enabled = True
		Me.txtRight.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtRight.HideSelection = True
		Me.txtRight.ReadOnly = False
		Me.txtRight.Maxlength = 0
		Me.txtRight.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtRight.MultiLine = False
		Me.txtRight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtRight.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtRight.TabStop = True
		Me.txtRight.Visible = True
		Me.txtRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtRight.Name = "txtRight"
		Me.txtLeft.AutoSize = False
		Me.txtLeft.Size = New System.Drawing.Size(29, 21)
		Me.txtLeft.Location = New System.Drawing.Point(164, 65)
		Me.txtLeft.TabIndex = 14
		Me.txtLeft.Text = "X"
		Me.txtLeft.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLeft.AcceptsReturn = True
		Me.txtLeft.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLeft.BackColor = System.Drawing.SystemColors.Window
		Me.txtLeft.CausesValidation = True
		Me.txtLeft.Enabled = True
		Me.txtLeft.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLeft.HideSelection = True
		Me.txtLeft.ReadOnly = False
		Me.txtLeft.Maxlength = 0
		Me.txtLeft.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLeft.MultiLine = False
		Me.txtLeft.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLeft.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLeft.TabStop = True
		Me.txtLeft.Visible = True
		Me.txtLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLeft.Name = "txtLeft"
		Me.txtDown.AutoSize = False
		Me.txtDown.Size = New System.Drawing.Size(29, 21)
		Me.txtDown.Location = New System.Drawing.Point(164, 41)
		Me.txtDown.TabIndex = 8
		Me.txtDown.Text = "X"
		Me.txtDown.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtDown.AcceptsReturn = True
		Me.txtDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtDown.BackColor = System.Drawing.SystemColors.Window
		Me.txtDown.CausesValidation = True
		Me.txtDown.Enabled = True
		Me.txtDown.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtDown.HideSelection = True
		Me.txtDown.ReadOnly = False
		Me.txtDown.Maxlength = 0
		Me.txtDown.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtDown.MultiLine = False
		Me.txtDown.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtDown.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtDown.TabStop = True
		Me.txtDown.Visible = True
		Me.txtDown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtDown.Name = "txtDown"
		Me.txtUp.AutoSize = False
		Me.txtUp.Size = New System.Drawing.Size(29, 21)
		Me.txtUp.Location = New System.Drawing.Point(164, 17)
		Me.txtUp.TabIndex = 2
		Me.txtUp.Text = "X"
		Me.txtUp.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtUp.AcceptsReturn = True
		Me.txtUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtUp.BackColor = System.Drawing.SystemColors.Window
		Me.txtUp.CausesValidation = True
		Me.txtUp.Enabled = True
		Me.txtUp.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtUp.HideSelection = True
		Me.txtUp.ReadOnly = False
		Me.txtUp.Maxlength = 0
		Me.txtUp.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtUp.MultiLine = False
		Me.txtUp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtUp.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtUp.TabStop = True
		Me.txtUp.Visible = True
		Me.txtUp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtUp.Name = "txtUp"
		Me._lblOr_3.Text = "or"
		Me._lblOr_3.Size = New System.Drawing.Size(11, 14)
		Me._lblOr_3.Location = New System.Drawing.Point(197, 93)
		Me._lblOr_3.TabIndex = 21
		Me._lblOr_3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblOr_3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblOr_3.BackColor = System.Drawing.SystemColors.Control
		Me._lblOr_3.Enabled = True
		Me._lblOr_3.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblOr_3.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblOr_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblOr_3.UseMnemonic = True
		Me._lblOr_3.Visible = True
		Me._lblOr_3.AutoSize = False
		Me._lblOr_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblOr_3.Name = "_lblOr_3"
		Me._lblOr_2.Text = "or"
		Me._lblOr_2.Size = New System.Drawing.Size(11, 14)
		Me._lblOr_2.Location = New System.Drawing.Point(197, 69)
		Me._lblOr_2.TabIndex = 15
		Me._lblOr_2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblOr_2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblOr_2.BackColor = System.Drawing.SystemColors.Control
		Me._lblOr_2.Enabled = True
		Me._lblOr_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblOr_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblOr_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblOr_2.UseMnemonic = True
		Me._lblOr_2.Visible = True
		Me._lblOr_2.AutoSize = False
		Me._lblOr_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblOr_2.Name = "_lblOr_2"
		Me._lblOr_1.Text = "or"
		Me._lblOr_1.Size = New System.Drawing.Size(11, 14)
		Me._lblOr_1.Location = New System.Drawing.Point(197, 45)
		Me._lblOr_1.TabIndex = 9
		Me._lblOr_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblOr_1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblOr_1.BackColor = System.Drawing.SystemColors.Control
		Me._lblOr_1.Enabled = True
		Me._lblOr_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblOr_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblOr_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblOr_1.UseMnemonic = True
		Me._lblOr_1.Visible = True
		Me._lblOr_1.AutoSize = False
		Me._lblOr_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblOr_1.Name = "_lblOr_1"
		Me._lblOr_0.Text = "or"
		Me._lblOr_0.Size = New System.Drawing.Size(11, 14)
		Me._lblOr_0.Location = New System.Drawing.Point(197, 21)
		Me._lblOr_0.TabIndex = 3
		Me._lblOr_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblOr_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblOr_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblOr_0.Enabled = True
		Me._lblOr_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblOr_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblOr_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblOr_0.UseMnemonic = True
		Me._lblOr_0.Visible = True
		Me._lblOr_0.AutoSize = False
		Me._lblOr_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblOr_0.Name = "_lblOr_0"
		Me._lblKomma_3.Text = ","
		Me._lblKomma_3.Size = New System.Drawing.Size(5, 14)
		Me._lblKomma_3.Location = New System.Drawing.Point(252, 45)
		Me._lblKomma_3.TabIndex = 11
		Me._lblKomma_3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblKomma_3.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblKomma_3.BackColor = System.Drawing.SystemColors.Control
		Me._lblKomma_3.Enabled = True
		Me._lblKomma_3.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblKomma_3.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblKomma_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblKomma_3.UseMnemonic = True
		Me._lblKomma_3.Visible = True
		Me._lblKomma_3.AutoSize = False
		Me._lblKomma_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblKomma_3.Name = "_lblKomma_3"
		Me._lblKomma_2.Text = ","
		Me._lblKomma_2.Size = New System.Drawing.Size(5, 14)
		Me._lblKomma_2.Location = New System.Drawing.Point(252, 69)
		Me._lblKomma_2.TabIndex = 17
		Me._lblKomma_2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblKomma_2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblKomma_2.BackColor = System.Drawing.SystemColors.Control
		Me._lblKomma_2.Enabled = True
		Me._lblKomma_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblKomma_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblKomma_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblKomma_2.UseMnemonic = True
		Me._lblKomma_2.Visible = True
		Me._lblKomma_2.AutoSize = False
		Me._lblKomma_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblKomma_2.Name = "_lblKomma_2"
		Me._lblKomma_1.Text = ","
		Me._lblKomma_1.Size = New System.Drawing.Size(5, 14)
		Me._lblKomma_1.Location = New System.Drawing.Point(252, 93)
		Me._lblKomma_1.TabIndex = 23
		Me._lblKomma_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblKomma_1.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblKomma_1.BackColor = System.Drawing.SystemColors.Control
		Me._lblKomma_1.Enabled = True
		Me._lblKomma_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblKomma_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblKomma_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblKomma_1.UseMnemonic = True
		Me._lblKomma_1.Visible = True
		Me._lblKomma_1.AutoSize = False
		Me._lblKomma_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblKomma_1.Name = "_lblKomma_1"
		Me._lblKomma_0.Text = ","
		Me._lblKomma_0.Size = New System.Drawing.Size(5, 14)
		Me._lblKomma_0.Location = New System.Drawing.Point(252, 21)
		Me._lblKomma_0.TabIndex = 5
		Me._lblKomma_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblKomma_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblKomma_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblKomma_0.Enabled = True
		Me._lblKomma_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblKomma_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblKomma_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblKomma_0.UseMnemonic = True
		Me._lblKomma_0.Visible = True
		Me._lblKomma_0.AutoSize = False
		Me._lblKomma_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblKomma_0.Name = "_lblKomma_0"
		Me.lblRight.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblRight.Text = "Joystick right translated into:"
		Me.lblRight.Size = New System.Drawing.Size(148, 14)
		Me.lblRight.Location = New System.Drawing.Point(13, 93)
		Me.lblRight.TabIndex = 19
		Me.lblRight.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblRight.BackColor = System.Drawing.SystemColors.Control
		Me.lblRight.Enabled = True
		Me.lblRight.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblRight.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblRight.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblRight.UseMnemonic = True
		Me.lblRight.Visible = True
		Me.lblRight.AutoSize = False
		Me.lblRight.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblRight.Name = "lblRight"
		Me.lblLeft.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblLeft.Text = "Joystick left translated into:"
		Me.lblLeft.Size = New System.Drawing.Size(148, 14)
		Me.lblLeft.Location = New System.Drawing.Point(13, 69)
		Me.lblLeft.TabIndex = 13
		Me.lblLeft.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblLeft.BackColor = System.Drawing.SystemColors.Control
		Me.lblLeft.Enabled = True
		Me.lblLeft.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblLeft.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblLeft.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblLeft.UseMnemonic = True
		Me.lblLeft.Visible = True
		Me.lblLeft.AutoSize = False
		Me.lblLeft.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblLeft.Name = "lblLeft"
		Me.lblDown.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblDown.Text = "Joystick down translated into:"
		Me.lblDown.Size = New System.Drawing.Size(148, 14)
		Me.lblDown.Location = New System.Drawing.Point(13, 45)
		Me.lblDown.TabIndex = 7
		Me.lblDown.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblDown.BackColor = System.Drawing.SystemColors.Control
		Me.lblDown.Enabled = True
		Me.lblDown.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblDown.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblDown.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblDown.UseMnemonic = True
		Me.lblDown.Visible = True
		Me.lblDown.AutoSize = False
		Me.lblDown.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblDown.Name = "lblDown"
		Me.lblUp.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblUp.Text = "Joystick up translated into:"
		Me.lblUp.Size = New System.Drawing.Size(148, 14)
		Me.lblUp.Location = New System.Drawing.Point(13, 21)
		Me.lblUp.TabIndex = 1
		Me.lblUp.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblUp.BackColor = System.Drawing.SystemColors.Control
		Me.lblUp.Enabled = True
		Me.lblUp.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblUp.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblUp.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblUp.UseMnemonic = True
		Me.lblUp.Visible = True
		Me.lblUp.AutoSize = False
		Me.lblUp.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblUp.Name = "lblUp"
		Me.Controls.Add(cmdLoad)
		Me.Controls.Add(cmdSave)
		Me.Controls.Add(CancelButton_Renamed)
		Me.Controls.Add(OKButton)
		Me.Controls.Add(fraButtonDefintions)
		Me.Controls.Add(fraDirectionDefintions)
		Me.fraButtonDefintions.Controls.Add(lvButtons)
		Me.fraButtonDefintions.Controls.Add(cmdUndefine)
		Me.fraButtonDefintions.Controls.Add(cmdDefine)
		Me.fraDirectionDefintions.Controls.Add(txtValueDown)
		Me.fraDirectionDefintions.Controls.Add(txtPortDown)
		Me.fraDirectionDefintions.Controls.Add(txtValueLeft)
		Me.fraDirectionDefintions.Controls.Add(txtPortLeft)
		Me.fraDirectionDefintions.Controls.Add(txtValueRight)
		Me.fraDirectionDefintions.Controls.Add(txtPortRight)
		Me.fraDirectionDefintions.Controls.Add(txtValueUp)
		Me.fraDirectionDefintions.Controls.Add(txtPortUp)
		Me.fraDirectionDefintions.Controls.Add(txtRight)
		Me.fraDirectionDefintions.Controls.Add(txtLeft)
		Me.fraDirectionDefintions.Controls.Add(txtDown)
		Me.fraDirectionDefintions.Controls.Add(txtUp)
		Me.fraDirectionDefintions.Controls.Add(_lblOr_3)
		Me.fraDirectionDefintions.Controls.Add(_lblOr_2)
		Me.fraDirectionDefintions.Controls.Add(_lblOr_1)
		Me.fraDirectionDefintions.Controls.Add(_lblOr_0)
		Me.fraDirectionDefintions.Controls.Add(_lblKomma_3)
		Me.fraDirectionDefintions.Controls.Add(_lblKomma_2)
		Me.fraDirectionDefintions.Controls.Add(_lblKomma_1)
		Me.fraDirectionDefintions.Controls.Add(_lblKomma_0)
		Me.fraDirectionDefintions.Controls.Add(lblRight)
		Me.fraDirectionDefintions.Controls.Add(lblLeft)
		Me.fraDirectionDefintions.Controls.Add(lblDown)
		Me.fraDirectionDefintions.Controls.Add(lblUp)
		Me.lblKomma.SetIndex(_lblKomma_3, CType(3, Short))
		Me.lblKomma.SetIndex(_lblKomma_2, CType(2, Short))
		Me.lblKomma.SetIndex(_lblKomma_1, CType(1, Short))
		Me.lblKomma.SetIndex(_lblKomma_0, CType(0, Short))
		Me.lblOr.SetIndex(_lblOr_3, CType(3, Short))
		Me.lblOr.SetIndex(_lblOr_2, CType(2, Short))
		Me.lblOr.SetIndex(_lblOr_1, CType(1, Short))
		Me.lblOr.SetIndex(_lblOr_0, CType(0, Short))
		CType(Me.lblOr, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.lblKomma, System.ComponentModel.ISupportInitialize).EndInit()
		Me.fraButtonDefintions.ResumeLayout(False)
		Me.fraDirectionDefintions.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class