<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmDefButton
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents CancelButton_Renamed As System.Windows.Forms.Button
	Public WithEvents OKButton As System.Windows.Forms.Button
	Public WithEvents txtValueFire As System.Windows.Forms.TextBox
	Public WithEvents txtPortFire As System.Windows.Forms.TextBox
	Public WithEvents txtFire As System.Windows.Forms.TextBox
	Public WithEvents _lblOr_0 As System.Windows.Forms.Label
	Public WithEvents _lblKomma_0 As System.Windows.Forms.Label
	Public WithEvents lblButton As System.Windows.Forms.Label
	Public WithEvents lblKomma As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents lblOr As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmDefButton))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.CancelButton_Renamed = New System.Windows.Forms.Button
		Me.OKButton = New System.Windows.Forms.Button
		Me.txtValueFire = New System.Windows.Forms.TextBox
		Me.txtPortFire = New System.Windows.Forms.TextBox
		Me.txtFire = New System.Windows.Forms.TextBox
		Me._lblOr_0 = New System.Windows.Forms.Label
		Me._lblKomma_0 = New System.Windows.Forms.Label
		Me.lblButton = New System.Windows.Forms.Label
		Me.lblKomma = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.lblOr = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.lblKomma, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.lblOr, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Joystick button definition"
		Me.ClientSize = New System.Drawing.Size(255, 71)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmDefButton"
		Me.CancelButton_Renamed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.CancelButton_Renamed
		Me.CancelButton_Renamed.Text = "Cancel"
		Me.CancelButton_Renamed.Size = New System.Drawing.Size(73, 25)
		Me.CancelButton_Renamed.Location = New System.Drawing.Point(128, 39)
		Me.CancelButton_Renamed.TabIndex = 7
		Me.CancelButton_Renamed.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.CancelButton_Renamed.BackColor = System.Drawing.SystemColors.Control
		Me.CancelButton_Renamed.CausesValidation = True
		Me.CancelButton_Renamed.Enabled = True
		Me.CancelButton_Renamed.ForeColor = System.Drawing.SystemColors.ControlText
		Me.CancelButton_Renamed.Cursor = System.Windows.Forms.Cursors.Default
		Me.CancelButton_Renamed.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.CancelButton_Renamed.TabStop = True
		Me.CancelButton_Renamed.Name = "CancelButton_Renamed"
		Me.OKButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.OKButton.Text = "OK"
		Me.AcceptButton = Me.OKButton
		Me.OKButton.Size = New System.Drawing.Size(73, 25)
		Me.OKButton.Location = New System.Drawing.Point(51, 39)
		Me.OKButton.TabIndex = 6
		Me.OKButton.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.OKButton.BackColor = System.Drawing.SystemColors.Control
		Me.OKButton.CausesValidation = True
		Me.OKButton.Enabled = True
		Me.OKButton.ForeColor = System.Drawing.SystemColors.ControlText
		Me.OKButton.Cursor = System.Windows.Forms.Cursors.Default
		Me.OKButton.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.OKButton.TabStop = True
		Me.OKButton.Name = "OKButton"
		Me.txtValueFire.AutoSize = False
		Me.txtValueFire.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtValueFire.Size = New System.Drawing.Size(26, 21)
		Me.txtValueFire.Location = New System.Drawing.Point(224, 6)
		Me.txtValueFire.TabIndex = 4
		Me.txtValueFire.Text = "255"
		Me.txtValueFire.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtValueFire.AcceptsReturn = True
		Me.txtValueFire.BackColor = System.Drawing.SystemColors.Window
		Me.txtValueFire.CausesValidation = True
		Me.txtValueFire.Enabled = True
		Me.txtValueFire.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValueFire.HideSelection = True
		Me.txtValueFire.ReadOnly = False
		Me.txtValueFire.Maxlength = 0
		Me.txtValueFire.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValueFire.MultiLine = False
		Me.txtValueFire.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValueFire.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValueFire.TabStop = True
		Me.txtValueFire.Visible = True
		Me.txtValueFire.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtValueFire.Name = "txtValueFire"
		Me.txtPortFire.AutoSize = False
		Me.txtPortFire.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtPortFire.Size = New System.Drawing.Size(38, 21)
		Me.txtPortFire.Location = New System.Drawing.Point(178, 6)
		Me.txtPortFire.TabIndex = 2
		Me.txtPortFire.Text = "65535"
		Me.txtPortFire.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPortFire.AcceptsReturn = True
		Me.txtPortFire.BackColor = System.Drawing.SystemColors.Window
		Me.txtPortFire.CausesValidation = True
		Me.txtPortFire.Enabled = True
		Me.txtPortFire.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPortFire.HideSelection = True
		Me.txtPortFire.ReadOnly = False
		Me.txtPortFire.Maxlength = 0
		Me.txtPortFire.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPortFire.MultiLine = False
		Me.txtPortFire.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPortFire.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPortFire.TabStop = True
		Me.txtPortFire.Visible = True
		Me.txtPortFire.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPortFire.Name = "txtPortFire"
		Me.txtFire.AutoSize = False
		Me.txtFire.Size = New System.Drawing.Size(29, 21)
		Me.txtFire.Location = New System.Drawing.Point(128, 6)
		Me.txtFire.TabIndex = 1
		Me.txtFire.Text = "X"
		Me.txtFire.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFire.AcceptsReturn = True
		Me.txtFire.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFire.BackColor = System.Drawing.SystemColors.Window
		Me.txtFire.CausesValidation = True
		Me.txtFire.Enabled = True
		Me.txtFire.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFire.HideSelection = True
		Me.txtFire.ReadOnly = False
		Me.txtFire.Maxlength = 0
		Me.txtFire.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFire.MultiLine = False
		Me.txtFire.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFire.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFire.TabStop = True
		Me.txtFire.Visible = True
		Me.txtFire.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFire.Name = "txtFire"
		Me._lblOr_0.Text = "or"
		Me._lblOr_0.Size = New System.Drawing.Size(11, 14)
		Me._lblOr_0.Location = New System.Drawing.Point(162, 10)
		Me._lblOr_0.TabIndex = 5
		Me._lblOr_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblOr_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblOr_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblOr_0.Enabled = True
		Me._lblOr_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblOr_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblOr_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblOr_0.UseMnemonic = True
		Me._lblOr_0.Visible = True
		Me._lblOr_0.AutoSize = False
		Me._lblOr_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblOr_0.Name = "_lblOr_0"
		Me._lblKomma_0.Text = ","
		Me._lblKomma_0.Size = New System.Drawing.Size(5, 14)
		Me._lblKomma_0.Location = New System.Drawing.Point(218, 10)
		Me._lblKomma_0.TabIndex = 3
		Me._lblKomma_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblKomma_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblKomma_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblKomma_0.Enabled = True
		Me._lblKomma_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblKomma_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblKomma_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblKomma_0.UseMnemonic = True
		Me._lblKomma_0.Visible = True
		Me._lblKomma_0.AutoSize = False
		Me._lblKomma_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblKomma_0.Name = "_lblKomma_0"
		Me.lblButton.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblButton.Text = "Button X translated into:"
		Me.lblButton.Size = New System.Drawing.Size(114, 15)
		Me.lblButton.Location = New System.Drawing.Point(4, 10)
		Me.lblButton.TabIndex = 0
		Me.lblButton.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblButton.BackColor = System.Drawing.SystemColors.Control
		Me.lblButton.Enabled = True
		Me.lblButton.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblButton.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblButton.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblButton.UseMnemonic = True
		Me.lblButton.Visible = True
		Me.lblButton.AutoSize = False
		Me.lblButton.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblButton.Name = "lblButton"
		Me.Controls.Add(CancelButton_Renamed)
		Me.Controls.Add(OKButton)
		Me.Controls.Add(txtValueFire)
		Me.Controls.Add(txtPortFire)
		Me.Controls.Add(txtFire)
		Me.Controls.Add(_lblOr_0)
		Me.Controls.Add(_lblKomma_0)
		Me.Controls.Add(lblButton)
		Me.lblKomma.SetIndex(_lblKomma_0, CType(0, Short))
		Me.lblOr.SetIndex(_lblOr_0, CType(0, Short))
		CType(Me.lblOr, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.lblKomma, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class