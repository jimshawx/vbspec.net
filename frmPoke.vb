Option Strict Off
Option Explicit On
Friend Class frmPoke
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmPoke.frm within vbSpec.vbp
	'
	'   Author: Miklos Muhi <miklos.muhi@bakonyi.de>
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	'*******************************************************************************
	'Cancel operation
	'*******************************************************************************
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		
		'Unload the form
		Me.Close()
	End Sub
	
	Private Sub cmdPeek_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPeek.Click
		txtValue.Text = Trim(CStr(modZ80.peekb(CInt(txtAddress.Text))))
	End Sub
	
	'*******************************************************************************
	'Poke memory
	'*******************************************************************************
	Private Sub cmdPoke_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPoke.Click
		
		'Variables
		Dim lAddress, lValue As Integer
		
		'Validation -- there must be a value for the Address
		If Trim(txtAddress.Text) = vbNullString Then
			'Report the problem
			MsgBox("You must give an address to poke to!", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			'Set the focus
			txtAddress.Focus()
			'Leave this function
			Exit Sub
		End If
		
		'Validation -- there must be a value to poke
		If Trim(txtValue.Text) = vbNullString Then
			'Report the problem
			MsgBox("Missing a value to poke!", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			'Set the focus
			txtValue.Focus()
			'Leave this function
			Exit Sub
		End If
		
		'Initialisation
		lAddress = -1
		lValue = -1
		
		'Error handling
		On Error GoTo POKE_ERROR
		
		'Convert the values
		lAddress = CInt(txtAddress.Text)
		lValue = CInt(txtValue.Text)
		
		'Poke value
		modZ80.pokeb(lAddress, lValue)
		
		'Reset controls
		txtAddress.Text = vbNullString
		txtValue.Text = vbNullString
		'Reset focus
		txtAddress.Focus()
		
		'There wasn't any problem
		Exit Sub
POKE_ERROR: 
		
		'There can be only one problem: the conversions of the strings in long or byte failed
		'If the address-conversion failed
		If lAddress = -1 Then
			'Report the problem
			MsgBox("Invalid address format!", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			'If the address-conversion was a success
		Else
			'If the address-conversion failed
			If lValue = -1 Then
				'Report the problem
				MsgBox("The value has an invalid format!", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
				'I don't know this...
			Else
				'Report the problem
				MsgBox("Unknown error!", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			End If
		End If
	End Sub
End Class