Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmDefJoystick
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmDefJoystick.frm within vbSpec.vbp
	'
	'   "Options->General settings->Joystick->Define" dialog for vbSpec
	'
	'   Author: Miklos Muhi <vbspec@muhi.org>
	'   http://www.muhi.org/vbspec/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	'Joystick definition rules:
	'1. You can only then define a joystick, if the joystick is connected.
	'2. If you open this configuration window, you will see the current joystick settigns,
	'   described with ports.
	'3. You can save and load joystick configuration files (myconfig.jcf)
	'   The file format of then JCF-files looks like this:
	'   [vbSpec joystick configuration file 0.01]
	'   UP=<Key>*<Port>*<Value>
	'   DOWN=<Key>*<Port>*<Value>
	'   LEFT=<Key>*<Port>*<Value>
	'   RIGHT=<Key>*<Port>*<Value>
	'   BUTTON=<Key>*<Port>*<Value>
	'   ...
	'   BUTTON=<Key>*<Port>*<Value>
	'
	'4. You can redefine define each supported joystick.
	
	'Variables
	Private bIsFirstActivate As Boolean
	'The joystick you redefine
	Private lJoystickNo As Integer
	'The current settings
	Private lJoystickSetting As Integer
	'Fire button
	Private lFireButton As Integer
	'Changes
	Private bEdit As Boolean
	'Keys, ports and  values
	Private sKey As String
	Private lPort, lValue As Integer
	'Edit flags
	Private bKey, bPortValue As Boolean
	'OK flag
	Private bOK As Boolean
	
	'This property sets the joystick you are going to redefine
	Friend WriteOnly Property JoystickNo() As Integer
		Set(ByVal Value As Integer)
			lJoystickNo = Value
		End Set
	End Property
	'This property provides the joysticks current settings
	Friend WriteOnly Property JoystickSetting() As Integer
		Set(ByVal Value As Integer)
			lJoystickSetting = Value
		End Set
	End Property
	'This property provides the current fire button setting
	Friend WriteOnly Property FireButton() As Integer
		Set(ByVal Value As Integer)
			lFireButton = Value
		End Set
	End Property
	'Get the OK-Flag
	Friend ReadOnly Property OK() As Boolean
		Get
			OK = bOK
		End Get
	End Property
	
	'The form will be loaded
	Private Sub frmDefJoystick_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		'Variables
		Dim oColHeader As System.Windows.Forms.ColumnHeader
		'Setup the controls
		txtUp.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtUp.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtUp.Maxlength = 3
		txtUp.ReadOnly = True
		txtPortUp.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtPortUp.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtPortUp.Maxlength = 5
		txtValueUp.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtValueUp.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtValueUp.Maxlength = 3
		txtDown.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtDown.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtDown.Maxlength = 3
		txtDown.ReadOnly = True
		txtPortDown.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtPortDown.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtPortDown.Maxlength = 5
		txtValueDown.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtValueDown.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtValueDown.Maxlength = 3
		txtLeft.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtLeft.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtLeft.Maxlength = 3
		txtLeft.ReadOnly = True
		txtPortLeft.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtPortLeft.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtPortLeft.Maxlength = 5
		txtValueLeft.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtValueLeft.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtValueLeft.Maxlength = 3
		txtRight.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtRight.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtRight.Maxlength = 3
		txtRight.ReadOnly = True
		txtPortRight.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtPortRight.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtPortRight.Maxlength = 5
		txtValueRight.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtValueRight.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtValueRight.Maxlength = 3
		With lvButtons
			.AllowColumnReorder = False
			'UPGRADE_ISSUE: MSComctlLib.ListView property lvButtons.Appearance was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'.Appearance = System.Windows.Forms.BorderStyle.Fixed3D
			'UPGRADE_ISSUE: MSComctlLib.ListView property lvButtons.FlatScrollBar was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'.FlatScrollBar = False
			.FullRowSelect = True
			.GridLines = True
			.HideSelection = False
			.LabelEdit = False
			.MultiSelect = False
			.View = System.Windows.Forms.View.Details
			oColHeader = .Columns.Add("Button")
			oColHeader.Width = CInt((.Width) / 100) * 25
			oColHeader = .Columns.Add("Key")
			oColHeader.Width = CInt((.Width) / 100) * 19
			oColHeader = .Columns.Add("Port")
			oColHeader.Width = CInt((.Width) / 100) * 23
			oColHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
			oColHeader = .Columns.Add("Value")
			oColHeader.Width = CInt((.Width) / 100) * 22
			oColHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		End With
		'Default values
		lJoystickNo = 1
		lJoystickSetting = modSpectrum.ZXJOYSTICKS.zxjKempston
		lFireButton = 1
		bEdit = True
		bKey = False
		bPortValue = False
		bOK = False
		'Now it's time for the first activation
		bIsFirstActivate = True
	End Sub
	
	'The form will be activated
	'UPGRADE_WARNING: Form event frmDefJoystick.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmDefJoystick_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		'Variables
		Dim lCounter As Integer
		Dim oItem As System.Windows.Forms.ListViewItem
		Dim sKey As String
		Dim lPort, lValue As Integer
		'Only by the first activation
		If bIsFirstActivate Then
			'If there is no valid joystick
			If Not ((modSpectrum.bJoystick1Valid And (lJoystickNo = 1)) Or ((modSpectrum.bJoystick2Valid And (lJoystickNo = 2)))) Then
				'Close window
				Me.Close()
				Exit Sub
			End If
			'Set caption
			Me.Text = "Redefine joystick " & Trim(CStr(lJoystickNo))
			'You cannot set an invalid joystick
			If lJoystickSetting = modSpectrum.ZXJOYSTICKS.zxjInvalid Then
				Me.Close()
				Exit Sub
			End If
			'Set values
			txtPortUp.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_UP).lPort))
			txtPortDown.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_DOWN).lPort))
			txtPortLeft.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_LEFT).lPort))
			txtPortRight.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_RIGHT).lPort))
			txtValueUp.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_UP).lValue))
			txtValueDown.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_DOWN).lValue))
			txtValueLeft.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_LEFT).lValue))
			txtValueRight.Text = Trim(CStr(aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_RIGHT).lValue))
			If lJoystickNo = 1 Then
				For lCounter = 1 To lPCJoystick1Buttons
					oItem = lvButtons.Items.Add("Button" & Trim(CStr(lCounter)))
					lPort = aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_BUTTON_BASE + lCounter).lPort
					lValue = aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_BUTTON_BASE + lCounter).lValue
					If (lPort <> 0) And (lValue <> 0) Then
						PortValueToKeyStroke(lPort, lValue, sKey)
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 1 Then
							oItem.SubItems(1).Text = sKey
						Else
							oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, sKey))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 2 Then
							oItem.SubItems(2).Text = CStr(lPort)
						Else
							oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, CStr(lPort)))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 3 Then
							oItem.SubItems(3).Text = CStr(lValue)
						Else
							oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, CStr(lValue)))
						End If
					Else
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 1 Then
							oItem.SubItems(1).Text = " "
						Else
							oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 2 Then
							oItem.SubItems(2).Text = " "
						Else
							oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 3 Then
							oItem.SubItems(3).Text = " "
						Else
							oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
						End If
					End If
				Next lCounter
			End If
			If lJoystickNo = 2 Then
				For lCounter = 1 To lPCJoystick2Buttons
					oItem = lvButtons.Items.Add(" ")
					lPort = aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_BUTTON_BASE + lCounter).lPort
					lValue = aJoystikDefinitionTable(lJoystickNo - 1, lJoystickSetting, JDT_BUTTON_BASE + lCounter).lValue
					If (lPort <> 0) And (lValue <> 0) Then
						PortValueToKeyStroke(lPort, lValue, sKey)
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 1 Then
							oItem.SubItems(1).Text = sKey
						Else
							oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, sKey))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 2 Then
							oItem.SubItems(2).Text = CStr(lPort)
						Else
							oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, CStr(lPort)))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 3 Then
							oItem.SubItems(3).Text = CStr(lValue)
						Else
							oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, CStr(lValue)))
						End If
					Else
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 1 Then
							oItem.SubItems(1).Text = " "
						Else
							oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 2 Then
							oItem.SubItems(2).Text = " "
						Else
							oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
						End If
						'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
						If oItem.SubItems.Count > 3 Then
							oItem.SubItems(3).Text = " "
						Else
							oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
						End If
					End If
				Next lCounter
			End If
			'Select the first button
			'UPGRADE_WARNING: Lower bound of collection lvButtons.ListItems has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			lvButtons.FocusedItem = lvButtons.Items.Item(1)
			'UPGRADE_ISSUE: MSComctlLib.ListView event lvButtons.ItemClick was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
			lvButtons_ItemClick(lvButtons.FocusedItem)
			'No edit
			bEdit = False
			'The first activation ends here
			bIsFirstActivate = False
		End If
	End Sub
	
	'The form will be unloaded
	Private Sub frmDefJoystick_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		'Variables
		Dim vbmrValue As MsgBoxResult
		'If there are unsaved data
		If bEdit Then
			'Ask the user
			vbmrValue = MsgBox("Do you want to take over your changes?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1)
			'Analyse
			Select Case vbmrValue
				'Save changes and exit
				Case MsgBoxResult.Yes
					'Save changes
					OKButton_Click(OKButton, New System.EventArgs())
					'Do not save changes
				Case MsgBoxResult.No
					'No changes
					bOK = False
					'Cancel
				Case MsgBoxResult.Cancel
					'UPGRADE_ISSUE: Event parameter Cancel was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FB723E3C-1C06-4D2B-B083-E6CD0D334DA8"'
					'Cancel = 1
			End Select
		End If
	End Sub
	
	'Close and save
	Private Sub OKButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OKButton.Click
		
		'Variablen
		Dim sKey As String
		Dim lPort As Integer
		Dim lValue As Integer
		Dim oItem As System.Windows.Forms.ListViewItem
		
		'Validate
		If Not Validate_Renamed() Then
			Exit Sub
		End If
		
		'Take over changes
		lPort = CInt(txtPortUp.Text)
		lValue = CInt(txtValueUp.Text)
		PortValueToKeyStroke(lPort, lValue, sKey)
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_UP).sKey = sKey
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_UP).lPort = lPort
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_UP).lValue = lValue
		lPort = CInt(txtPortDown.Text)
		lValue = CInt(txtValueDown.Text)
		PortValueToKeyStroke(lPort, lValue, sKey)
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_DOWN).sKey = sKey
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_DOWN).lPort = lPort
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_DOWN).lValue = lValue
		lPort = CInt(txtPortLeft.Text)
		lValue = CInt(txtValueLeft.Text)
		PortValueToKeyStroke(lPort, lValue, sKey)
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_LEFT).sKey = sKey
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_LEFT).lPort = lPort
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_LEFT).lValue = lValue
		lPort = CInt(txtPortRight.Text)
		lValue = CInt(txtValueRight.Text)
		PortValueToKeyStroke(lPort, lValue, sKey)
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_RIGHT).sKey = sKey
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_RIGHT).lPort = lPort
		aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_RIGHT).lValue = lValue
		For	Each oItem In lvButtons.Items
			'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			If (Trim(oItem.SubItems(2).Text) <> vbNullString) And (Trim(oItem.SubItems(3).Text) <> vbNullString) Then
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				lPort = CInt(oItem.SubItems(2).Text)
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				lValue = CInt(oItem.SubItems(3).Text)
				PortValueToKeyStroke(lPort, lValue, sKey)
				aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON_BASE + oItem.Index).sKey = sKey
				aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON_BASE + oItem.Index).lPort = lPort
				aJoystikDefinitionTable(lJoystickNo - 1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON_BASE + oItem.Index).lValue = lValue
			End If
		Next oItem
		
		'No edit
		bEdit = False
		'Taken over
		bOK = True
		'Close form
		Me.Close()
	End Sub
	
	'Close without save
	Private Sub CancelButton_Renamed_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CancelButton_Renamed.Click
		bOK = False
		Me.Close()
	End Sub
	
	'Save the joystick configuration
	Private Sub cmdSave_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdSave.Click
		
		'Variables
		Dim sFileName As String
		Dim hFile As Integer
		Dim oItem As System.Windows.Forms.ListViewItem
		
		'Error handling
		On Error GoTo CMDSAVECLICK_ERROR

		'Initialise
		'UPGRADE_WARNING: CommonDialog variable was not upgraded Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="671167DC-EA81-475D-B690-7A40C7BF4A23"'
		'With comdlgFile
		'	'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'	.CancelError = True
		'	.Title = "Save joystick configuration"
		'	.DefaultExt = ".jcf"
		'	.FileName = ""
		'	'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'	.Filter = "vbSpec joystick configuration file (*.jcf)|*.jcf|"
		'	'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'	.CheckPathExists = True
		'	.CheckPathExists = True
		'	'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'	.OverwritePrompt = True
		'	'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'	'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'	.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
		'	'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'	'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'	.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
		'	'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'	'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'	.ShowReadOnly = False
		'	'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'	'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'	.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
		'	.ShowDialog()
		'End With
		'Get file name
		sFileName = comdlgFileOpen.FileName
		
		'Start
		hFile = FreeFile
		FileOpen(hFile, sFileName, OpenMode.Output)
		
		'Header
		PrintLine(hFile, "[vbSpec joystick configuration file 0.01]")
		'Up
		PrintLine(hFile, "UP=" & Trim(txtUp.Text) & "*" & Trim(CStr(txtPortUp.Text)) & "*" & Trim(CStr(txtValueUp.Text)))
		'Down
		PrintLine(hFile, "DOWN=" & Trim(txtDown.Text) & "*" & Trim(CStr(txtPortDown.Text)) & "*" & Trim(CStr(txtValueDown.Text)))
		'Left
		PrintLine(hFile, "LEFT=" & Trim(txtLeft.Text) & "*" & Trim(CStr(txtPortLeft.Text)) & "*" & Trim(CStr(txtValueLeft.Text)))
		'Right
		PrintLine(hFile, "RIGHT=" & Trim(txtRight.Text) & "*" & Trim(CStr(txtPortRight.Text)) & "*" & Trim(CStr(txtValueRight.Text)))
		'Buttons
		For	Each oItem In lvButtons.Items
			'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			PrintLine(hFile, "BUTTON=" & Trim(oItem.SubItems(1).Text) & "*" & Trim(oItem.SubItems(2).Text) & "*" & Trim(oItem.SubItems(3).Text))
		Next oItem
		
		'Close file
		FileClose(hFile)
		
		Exit Sub
CMDSAVECLICK_ERROR: 
	End Sub
	
	'Load a joystick configuration
	Private Sub cmdLoad_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLoad.Click

		'Variables
		'		Dim sFileName As String
		'		Dim hFile As Integer
		'		Dim oItem As System.Windows.Forms.ListViewItem
		'		Dim sRow As String
		'		Dim vRow As Object

		'		'Error handling
		'		On Error GoTo CMDLOADCLICK_ERROR

		'		'Initialise
		'		'UPGRADE_WARNING: CommonDialog variable was not upgraded Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="671167DC-EA81-475D-B690-7A40C7BF4A23"'
		'		With comdlgFile
		'			'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'			.CancelError = True
		'			.Title = "Load joystick configuration"
		'			.DefaultExt = ".jcf"
		'			.FileName = ""
		'			'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'			.Filter = "vbSpec joystick configuration file (*.jcf)|*.jcf|"
		'			'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'			.CheckPathExists = True
		'			.CheckPathExists = True
		'			'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'			.OverwritePrompt = True
		'			'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'			'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'			'.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
		'			'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'			'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'			'.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
		'			'UPGRADE_WARNING: MSComDlg.CommonDialog property comdlgFile.Flags was upgraded to comdlgFileOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'			'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'			'.ShowReadOnly = False
		'			'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'			'UPGRADE_ISSUE: MSComDlg.CommonDialog property comdlgFile.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'			'.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
		'			.ShowDialog()
		'		End With
		'		'Get file name
		'		sFileName = comdlgFileOpen.FileName

		'		'Start
		'		hFile = FreeFile
		'		FileOpen(hFile, sFileName, OpenMode.Input)

		'		'Read the first row
		'		Input(hFile, sRow)
		'		'The file must begin with the first row
		'		If sRow = "[vbSpec joystick configuration file 0.01]" Then
		'			'Clear the button list
		'			lvButtons.Items.Clear()
		'			'Scan the whole file
		'			Do While Not EOF(hFile)
		'				'Up
		'				If UCase(VB.Left(sRow, 3)) = "UP=" Then
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					vRow = Split(VB.Right(sRow, Len(sRow) - 3), "*")
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtPortUp.Text = vRow(1)
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtValueUp.Text = vRow(2)
		'				End If
		'				'Down
		'				If UCase(VB.Left(sRow, 5)) = "DOWN=" Then
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					vRow = Split(VB.Right(sRow, Len(sRow) - 5), "*")
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtPortDown.Text = vRow(1)
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtValueDown.Text = vRow(2)
		'				End If
		'				'Left
		'				If UCase(VB.Left(sRow, 5)) = "LEFT=" Then
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					vRow = Split(VB.Right(sRow, Len(sRow) - 5), "*")
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtPortLeft.Text = vRow(1)
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtValueLeft.Text = vRow(2)
		'				End If
		'				'Right
		'				If UCase(VB.Left(sRow, 6)) = "RIGHT=" Then
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					vRow = Split(VB.Right(sRow, Len(sRow) - 6), "*")
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtPortRight.Text = vRow(1)
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					txtValueRight.Text = vRow(2)
		'				End If
		'				'Button
		'				If UCase(VB.Left(sRow, 7)) = "BUTTON=" Then
		'					'UPGRADE_WARNING: Couldn't resolve default property of object vRow. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'					vRow = Split(VB.Right(sRow, Len(sRow) - 7), "*")
		'					If lJoystickNo = 1 Then
		'						If lvButtons.Items.Count < lPCJoystick1Buttons Then
		'							oItem = lvButtons.Items.Add("Button" & Trim(CStr(lvButtons.Items.Count + 1)))
		'							'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		'							'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'							If oItem.SubItems.Count > 1 Then
		'								oItem.SubItems(1).Text = vRow(0)
		'							Else
		'								oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(0)))
		'							End If
		'							'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		'							'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'							If oItem.SubItems.Count > 2 Then
		'								oItem.SubItems(2).Text = vRow(1)
		'							Else
		'								oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(1)))
		'							End If
		'							'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		'							'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'							If oItem.SubItems.Count > 3 Then
		'								oItem.SubItems(3).Text = vRow(2)
		'							Else
		'								oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(2)))
		'							End If
		'						End If
		'					Else
		'						If lvButtons.Items.Count < lPCJoystick2Buttons Then
		'							oItem = lvButtons.Items.Add("Button" & Trim(CStr(lvButtons.Items.Count + 1)))
		'							'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		'							'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'							If oItem.SubItems.Count > 1 Then
		'								oItem.SubItems(1).Text = vRow(0)
		'							Else
		'								oItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(0)))
		'							End If
		'							'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		'							'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'							If oItem.SubItems.Count > 2 Then
		'								oItem.SubItems(2).Text = vRow(1)
		'							Else
		'								oItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(1)))
		'							End If
		'							'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		'							'UPGRADE_WARNING: Couldn't resolve default property of object vRow(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'							If oItem.SubItems.Count > 3 Then
		'								oItem.SubItems(3).Text = vRow(2)
		'							Else
		'								oItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, vRow(2)))
		'							End If
		'						End If
		'					End If
		'				End If
		'				'Read the next row
		'				Input(hFile, sRow)
		'			Loop 
		'		End If

		'		'Close file
		'		FileClose(hFile)

		'		Exit Sub
		'CMDLOADCLICK_ERROR: 
	End Sub
	
	'The selection is changed
	'UPGRADE_ISSUE: MSComctlLib.ListView event lvButtons.ItemClick was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub lvButtons_ItemClick(ByVal Item As System.Windows.Forms.ListViewItem)
		'You can define a button only if it has no definition yet
		'UPGRADE_WARNING: Lower bound of collection Item has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		cmdDefine.Enabled = CBool(Trim(Item.SubItems(2).Text) = vbNullString)
		'You can undefine a button if it has a definition yet
		cmdUndefine.Enabled = Not cmdDefine.Enabled
	End Sub
	
	'Define button
	Private Sub cmdDefine_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDefine.Click
		'Variables
		Dim frmDefButtonWnd As frmDefButton
		'Error handel
		On Error GoTo CMDDEFINECLICK_ERROR
		'Initialise
		frmDefButtonWnd = New frmDefButton
		'UPGRADE_ISSUE: Load statement is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
		'Load(frmDefButtonWnd)
		frmDefButtonWnd.Button = lvButtons.FocusedItem.Index
		'Show form
		frmDefButtonWnd.ShowDialog()
		'If the user wants to save
		If frmDefButtonWnd.OK Then
			'Get changes
			'UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			If lvButtons.FocusedItem.SubItems.Count > 1 Then
				lvButtons.FocusedItem.SubItems(1).Text = Trim(CStr(frmDefButtonWnd.ButtonChar))
			Else
				lvButtons.FocusedItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, Trim(CStr(frmDefButtonWnd.ButtonChar))))
			End If
			'UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			If lvButtons.FocusedItem.SubItems.Count > 2 Then
				lvButtons.FocusedItem.SubItems(2).Text = Trim(CStr(frmDefButtonWnd.ButtonPort))
			Else
				lvButtons.FocusedItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, Trim(CStr(frmDefButtonWnd.ButtonPort))))
			End If
			'UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			If lvButtons.FocusedItem.SubItems.Count > 3 Then
				lvButtons.FocusedItem.SubItems(3).Text = Trim(CStr(frmDefButtonWnd.ButtonValue))
			Else
				lvButtons.FocusedItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, Trim(CStr(frmDefButtonWnd.ButtonValue))))
			End If
			'That's a change
			bEdit = True
		End If
		'Anihilate objects
		If Not frmDefButtonWnd Is Nothing Then
			'UPGRADE_NOTE: Object frmDefButtonWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			frmDefButtonWnd = Nothing
		End If
		'End procedure
		Exit Sub
CMDDEFINECLICK_ERROR: 
		'Report error
		MsgBox(Err.Description, MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
		'Anihilate objects
		If Not frmDefButtonWnd Is Nothing Then
			'UPGRADE_NOTE: Object frmDefButtonWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			frmDefButtonWnd = Nothing
		End If
	End Sub
	
	'Anihilate a button definition
	Private Sub cmdUndefine_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdUndefine.Click
		'Anihilate definition
		'UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		If lvButtons.FocusedItem.SubItems.Count > 1 Then
			lvButtons.FocusedItem.SubItems(1).Text = " "
		Else
			lvButtons.FocusedItem.SubItems.Insert(1, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
		End If
		'UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		If lvButtons.FocusedItem.SubItems.Count > 2 Then
			lvButtons.FocusedItem.SubItems(2).Text = " "
		Else
			lvButtons.FocusedItem.SubItems.Insert(2, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
		End If
		'UPGRADE_WARNING: Lower bound of collection lvButtons.SelectedItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
		If lvButtons.FocusedItem.SubItems.Count > 3 Then
			lvButtons.FocusedItem.SubItems(3).Text = " "
		Else
			lvButtons.FocusedItem.SubItems.Insert(3, New System.Windows.Forms.ListViewItem.ListViewSubItem(Nothing, " "))
		End If
		'Regulate selection
		'UPGRADE_ISSUE: MSComctlLib.ListView event lvButtons.ItemClick was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		lvButtons_ItemClick(lvButtons.FocusedItem)
		'That's a change
		bEdit = True
	End Sub
	
	'Default change hanlder
	Private Sub DefaultChangeHanlder()
		bEdit = True
	End Sub
	
	'Default focus hanlder
	Private Sub DefaultFocusHanlder(ByRef oTextBox As System.Windows.Forms.TextBox)
		If oTextBox.Text <> vbNullString Then
			oTextBox.SelectionStart = 0
			oTextBox.SelectionLength = Len(oTextBox.Text)
		End If
	End Sub
	
	'Define per keystroke
	Private Sub txtUp_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtUp.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If Not bPortValue Then
			bKey = True
			KeyStrokeToPortValue(KeyCode, Shift, sKey, lPort, lValue)
			txtUp.Text = sKey
			txtPortUp.Text = Trim(CStr(lPort))
			txtValueUp.Text = Trim(CStr(lValue))
			bKey = False
		End If
	End Sub
	Private Sub txtDown_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtDown.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If Not bPortValue Then
			bKey = True
			KeyStrokeToPortValue(KeyCode, Shift, sKey, lPort, lValue)
			txtDown.Text = sKey
			txtPortDown.Text = Trim(CStr(lPort))
			txtValueDown.Text = Trim(CStr(lValue))
			bKey = False
		End If
	End Sub
	Private Sub txtLeft_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtLeft.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If Not bPortValue Then
			bKey = True
			KeyStrokeToPortValue(KeyCode, Shift, sKey, lPort, lValue)
			txtLeft.Text = sKey
			txtPortLeft.Text = Trim(CStr(lPort))
			txtValueLeft.Text = Trim(CStr(lValue))
			bKey = False
		End If
	End Sub
	Private Sub txtRight_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtRight.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If Not bPortValue Then
			bKey = True
			KeyStrokeToPortValue(KeyCode, Shift, sKey, lPort, lValue)
			txtRight.Text = sKey
			txtPortRight.Text = Trim(CStr(lPort))
			txtValueRight.Text = Trim(CStr(lValue))
			bKey = False
		End If
	End Sub
	
	'Define per port, in value
	'UPGRADE_WARNING: Event txtPortUp.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtPortUp_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortUp.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortUp.Text) And IsNumeric(txtValueUp.Text)) Then
				txtUp.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortUp.Text), CInt(txtValueUp.Text), sKey)
			txtUp.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtValueUp.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtValueUp_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueUp.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortUp.Text) And IsNumeric(txtValueUp.Text)) Then
				txtUp.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortUp.Text), CInt(txtValueUp.Text), sKey)
			txtUp.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtPortDown.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtPortDown_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortDown.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortDown.Text) And IsNumeric(txtValueDown.Text)) Then
				txtDown.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortDown.Text), CInt(txtValueDown.Text), sKey)
			txtDown.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtValueDown.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtValueDown_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueDown.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortDown.Text) And IsNumeric(txtValueDown.Text)) Then
				txtDown.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortDown.Text), CInt(txtValueDown.Text), sKey)
			txtDown.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtPortLeft.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtPortLeft_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortLeft.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortLeft.Text) And IsNumeric(txtValueLeft.Text)) Then
				txtLeft.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortLeft.Text), CInt(txtValueLeft.Text), sKey)
			txtLeft.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtValueLeft.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtValueLeft_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueLeft.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortLeft.Text) And IsNumeric(txtValueLeft.Text)) Then
				txtLeft.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortLeft.Text), CInt(txtValueLeft.Text), sKey)
			txtLeft.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtPortRight.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtPortRight_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortRight.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortRight.Text) And IsNumeric(txtValueRight.Text)) Then
				txtRight.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortRight.Text), CInt(txtValueRight.Text), sKey)
			txtRight.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	'UPGRADE_WARNING: Event txtValueRight.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtValueRight_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueRight.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortRight.Text) And IsNumeric(txtValueRight.Text)) Then
				txtRight.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortRight.Text), CInt(txtValueRight.Text), sKey)
			txtRight.Text = sKey
			bPortValue = False
		End If
		DefaultChangeHanlder()
	End Sub
	
	'Focus
	Private Sub txtPortUp_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortUp.Enter
		DefaultFocusHanlder(txtPortUp)
	End Sub
	Private Sub txtValueUp_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueUp.Enter
		DefaultFocusHanlder(txtValueUp)
	End Sub
	Private Sub txtPortDown_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortDown.Enter
		DefaultFocusHanlder(txtPortDown)
	End Sub
	Private Sub txtValueDown_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueDown.Enter
		DefaultFocusHanlder(txtValueDown)
	End Sub
	Private Sub txtPortLeft_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortLeft.Enter
		DefaultFocusHanlder(txtPortLeft)
	End Sub
	Private Sub txtValueLeft_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueLeft.Enter
		DefaultFocusHanlder(txtValueLeft)
	End Sub
	Private Sub txtPortRight_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortRight.Enter
		DefaultFocusHanlder(txtPortRight)
	End Sub
	Private Sub txtValueRight_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueRight.Enter
		DefaultFocusHanlder(txtValueRight)
	End Sub
	
	'Validate joystick settings
	'UPGRADE_NOTE: Validate was upgraded to Validate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Function Validate_Renamed() As Boolean
		
		'Variables
		Dim oItem As System.Windows.Forms.ListViewItem
		
		'Preset
		Validate_Renamed = False
		
		'All values must be numeric
		If Not IsNumeric(txtPortUp.Text) Then
			MsgBox("Please enter a numeric value for the up port.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortUp.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtPortDown.Text) Then
			MsgBox("Please enter a numeric value for the down port.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortDown.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtPortLeft.Text) Then
			MsgBox("Please enter a numeric value for the left port.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortLeft.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtPortRight.Text) Then
			MsgBox("Please enter a numeric value for the right port.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortRight.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtValueUp.Text) Then
			MsgBox("Please enter a numeric value for the up in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueUp.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtValueDown.Text) Then
			MsgBox("Please enter a numeric value for the down in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueDown.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtValueLeft.Text) Then
			MsgBox("Please enter a numeric value for the left in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueLeft.Focus()
			Exit Function
		End If
		If Not IsNumeric(txtValueRight.Text) Then
			MsgBox("Please enter a numeric value for the right in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueRight.Focus()
			Exit Function
		End If
		
		'All port values must be between 0 and 65535
		If CInt(txtPortUp.Text) < 0 Or CInt(txtPortUp.Text) > 65535 Then
			MsgBox("Please enter a value between 0 and 65535 for the up port value.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortUp.Focus()
			Exit Function
		End If
		If CInt(txtPortDown.Text) < 0 Or CInt(txtPortDown.Text) > 65535 Then
			MsgBox("Please enter a value between 0 and 65535 for the down port value.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortDown.Focus()
			Exit Function
		End If
		If CInt(txtPortLeft.Text) < 0 Or CInt(txtPortLeft.Text) > 65535 Then
			MsgBox("Please enter a value between 0 and 65535 for the left port value.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortLeft.Focus()
			Exit Function
		End If
		If CInt(txtPortRight.Text) < 0 Or CInt(txtPortRight.Text) > 65535 Then
			MsgBox("Please enter a value between 0 and 65535 for the right port value.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtPortRight.Focus()
			Exit Function
		End If
		
		'All in values must be between 0 and 255
		If CInt(txtValueUp.Text) < 0 Or CInt(txtValueUp.Text) > 255 Then
			MsgBox("Please enter a value between 0 and 255 for the up in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueUp.Focus()
			Exit Function
		End If
		If CInt(txtValueDown.Text) < 0 Or CInt(txtValueDown.Text) > 255 Then
			MsgBox("Please enter a value between 0 and 255 for the down in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueDown.Focus()
			Exit Function
		End If
		If CInt(txtValueLeft.Text) < 0 Or CInt(txtValueLeft.Text) > 255 Then
			MsgBox("Please enter a value between 0 and 255 for the left in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueLeft.Focus()
			Exit Function
		End If
		If CInt(txtValueRight.Text) < 0 Or CInt(txtValueRight.Text) > 255 Then
			MsgBox("Please enter a value between 0 and 255 for the right in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
			txtValueRight.Focus()
			Exit Function
		End If
		
		'Validate list items
		For	Each oItem In lvButtons.Items
			'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
			If (Trim(oItem.SubItems(2).Text) <> vbNullString) And (Trim(oItem.SubItems(3).Text) <> vbNullString) Then
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				If Not IsNumeric(oItem.SubItems(2).Text) Then
					MsgBox("Please enter numeric value for button " & Trim(CStr(oItem.Index)) & " port.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
					lvButtons.FocusedItem = oItem
					Exit Function
				End If
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				If Not IsNumeric(oItem.SubItems(3).Text) Then
					MsgBox("Please enter numeric value for button " & Trim(CStr(oItem.Index)) & " in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
					lvButtons.FocusedItem = oItem
					Exit Function
				End If
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				If CInt(oItem.SubItems(2).Text) < 0 Or CInt(oItem.SubItems(2).Text) > 65535 Then
					MsgBox("Please enter a value between 0 and 65535 for button " & Trim(CStr(oItem.Index)) & " port.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
					lvButtons.FocusedItem = oItem
					Exit Function
				End If
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				If Not IsNumeric(oItem.SubItems(3).Text) Then
					MsgBox("Please enter numeric value for button " & Trim(CStr(oItem.Index)) & " in.", MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
					lvButtons.FocusedItem = oItem
					Exit Function
				End If
			Else
				'UPGRADE_WARNING: Lower bound of collection oItem has changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A3B628A0-A810-4AE2-BFA2-9E7A29EB9AD0"'
				If (Trim(oItem.SubItems(2).Text) <> vbNullString) Or (Trim(oItem.SubItems(3).Text) <> vbNullString) Then
					MsgBox("You must enter a port number and an in value to define a button.")
					lvButtons.FocusedItem = oItem
					Exit Function
				End If
			End If
		Next oItem
		
		'That shoul be OK
		Validate_Renamed = True
	End Function
End Class