<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMainWnd
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents _mnuFile_1 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_2 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_3 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_4 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_5 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_6 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_7 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_8 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_9 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_10 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_11 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_12 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_13 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_14 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_15 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_16 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuFile_17 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFileMain As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_1 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_2 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_3 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_4 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_5 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_6 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_7 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuOptions_8 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuOptionsMain As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuHelp_1 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuHelp_2 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents _mnuHelp_3 As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuHelpMain As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFullScreenFile As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFullScreenOptions As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFullScreenHelp As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFullNormalView As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents mnuFullScreenMenu As System.Windows.Forms.ToolStripMenuItem
	Public WithEvents MainMenu1 As System.Windows.Forms.MenuStrip
	Public WithEvents _Line1_3 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents _Line1_2 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblStatusMsg As System.Windows.Forms.Label
	Public WithEvents picStatus As System.Windows.Forms.Panel
	Public dlgCommonOpen As System.Windows.Forms.OpenFileDialog
	Public dlgCommonSave As System.Windows.Forms.SaveFileDialog
	Public WithEvents picDisplay As System.Windows.Forms.PictureBox
	Public WithEvents _Line1_1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents _Line1_0 As Microsoft.VisualBasic.PowerPacks.LineShape
	'Public WithEvents Line1 As LineShapeArray
	Public WithEvents mnuFile As Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray
	Public WithEvents mnuHelp As Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray
	Public WithEvents mnuOptions As Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray
	Public WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMainWnd))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.MainMenu1 = New System.Windows.Forms.MenuStrip
		Me.mnuFileMain = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_1 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_2 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_3 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_4 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_5 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_6 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_7 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_8 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_9 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_10 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_11 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_12 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_13 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_14 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_15 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_16 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuFile_17 = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuOptionsMain = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_1 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_2 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_3 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_4 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_5 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_6 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_7 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuOptions_8 = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuHelpMain = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuHelp_1 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuHelp_2 = New System.Windows.Forms.ToolStripMenuItem
		Me._mnuHelp_3 = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFullScreenMenu = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFullScreenFile = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFullScreenOptions = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFullScreenHelp = New System.Windows.Forms.ToolStripMenuItem
		Me.mnuFullNormalView = New System.Windows.Forms.ToolStripMenuItem
		Me.picStatus = New System.Windows.Forms.Panel
		Me._Line1_3 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me._Line1_2 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblStatusMsg = New System.Windows.Forms.Label
		Me.dlgCommonOpen = New System.Windows.Forms.OpenFileDialog
		Me.dlgCommonSave = New System.Windows.Forms.SaveFileDialog
		Me.picDisplay = New System.Windows.Forms.PictureBox
		Me._Line1_1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me._Line1_0 = New Microsoft.VisualBasic.PowerPacks.LineShape
		'Me.Line1 = New LineShapeArray(components)
		Me.mnuFile = New Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components)
		Me.mnuHelp = New Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components)
		Me.mnuOptions = New Microsoft.VisualBasic.Compatibility.VB6.ToolStripMenuItemArray(components)
		Me.MainMenu1.SuspendLayout()
		Me.picStatus.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		'CType(Me.Line1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.mnuFile, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.mnuHelp, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.mnuOptions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Text = "vbSpec"
		Me.ClientSize = New System.Drawing.Size(281, 266)
		Me.Location = New System.Drawing.Point(10, 48)
		Me.ControlBox = False
		Me.Icon = CType(resources.GetObject("frmMainWnd.Icon"), System.Drawing.Icon)
		Me.KeyPreview = True
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.Enabled = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmMainWnd"
		Me.mnuFileMain.Name = "mnuFileMain"
		Me.mnuFileMain.Text = "&File"
		Me.mnuFileMain.Checked = False
		Me.mnuFileMain.Enabled = True
		Me.mnuFileMain.Visible = True
		Me._mnuFile_1.Name = "_mnuFile_1"
		Me._mnuFile_1.Text = "&Open..."
		Me._mnuFile_1.Checked = False
		Me._mnuFile_1.Enabled = True
		Me._mnuFile_1.Visible = True
		Me._mnuFile_2.Name = "_mnuFile_2"
		Me._mnuFile_2.Text = "&Save As..."
		Me._mnuFile_2.Checked = False
		Me._mnuFile_2.Enabled = True
		Me._mnuFile_2.Visible = True
		Me._mnuFile_3.Enabled = True
		Me._mnuFile_3.Visible = True
		Me._mnuFile_3.Name = "_mnuFile_3"
		Me._mnuFile_4.Name = "_mnuFile_4"
		Me._mnuFile_4.Text = "&Create blank tape file for saving..."
		Me._mnuFile_4.Checked = False
		Me._mnuFile_4.Enabled = True
		Me._mnuFile_4.Visible = True
		Me._mnuFile_5.Enabled = True
		Me._mnuFile_5.Visible = True
		Me._mnuFile_5.Name = "_mnuFile_5"
		Me._mnuFile_6.Name = "_mnuFile_6"
		Me._mnuFile_6.Text = "Load &Binary..."
		Me._mnuFile_6.Checked = False
		Me._mnuFile_6.Enabled = True
		Me._mnuFile_6.Visible = True
		Me._mnuFile_7.Name = "_mnuFile_7"
		Me._mnuFile_7.Text = "Save B&inary..."
		Me._mnuFile_7.Checked = False
		Me._mnuFile_7.Enabled = True
		Me._mnuFile_7.Visible = True
		Me._mnuFile_8.Enabled = True
		Me._mnuFile_8.Visible = True
		Me._mnuFile_8.Name = "_mnuFile_8"
		Me._mnuFile_9.Name = "_mnuFile_9"
		Me._mnuFile_9.Text = "&Reset Spectrum"
		Me._mnuFile_9.Checked = False
		Me._mnuFile_9.Enabled = True
		Me._mnuFile_9.Visible = True
		Me._mnuFile_10.Visible = False
		Me._mnuFile_10.Enabled = True
		Me._mnuFile_10.Name = "_mnuFile_10"
		Me._mnuFile_11.Name = "_mnuFile_11"
		Me._mnuFile_11.Text = "&1"
		Me._mnuFile_11.Visible = False
		Me._mnuFile_11.Checked = False
		Me._mnuFile_11.Enabled = True
		Me._mnuFile_12.Name = "_mnuFile_12"
		Me._mnuFile_12.Text = "&2"
		Me._mnuFile_12.Visible = False
		Me._mnuFile_12.Checked = False
		Me._mnuFile_12.Enabled = True
		Me._mnuFile_13.Name = "_mnuFile_13"
		Me._mnuFile_13.Text = "&3"
		Me._mnuFile_13.Visible = False
		Me._mnuFile_13.Checked = False
		Me._mnuFile_13.Enabled = True
		Me._mnuFile_14.Name = "_mnuFile_14"
		Me._mnuFile_14.Text = "&4"
		Me._mnuFile_14.Visible = False
		Me._mnuFile_14.Checked = False
		Me._mnuFile_14.Enabled = True
		Me._mnuFile_15.Name = "_mnuFile_15"
		Me._mnuFile_15.Text = "&5"
		Me._mnuFile_15.Visible = False
		Me._mnuFile_15.Checked = False
		Me._mnuFile_15.Enabled = True
		Me._mnuFile_16.Enabled = True
		Me._mnuFile_16.Visible = True
		Me._mnuFile_16.Name = "_mnuFile_16"
		Me._mnuFile_17.Name = "_mnuFile_17"
		Me._mnuFile_17.Text = "E&xit"
		Me._mnuFile_17.Checked = False
		Me._mnuFile_17.Enabled = True
		Me._mnuFile_17.Visible = True
		Me.mnuOptionsMain.Name = "mnuOptionsMain"
		Me.mnuOptionsMain.Text = "&Options"
		Me.mnuOptionsMain.Checked = False
		Me.mnuOptionsMain.Enabled = True
		Me.mnuOptionsMain.Visible = True
		Me._mnuOptions_1.Name = "_mnuOptions_1"
		Me._mnuOptions_1.Text = "&General Settings..."
		Me._mnuOptions_1.Checked = False
		Me._mnuOptions_1.Enabled = True
		Me._mnuOptions_1.Visible = True
		Me._mnuOptions_2.Name = "_mnuOptions_2"
		Me._mnuOptions_2.Text = "&Display..."
		Me._mnuOptions_2.Checked = False
		Me._mnuOptions_2.Enabled = True
		Me._mnuOptions_2.Visible = True
		Me._mnuOptions_3.Enabled = True
		Me._mnuOptions_3.Visible = True
		Me._mnuOptions_3.Name = "_mnuOptions_3"
		Me._mnuOptions_4.Name = "_mnuOptions_4"
		Me._mnuOptions_4.Text = "&Tape Controls..."
		Me._mnuOptions_4.Checked = False
		Me._mnuOptions_4.Enabled = True
		Me._mnuOptions_4.Visible = True
		Me._mnuOptions_5.Name = "_mnuOptions_5"
		Me._mnuOptions_5.Text = "ZX &Printer..."
		Me._mnuOptions_5.Checked = False
		Me._mnuOptions_5.Enabled = True
		Me._mnuOptions_5.Visible = True
		Me._mnuOptions_6.Enabled = True
		Me._mnuOptions_6.Visible = True
		Me._mnuOptions_6.Name = "_mnuOptions_6"
		Me._mnuOptions_7.Name = "_mnuOptions_7"
		Me._mnuOptions_7.Text = "&YZ Switch"
		Me._mnuOptions_7.Checked = False
		Me._mnuOptions_7.Enabled = True
		Me._mnuOptions_7.Visible = True
		Me._mnuOptions_8.Name = "_mnuOptions_8"
		Me._mnuOptions_8.Text = "&Poke memory"
		Me._mnuOptions_8.Checked = False
		Me._mnuOptions_8.Enabled = True
		Me._mnuOptions_8.Visible = True
		Me.mnuHelpMain.Name = "mnuHelpMain"
		Me.mnuHelpMain.Text = "&Help"
		Me.mnuHelpMain.Checked = False
		Me.mnuHelpMain.Enabled = True
		Me.mnuHelpMain.Visible = True
		Me._mnuHelp_1.Name = "_mnuHelp_1"
		Me._mnuHelp_1.Text = "Spectrum &Keyboard..."
		Me._mnuHelp_1.Checked = False
		Me._mnuHelp_1.Enabled = True
		Me._mnuHelp_1.Visible = True
		Me._mnuHelp_2.Enabled = True
		Me._mnuHelp_2.Visible = True
		Me._mnuHelp_2.Name = "_mnuHelp_2"
		Me._mnuHelp_3.Name = "_mnuHelp_3"
		Me._mnuHelp_3.Text = "&About vbSpec..."
		Me._mnuHelp_3.Checked = False
		Me._mnuHelp_3.Enabled = True
		Me._mnuHelp_3.Visible = True
		Me.mnuFullScreenMenu.Name = "mnuFullScreenMenu"
		Me.mnuFullScreenMenu.Text = "Full screen mode menu"
		Me.mnuFullScreenMenu.Visible = False
		Me.mnuFullScreenMenu.Checked = False
		Me.mnuFullScreenMenu.Enabled = True
		Me.mnuFullScreenFile.Name = "mnuFullScreenFile"
		Me.mnuFullScreenFile.Text = "File..."
		Me.mnuFullScreenFile.Checked = False
		Me.mnuFullScreenFile.Enabled = True
		Me.mnuFullScreenFile.Visible = True
		Me.mnuFullScreenOptions.Name = "mnuFullScreenOptions"
		Me.mnuFullScreenOptions.Text = "Options..."
		Me.mnuFullScreenOptions.Checked = False
		Me.mnuFullScreenOptions.Enabled = True
		Me.mnuFullScreenOptions.Visible = True
		Me.mnuFullScreenHelp.Name = "mnuFullScreenHelp"
		Me.mnuFullScreenHelp.Text = "Help..."
		Me.mnuFullScreenHelp.Checked = False
		Me.mnuFullScreenHelp.Enabled = True
		Me.mnuFullScreenHelp.Visible = True
		Me.mnuFullNormalView.Name = "mnuFullNormalView"
		Me.mnuFullNormalView.Text = "Normal view!"
		Me.mnuFullNormalView.Checked = False
		Me.mnuFullNormalView.Enabled = True
		Me.mnuFullNormalView.Visible = True
		Me.picStatus.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.picStatus.Size = New System.Drawing.Size(281, 22)
		Me.picStatus.Location = New System.Drawing.Point(0, 244)
		Me.picStatus.TabIndex = 1
		Me.picStatus.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.picStatus.BackColor = System.Drawing.SystemColors.Control
		Me.picStatus.CausesValidation = True
		Me.picStatus.Enabled = True
		Me.picStatus.ForeColor = System.Drawing.SystemColors.ControlText
		Me.picStatus.Cursor = System.Windows.Forms.Cursors.Default
		Me.picStatus.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picStatus.TabStop = True
		Me.picStatus.Visible = True
		Me.picStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picStatus.Name = "picStatus"
		Me._Line1_3.BorderColor = System.Drawing.SystemColors.ControlDark
		Me._Line1_3.X1 = 0
		Me._Line1_3.X2 = 11884
		Me._Line1_3.Y1 = 0
		Me._Line1_3.Y2 = 0
		Me._Line1_3.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me._Line1_3.BorderWidth = 1
		Me._Line1_3.Visible = True
		Me._Line1_3.Name = "_Line1_3"
		Me._Line1_2.BorderColor = System.Drawing.SystemColors.ControlLight
		Me._Line1_2.X1 = 0
		Me._Line1_2.X2 = 11884
		Me._Line1_2.Y1 = 1
		Me._Line1_2.Y2 = 1
		Me._Line1_2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me._Line1_2.BorderWidth = 1
		Me._Line1_2.Visible = True
		Me._Line1_2.Name = "_Line1_2"
		Me.lblStatusMsg.Text = "vbSpec"
		Me.lblStatusMsg.Size = New System.Drawing.Size(269, 17)
		Me.lblStatusMsg.Location = New System.Drawing.Point(4, 6)
		Me.lblStatusMsg.TabIndex = 2
		Me.lblStatusMsg.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblStatusMsg.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblStatusMsg.BackColor = System.Drawing.SystemColors.Control
		Me.lblStatusMsg.Enabled = True
		Me.lblStatusMsg.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblStatusMsg.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblStatusMsg.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblStatusMsg.UseMnemonic = True
		Me.lblStatusMsg.Visible = True
		Me.lblStatusMsg.AutoSize = False
		Me.lblStatusMsg.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblStatusMsg.Name = "lblStatusMsg"
		Me.picDisplay.BackColor = System.Drawing.SystemColors.Window
		Me.picDisplay.ForeColor = System.Drawing.SystemColors.WindowText
		Me.picDisplay.Size = New System.Drawing.Size(256, 192)
		Me.picDisplay.Location = New System.Drawing.Point(12, 40)
		Me.picDisplay.TabIndex = 0
		Me.picDisplay.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.picDisplay.Dock = System.Windows.Forms.DockStyle.None
		Me.picDisplay.CausesValidation = True
		Me.picDisplay.Enabled = True
		Me.picDisplay.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picDisplay.TabStop = True
		Me.picDisplay.Visible = True
		Me.picDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.picDisplay.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picDisplay.Name = "picDisplay"
		Me._Line1_1.BorderColor = System.Drawing.SystemColors.ControlLight
		Me._Line1_1.X1 = 0
		Me._Line1_1.X2 = 792
		Me._Line1_1.Y1 = 1
		Me._Line1_1.Y2 = 1
		Me._Line1_1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me._Line1_1.BorderWidth = 1
		Me._Line1_1.Visible = True
		Me._Line1_1.Name = "_Line1_1"
		Me._Line1_0.BorderColor = System.Drawing.SystemColors.ControlDark
		Me._Line1_0.X1 = 0
		Me._Line1_0.X2 = 792
		Me._Line1_0.Y1 = 0
		Me._Line1_0.Y2 = 0
		Me._Line1_0.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me._Line1_0.BorderWidth = 1
		Me._Line1_0.Visible = True
		Me._Line1_0.Name = "_Line1_0"
		Me.Controls.Add(picStatus)
		Me.Controls.Add(picDisplay)
		Me.ShapeContainer1.Shapes.Add(_Line1_1)
		Me.ShapeContainer1.Shapes.Add(_Line1_0)
		Me.Controls.Add(ShapeContainer1)
		Me.ShapeContainer2.Shapes.Add(_Line1_3)
		Me.ShapeContainer2.Shapes.Add(_Line1_2)
		Me.picStatus.Controls.Add(lblStatusMsg)
		Me.picStatus.Controls.Add(ShapeContainer2)
		'Me.Line1.SetIndex(_Line1_3, CType(3, Short))
		'Me.Line1.SetIndex(_Line1_2, CType(2, Short))
		'Me.Line1.SetIndex(_Line1_1, CType(1, Short))
		'Me.Line1.SetIndex(_Line1_0, CType(0, Short))
		Me.mnuFile.SetIndex(_mnuFile_1, CType(1, Short))
		Me.mnuFile.SetIndex(_mnuFile_2, CType(2, Short))
		Me.mnuFile.SetIndex(_mnuFile_3, CType(3, Short))
		Me.mnuFile.SetIndex(_mnuFile_4, CType(4, Short))
		Me.mnuFile.SetIndex(_mnuFile_5, CType(5, Short))
		Me.mnuFile.SetIndex(_mnuFile_6, CType(6, Short))
		Me.mnuFile.SetIndex(_mnuFile_7, CType(7, Short))
		Me.mnuFile.SetIndex(_mnuFile_8, CType(8, Short))
		Me.mnuFile.SetIndex(_mnuFile_9, CType(9, Short))
		Me.mnuFile.SetIndex(_mnuFile_10, CType(10, Short))
		Me.mnuFile.SetIndex(_mnuFile_11, CType(11, Short))
		Me.mnuFile.SetIndex(_mnuFile_12, CType(12, Short))
		Me.mnuFile.SetIndex(_mnuFile_13, CType(13, Short))
		Me.mnuFile.SetIndex(_mnuFile_14, CType(14, Short))
		Me.mnuFile.SetIndex(_mnuFile_15, CType(15, Short))
		Me.mnuFile.SetIndex(_mnuFile_16, CType(16, Short))
		Me.mnuFile.SetIndex(_mnuFile_17, CType(17, Short))
		Me.mnuHelp.SetIndex(_mnuHelp_1, CType(1, Short))
		Me.mnuHelp.SetIndex(_mnuHelp_2, CType(2, Short))
		Me.mnuHelp.SetIndex(_mnuHelp_3, CType(3, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_1, CType(1, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_2, CType(2, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_3, CType(3, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_4, CType(4, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_5, CType(5, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_6, CType(6, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_7, CType(7, Short))
		Me.mnuOptions.SetIndex(_mnuOptions_8, CType(8, Short))
		CType(Me.mnuOptions, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.mnuHelp, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.mnuFile, System.ComponentModel.ISupportInitialize).EndInit()
		'CType(Me.Line1, System.ComponentModel.ISupportInitialize).EndInit()
		MainMenu1.Items.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuFileMain, Me.mnuOptionsMain, Me.mnuHelpMain, Me.mnuFullScreenMenu})
		mnuFileMain.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me._mnuFile_1, Me._mnuFile_2, Me._mnuFile_3, Me._mnuFile_4, Me._mnuFile_5, Me._mnuFile_6, Me._mnuFile_7, Me._mnuFile_8, Me._mnuFile_9, Me._mnuFile_10, Me._mnuFile_11, Me._mnuFile_12, Me._mnuFile_13, Me._mnuFile_14, Me._mnuFile_15, Me._mnuFile_16, Me._mnuFile_17})
		mnuOptionsMain.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me._mnuOptions_1, Me._mnuOptions_2, Me._mnuOptions_3, Me._mnuOptions_4, Me._mnuOptions_5, Me._mnuOptions_6, Me._mnuOptions_7, Me._mnuOptions_8})
		mnuHelpMain.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me._mnuHelp_1, Me._mnuHelp_2, Me._mnuHelp_3})
		mnuFullScreenMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem(){Me.mnuFullScreenFile, Me.mnuFullScreenOptions, Me.mnuFullScreenHelp, Me.mnuFullNormalView})
		Me.Controls.Add(MainMenu1)
		Me.MainMenu1.ResumeLayout(False)
		Me.picStatus.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class