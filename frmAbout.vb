Option Strict Off
Option Explicit On
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmAbout
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmAbout.frm within vbSpec.vbp
	'
	'   "About..." dialog for vbSpec
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2000 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Me.Close()
	End Sub
	
	Private Sub frmAbout_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		lblVerInfo.Text = "Version " & My.Application.Info.Version.Major & "." & VB6.Format(My.Application.Info.Version.Minor, "00") & "." & VB6.Format(My.Application.Info.Version.Revision, "0000")
	End Sub
	
	Private Sub Image1_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles Image1.MouseMove
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		lblWebSite.Font = VB6.FontChangeUnderline(lblWebSite.Font, False)
	End Sub


	Private Sub lblStatic_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles lblStatic.MouseMove
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		Dim Index As Short = lblStatic.GetIndex(eventSender)
		lblWebSite.Font = VB6.FontChangeUnderline(lblWebSite.Font, False)
	End Sub


	Private Sub lblVerInfo_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles lblVerInfo.MouseMove
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		lblWebSite.Font = VB6.FontChangeUnderline(lblWebSite.Font, False)
	End Sub


	Private Sub lblWebSite_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lblWebSite.Click
		ShellExecute(0, "open", "http://www.muhi.org/vbspec/", vbNullString, vbNullString, 0)
	End Sub



	Private Sub lblWebSite_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles lblWebSite.MouseMove
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		lblWebSite.Font = VB6.FontChangeUnderline(lblWebSite.Font, True)
	End Sub


	Private Sub frmAbout_MouseMove(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		lblWebSite.Font = VB6.FontChangeUnderline(lblWebSite.Font, False)
	End Sub
End Class