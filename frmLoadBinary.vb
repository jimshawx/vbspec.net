Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmLoadBinary
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmLoadBinary.frm within vbSpec.vbp
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2000 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Hide()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		Dim lAddr, h As Integer
		Dim s As String
		
		On Error Resume Next
		
		If VB.Left(txtAddr.Text, 1) = "$" Then
			lAddr = Val("&H" & Mid(txtAddr.Text, 2) & "&")
		ElseIf cboBase.Text = "Hex" Then 
			lAddr = Val("&H" & txtAddr.Text & "&")
		Else
			lAddr = Val(txtAddr.Text)
		End If
		
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Dir(txtFile.Text) = "" Then
			MsgBox(txtFile.Text & vbCrLf & vbCrLf & "File not found.", MsgBoxStyle.OKOnly Or MsgBoxStyle.Exclamation, "vbSpec")
			Exit Sub
		Else
			err.Clear()
			h = FreeFile
			FileOpen(h, txtFile.Text, OpenMode.Binary)
			
			If Err.Number = 76 Then
				FileClose(h)
				MsgBox(txtFile.Text & vbCrLf & vbCrLf & "File not found.", MsgBoxStyle.OKOnly Or MsgBoxStyle.Exclamation, "vbSpec")
				Exit Sub
			End If
			
			If LOF(h) + lAddr > 65536 Then
				If MsgBox(txtFile.Text & vbCrLf & vbCrLf & "File will overrun the 64K upper boundary." & vbCrLf & "Do you want to load the first " & CStr(65536 - lAddr) & " bytes?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, "vbSpec") = MsgBoxResult.No Then
					FileClose(h)
					Exit Sub
				End If
			End If

			' // Load as many bytes as will fit into the Z80 memory space
			If LOF(h) > 65536 Then s = InputString16(h, 65536) Else s = InputString16(h, LOF(h))
			If lAddr + Len(s) > 65536 Then s = VB.Left(s, 65536 - lAddr)
			FileClose(h)
			
			h = 1
			Do 
				gRAMPage(glPageAt(glMemAddrDiv16384(lAddr)), lAddr And 16383) = Asc(Mid(s, h, 1))
				lAddr = lAddr + 1
				h = h + 1
			Loop Until h > Len(s)
		End If
		
		Me.Hide()
		
		'MM 23.04.2003
		'Bugfix - you will see instantly if you binary load a screen
		initscreen()
		screenPaint()
	End Sub
	
	Private Sub cmdOpen_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOpen.Click
		On Error Resume Next
		
		err.Clear()
		dlgCommonOpen.Title = "Open Binary File"
		dlgCommonOpen.DefaultExt = ".bin"
		dlgCommonOpen.FileName = ""
		'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		dlgCommonOpen.Filter = "All Files|*.*"
		'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.CheckFileExists which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		dlgCommonOpen.CheckFileExists = True
		dlgCommonOpen.CheckPathExists = True
		'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
		'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
		'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'dlgCommon.CancelError = True
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Dir(txtFile.Text) <> "" Then
			dlgCommonOpen.InitialDirectory = txtFile.Text
		End If
		
		dlgCommonOpen.ShowDialog()
		'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		If Err.Number = DialogResult.Cancel Then
			Exit Sub
		End If
		
		If dlgCommonOpen.FileName <> "" Then
			txtFile.Text = dlgCommonOpen.FileName
		End If
	End Sub
	
	Private Sub frmLoadBinary_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		cboBase.Items.Add("Decimal")
		cboBase.Items.Add("Hex")
		
		cboBase.SelectedIndex = 0
	End Sub
	
	
	Private Sub txtAddr_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles txtAddr.KeyPress
		Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
		If KeyAscii >= 97 And KeyAscii <= 102 Then KeyAscii = KeyAscii - 32
		If KeyAscii > 70 Then KeyAscii = 0
		If KeyAscii > 57 And KeyAscii < 65 Then KeyAscii = 0
		
		If KeyAscii > 64 Then cboBase.SelectedIndex = 1
		eventArgs.KeyChar = Chr(KeyAscii)
		If KeyAscii = 0 Then
			eventArgs.Handled = True
		End If
	End Sub
End Class