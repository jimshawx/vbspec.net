Option Strict Off
Option Explicit On
Friend Class frmTapePlayer
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmTapePlayer.frm within vbSpec.vbp
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2000 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	Public Sub UpdateCurBlock()
		If lstBlocks.Items.Count > TZXCurBlock Then lstBlocks.SelectedIndex = TZXCurBlock
	End Sub
	
	Public Sub UpdateTapeList()
		Dim l As Integer
		Dim lType, lLen As Integer
		Dim sText As String
		
		lstBlocks.Items.Clear()
		
		For l = 0 To TZXNumBlocks - 1
			GetTZXBlockInfo(l, lType, sText, lLen)
			lstBlocks.Items.Add(Hex(lType) & " - " & sText)
		Next l
		
		If lstBlocks.Items.Count > TZXCurBlock Then lstBlocks.SelectedIndex = TZXCurBlock
	End Sub
	
	Private Sub cmdNext_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdNext.Click
		If (gbTZXInserted) And (TZXCurBlock < TZXNumBlocks - 1) Then
			SetCurTZXBlock(TZXCurBlock + 1)
		End If
		frmMainWnd.Activate()
	End Sub
	
	Private Sub cmdPlay_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPlay.Click
		If gbTZXInserted Then StartTape()
		frmMainWnd.Activate()
	End Sub
	
	Private Sub cmdPrev_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdPrev.Click
		If (gbTZXInserted) And (TZXCurBlock > 0) Then
			SetCurTZXBlock(TZXCurBlock - 1)
		End If
		frmMainWnd.Activate()
	End Sub
	
	Private Sub cmdRewind_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRewind.Click
		If gbTZXInserted Then SetCurTZXBlock(0)
		frmMainWnd.Activate()
	End Sub
	
	
	Private Sub cmdStop_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdStop.Click
		If gbTZXInserted Then StopTape()
		frmMainWnd.Activate()
	End Sub
	
	
	'UPGRADE_WARNING: Form event frmTapePlayer.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmTapePlayer_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		UpdateCurBlock()
	End Sub
	
	Private Sub frmTapePlayer_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If (Shift And 4) Then Exit Sub
		doKey(True, KeyCode, Shift)
	End Sub
	
	
	Private Sub frmTapePlayer_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		doKey(False, KeyCode, Shift)
	End Sub
	
	Private Sub frmTapePlayer_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim X, y As Integer
		
		X = Val(GetSetting("Grok", "vbSpec", "TapeWndX", "-1"))
		y = Val(GetSetting("Grok", "vbSpec", "TapeWndY", "-1"))

		If X >= 0 And X <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - 16 Then
			Me.Left = X
		End If
		If y >= 0 And y <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - 16 Then
			Me.Top = y
		End If
	End Sub


	Private Sub frmTapePlayer_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		SaveSetting("Grok", "vbSpec", "TapeWndX", CStr(Me.Left))
		SaveSetting("Grok", "vbSpec", "TapeWndY", CStr(Me.Top))

		frmMainWnd.mnuOptions(4).Checked = False
	End Sub
	
	'UPGRADE_WARNING: Event lstBlocks.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub lstBlocks_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lstBlocks.SelectedIndexChanged
		UpdateCurBlock()
	End Sub
End Class