<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmAbout
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents lblWebSite As System.Windows.Forms.Label
	Public WithEvents _lblStatic_2 As System.Windows.Forms.Label
	Public WithEvents _Line1_0 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents _Line1_1 As Microsoft.VisualBasic.PowerPacks.LineShape
	Public WithEvents lblVerInfo As System.Windows.Forms.Label
	Public WithEvents _lblStatic_0 As System.Windows.Forms.Label
	Public WithEvents Image1 As System.Windows.Forms.PictureBox
	'Public WithEvents Line1 As LineShapeArray
	Public WithEvents lblStatic As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmAbout))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer
		Me.cmdOK = New System.Windows.Forms.Button
		Me.lblWebSite = New System.Windows.Forms.Label
		Me._lblStatic_2 = New System.Windows.Forms.Label
		Me._Line1_0 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me._Line1_1 = New Microsoft.VisualBasic.PowerPacks.LineShape
		Me.lblVerInfo = New System.Windows.Forms.Label
		Me._lblStatic_0 = New System.Windows.Forms.Label
		Me.Image1 = New System.Windows.Forms.PictureBox
		'Me.Line1 = New LineShapeArray(components)
		Me.lblStatic = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		'CType(Me.Line1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.lblStatic, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "About vbSpec"
		Me.ClientSize = New System.Drawing.Size(316, 188)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmAbout"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdOK
		Me.cmdOK.Text = "OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(73, 25)
		Me.cmdOK.Location = New System.Drawing.Point(237, 156)
		Me.cmdOK.TabIndex = 4
		Me.cmdOK.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.lblWebSite.Text = "http://www.muhi.org/vbspec/"
		Me.lblWebSite.ForeColor = System.Drawing.SystemColors.Highlight
		Me.lblWebSite.Size = New System.Drawing.Size(151, 17)
		Me.lblWebSite.Location = New System.Drawing.Point(96, 52)
		Me.lblWebSite.TabIndex = 3
		Me.lblWebSite.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblWebSite.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblWebSite.BackColor = System.Drawing.SystemColors.Control
		Me.lblWebSite.Enabled = True
		Me.lblWebSite.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblWebSite.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblWebSite.UseMnemonic = True
		Me.lblWebSite.Visible = True
		Me.lblWebSite.AutoSize = False
		Me.lblWebSite.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblWebSite.Name = "lblWebSite"
		Me._lblStatic_2.Text = "vbSpec comes with ABSOLUTELY NO WARRANTY. This is free software and you are welcome to redistribute it under certain conditions; see the README.TXT file which accompanies this distribution for details."
		Me._lblStatic_2.Size = New System.Drawing.Size(261, 73)
		Me._lblStatic_2.Location = New System.Drawing.Point(52, 76)
		Me._lblStatic_2.TabIndex = 2
		Me._lblStatic_2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblStatic_2.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblStatic_2.BackColor = System.Drawing.SystemColors.Control
		Me._lblStatic_2.Enabled = True
		Me._lblStatic_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblStatic_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblStatic_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblStatic_2.UseMnemonic = True
		Me._lblStatic_2.Visible = True
		Me._lblStatic_2.AutoSize = False
		Me._lblStatic_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblStatic_2.Name = "_lblStatic_2"
		Me._Line1_0.BorderColor = System.Drawing.SystemColors.ControlDark
		Me._Line1_0.X1 = 48
		Me._Line1_0.X2 = 616
		Me._Line1_0.Y1 = 44
		Me._Line1_0.Y2 = 44
		Me._Line1_0.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me._Line1_0.BorderWidth = 1
		Me._Line1_0.Visible = True
		Me._Line1_0.Name = "_Line1_0"
		Me._Line1_1.BorderColor = System.Drawing.SystemColors.ControlLight
		Me._Line1_1.X1 = 48
		Me._Line1_1.X2 = 620
		Me._Line1_1.Y1 = 45
		Me._Line1_1.Y2 = 45
		Me._Line1_1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid
		Me._Line1_1.BorderWidth = 1
		Me._Line1_1.Visible = True
		Me._Line1_1.Name = "_Line1_1"
		Me.lblVerInfo.Text = "Version 0.00.0000"
		Me.lblVerInfo.Size = New System.Drawing.Size(253, 17)
		Me.lblVerInfo.Location = New System.Drawing.Point(52, 24)
		Me.lblVerInfo.TabIndex = 1
		Me.lblVerInfo.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblVerInfo.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblVerInfo.BackColor = System.Drawing.SystemColors.Control
		Me.lblVerInfo.Enabled = True
		Me.lblVerInfo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblVerInfo.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblVerInfo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblVerInfo.UseMnemonic = True
		Me.lblVerInfo.Visible = True
		Me.lblVerInfo.AutoSize = False
		Me.lblVerInfo.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblVerInfo.Name = "lblVerInfo"
		Me._lblStatic_0.Text = "vbSpec - Sinclair ZX Spectrum Emulator"
		Me._lblStatic_0.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblStatic_0.Size = New System.Drawing.Size(253, 17)
		Me._lblStatic_0.Location = New System.Drawing.Point(52, 8)
		Me._lblStatic_0.TabIndex = 0
		Me._lblStatic_0.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me._lblStatic_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblStatic_0.Enabled = True
		Me._lblStatic_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblStatic_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblStatic_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblStatic_0.UseMnemonic = True
		Me._lblStatic_0.Visible = True
		Me._lblStatic_0.AutoSize = False
		Me._lblStatic_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblStatic_0.Name = "_lblStatic_0"
		Me.Image1.Size = New System.Drawing.Size(32, 32)
		Me.Image1.Location = New System.Drawing.Point(8, 8)
		Me.Image1.Image = CType(resources.GetObject("Image1.Image"), System.Drawing.Image)
		Me.Image1.Enabled = True
		Me.Image1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Image1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.Image1.Visible = True
		Me.Image1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Image1.Name = "Image1"
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(lblWebSite)
		Me.Controls.Add(_lblStatic_2)
		Me.ShapeContainer1.Shapes.Add(_Line1_0)
		Me.ShapeContainer1.Shapes.Add(_Line1_1)
		Me.Controls.Add(lblVerInfo)
		Me.Controls.Add(_lblStatic_0)
		Me.Controls.Add(Image1)
		Me.Controls.Add(ShapeContainer1)
		'Me.Line1.SetIndex(_Line1_0, CType(0, Short))
		'Me.Line1.SetIndex(_Line1_1, CType(1, Short))
		Me.lblStatic.SetIndex(_lblStatic_2, CType(2, Short))
		Me.lblStatic.SetIndex(_lblStatic_0, CType(0, Short))
		CType(Me.lblStatic, System.ComponentModel.ISupportInitialize).EndInit()
		'CType(Me.Line1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class