Option Strict Off
Option Explicit On
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices

Module modSpectrum
	' /*******************************************************************************
	'   modSpectrum.bas within vbSpec.vbp
	'
	'   Routines for emulating the spectrum hardware; displaying the
	'   video memory (0x4000 - 0x5AFF), reading the keyboard (port
	'   0xFE), and displaying the border colour (out (xxFE),x)
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2003 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/


	'MM JD

	'MM 03.02.2003 - BEGIN
	'Remarks:
	'1. this code supports analog joysticks only
	'2. the user must have a joytick driver under Windows
	'3. the Y-axes represents the up and down
	'4. the X-axes represents the right and left
	'5. only one PC-joystick button is as fire supported
	'6. the user can theoretically assign two PC-joysticks to one ZX-joystick, but I
	'   don't think that't a good idea...
	'JoyStick API
	Public Const MAXPNAMELEN As Short = 32
	'MM 03.02.2003 - BEGIN
	Public Const MAX_JOYSTICKOEMVXDNAME As Short = 260
	'MM 03.02.2003 - END
	' The JOYINFOEX user-defined type contains extended information about the joystick position,
	' point-of-view position, and button state.
	Structure JOYINFOEX
		Dim dwSize As Integer ' size of structure
		Dim dwFlags As Integer ' flags to indicate what to return
		Dim dwXpos As Integer ' x position
		Dim dwYpos As Integer ' y position
		Dim dwZpos As Integer ' z position
		Dim dwRpos As Integer ' rudder/4th axis position
		Dim dwUpos As Integer ' 5th axis position
		Dim dwVpos As Integer ' 6th axis position
		Dim dwButtons As Integer ' button states
		Dim dwButtonNumber As Integer ' current button number pressed
		Dim dwPOV As Integer ' point of view state
		Dim dwReserved1 As Integer ' reserved for communication between winmm driver
		Dim dwReserved2 As Integer ' reserved for future expansion
	End Structure
	' The JOYCAPS user-defined type contains information about the joystick capabilities
	Structure JOYCAPS
		Dim wMid As Short ' Manufacturer identifier of the device driver for the MIDI output device
		' For a list of identifiers, see the Manufacturer Indentifier topic in the
		' Multimedia Reference of the Platform SDK.
		Dim wPid As Short ' Product Identifier Product of the MIDI output device. For a list of
		' product identifiers, see the Product Identifiers topic in the Multimedia
		' Reference of the Platform SDK.
		'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(MAXPNAMELEN), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=MAXPNAMELEN)> Public szPname() As Char ' Null-terminated string containing the joystick product name
		Dim wXmin As Integer ' Minimum X-coordinate.
		Dim wXmax As Integer ' Maximum X-coordinate.
		Dim wYmin As Integer ' Minimum Y-coordinate
		Dim wYmax As Integer ' Maximum Y-coordinate
		Dim wZmin As Integer ' Minimum Z-coordinate
		Dim wZmax As Integer ' Maximum Z-coordinate
		Dim wNumButtons As Integer ' Number of joystick buttons
		Dim wPeriodMin As Integer ' Smallest polling frequency supported when captured by the joySetCapture function.
		Dim wPeriodMax As Integer ' Largest polling frequency supported when captured by the joySetCapture function.
		Dim wRmin As Integer ' Minimum rudder value. The rudder is a fourth axis of movement.
		Dim wRmax As Integer ' Maximum rudder value. The rudder is a fourth axis of movement.
		Dim wUmin As Integer ' Minimum u-coordinate (fifth axis) values.
		Dim wUmax As Integer ' Maximum u-coordinate (fifth axis) values.
		Dim wVmin As Integer ' Minimum v-coordinate (sixth axis) values.
		Dim wVmax As Integer ' Maximum v-coordinate (sixth axis) values.
		Dim wCaps As Integer ' Joystick capabilities as defined by the following flags
		'     JOYCAPS_HASZ-     Joystick has z-coordinate information.
		'     JOYCAPS_HASR-     Joystick has rudder (fourth axis) information.
		'     JOYCAPS_HASU-     Joystick has u-coordinate (fifth axis) information.
		'     JOYCAPS_HASV-     Joystick has v-coordinate (sixth axis) information.
		'     JOYCAPS_HASPOV-   Joystick has point-of-view information.
		'     JOYCAPS_POV4DIR-  Joystick point-of-view supports discrete values (centered, forward, backward, left, and right).
		'     JOYCAPS_POVCTS Joystick point-of-view supports continuous degree bearings.
		Dim wMaxAxes As Integer ' Maximum number of axes supported by the joystick.
		Dim wNumAxes As Integer ' Number of axes currently in use by the joystick.
		Dim wMaxButtons As Integer ' Maximum number of buttons supported by the joystick.
		'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(MAXPNAMELEN), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=MAXPNAMELEN)> Public szRegKey() As Char ' String containing the registry key for the joystick.
		'MM 03.02.2003 - BEGIN
		'UPGRADE_WARNING: Fixed-length string size must fit in the buffer. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="3C1E4426-0B80-443E-B943-0627CD55D48B"'
		<VBFixedString(MAX_JOYSTICKOEMVXDNAME), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValArray, SizeConst:=MAX_JOYSTICKOEMVXDNAME)> Public szOEMVxD() As Char ' Null-terminated string identifying the joystick driver OEM.
		'MM 03.02.2003 - END
	End Structure
	'UPGRADE_WARNING: Structure JOYINFOEX may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Declare Function joyGetPosEx Lib "winmm.dll" (ByVal uJoyID As Integer, ByRef pji As JOYINFOEX) As Integer
	' This function queries a joystick for its position and button status. The function
	' requires the following parameters;
	'     uJoyID-  integer identifying the joystick to be queried. Use the constants
	'              JOYSTICKID1 or JOYSTICKID2 for this value.
	'     pji-     user-defined type variable that stores extended position information
	'              and button status of the joystick. The information returned from
	'              this function depends on the flags you specify in dwFlags member of
	'              the user-defined type variable.
	'
	' The function returns the constant JOYERR_NOERROR if successful or one of the
	' following error values:
	'     MMSYSERR_NODRIVER-      The joystick driver is not present.
	'     MMSYSERR_INVALPARAM-    An invalid parameter was passed.
	'     MMSYSERR_BADDEVICEID-   The specified joystick identifier is invalid.
	'     JOYERR_UNPLUGGED-       The specified joystick is not connected to the system.
	'UPGRADE_WARNING: Structure JOYCAPS may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Declare Function joyGetDevCaps Lib "winmm.dll" Alias "joyGetDevCapsA" (ByVal id As Integer, ByRef lpCaps As JOYCAPS, ByVal uSize As Integer) As Integer
	' This function queries a joystick to determine its capabilities. The function requires
	' the following parameters:
	'     uJoyID-  integer identifying the joystick to be queried. Use the contstants
	'              JOYSTICKID1 or JOYSTICKID2 for this value.
	'     pjc-     user-defined type variable that stores the capabilities of the joystick.
	'     cbjc-    Size, in bytes, of the pjc variable. Use the Len function for this value.
	' The function returns the constant JOYERR_NOERROR if a joystick is present or one of
	' the following error values:
	'     MMSYSERR_NODRIVER-   The joystick driver is not present.
	'     MMSYSERR_INVALPARAM- An invalid parameter was passed.
	Public Const JOYERR_OK As Short = 0
	Public Const JOYSTICKID1 As Short = 0
	Public Const JOYSTICKID2 As Short = 1
	Public Const JOY_RETURNBUTTONS As Integer = &H80
	Public Const JOY_RETURNCENTERED As Integer = &H400
	Public Const JOY_RETURNPOV As Integer = &H40
	Public Const JOY_RETURNR As Integer = &H8
	Public Const JOY_RETURNU As Integer = &H10
	Public Const JOY_RETURNV As Integer = &H20
	Public Const JOY_RETURNX As Integer = &H1
	Public Const JOY_RETURNY As Integer = &H2
	Public Const JOY_RETURNZ As Integer = &H4
	Public Const JOY_RETURNALL As Boolean = (JOY_RETURNX Or JOY_RETURNY Or JOY_RETURNZ Or JOY_RETURNR Or JOY_RETURNU Or JOY_RETURNV Or JOY_RETURNPOV Or JOY_RETURNBUTTONS)
	Public Const JOYCAPS_HASZ As Integer = &H1
	Public Const JOYCAPS_HASR As Integer = &H2
	Public Const JOYCAPS_HASU As Integer = &H4
	Public Const JOYCAPS_HASV As Integer = &H8
	Public Const JOYCAPS_HASPOV As Integer = &H10
	Public Const JOYCAPS_POV4DIR As Integer = &H20
	Public Const JOYCAPS_POVCTS As Integer = &H40
	Public Const JOYERR_BASE As Short = 160
	Public Const JOYERR_UNPLUGGED As Integer = (JOYERR_BASE + 7)
	'Supported ZX-Joysticks
	Public Enum ZXJOYSTICKS
		zxjInvalid = -1
		zxjKempston = 0
		zxjCursor = 1
		zxjSinclair1 = 2
		zxjSinclair2 = 3
		zxjFullerBox = 4
		'MM JD
		zxjUserDefined = 5
	End Enum
	'Supported PC-joystick buttons
	Public Enum PCJOYBUTTONS
		pcjbInvalid = -1
		pcjbButton1 = 0
		pcjbButton2 = 1
		pcjbButton3 = 2
		pcjbButton4 = 3
		pcjbButton5 = 4
		pcjbButton6 = 5
		pcjbButton7 = 6
		pcjbButton8 = 7
	End Enum
	'Settings for PC-joystick nr. 1
	'The ZX-joystick type to be emulated by PC-joystick nr. 1
	Public lPCJoystick1Is As ZXJOYSTICKS
	'The Fire button of the ZX-joystick played by the button nr. ... of the
	'PC-joystick  nr. 1
	Public lPCJoystick1Fire As PCJOYBUTTONS
	'Number of the buttons by PC-joystick nr. 1
	'Remarks: up to 8 PC-joystick buttons supported
	Public lPCJoystick1Buttons As Integer
	'Settings for PC-joystick nr. 2
	'The ZX-joystick type to be emulated by PC-joystick nr. 2
	Public lPCJoystick2Is As ZXJOYSTICKS
	'The Fire button of the ZX-joystick played by the button nr. ... of the
	'PC-joystick  nr. 2
	Public lPCJoystick2Fire As PCJOYBUTTONS
	'Number of the buttons by PC-joystick nr. 2
	'Remarks: up to 8 PC-joystick buttons supported
	Public lPCJoystick2Buttons As Integer
	'Supported joystick directions
	Public Const zxjdUp As Integer = 1
	Public Const zxjdDown As Integer = 2
	Public Const zxjdLeft As Integer = 4
	Public Const zxjdRight As Integer = 8
	Public Const zxjdFire As Integer = 16
	'MM JD
	Public Const zxjdButtonBase As Integer = 32
	'Kempston joystick directions
	Public Const KEMPSTON_UP As Integer = 8
	Public Const KEMPSTON_PUP As Integer = 31
	Public Const KEMPSTON_DOWN As Integer = 4
	Public Const KEMPSTON_PDOWN As Integer = 31
	Public Const KEMPSTON_LEFT As Integer = 2
	Public Const KEMPSTON_PLEFT As Integer = 31
	Public Const KEMPSTON_RIGHT As Integer = 1
	Public Const KEMPSTON_PRIGHT As Integer = 31
	Public Const KEMPSTON_FIRE As Integer = 16
	Public Const KEMPSTON_PFIRE As Integer = 31
	'Cursor joystick directions
	Public Const CURSOR_UP As Integer = 183
	Public Const CURSOR_PUP As Integer = 61438
	Public Const CURSOR_DOWN As Integer = 175
	Public Const CURSOR_PDOWN As Integer = 61438
	Public Const CURSOR_LEFT As Integer = 175
	Public Const CURSOR_PLEFT As Integer = 63486
	Public Const CURSOR_RIGHT As Integer = 187
	Public Const CURSOR_PRIGHT As Integer = 61438
	Public Const CURSOR_FIRE As Integer = 190
	Public Const CURSOR_PFIRE As Integer = 61438
	'Sinclair1 joystick directions
	Public Const SINCLAIR1_UP As Integer = 189
	Public Const SINCLAIR1_PUP As Integer = 61438
	Public Const SINCLAIR1_DOWN As Integer = 187
	Public Const SINCLAIR1_PDOWN As Integer = 61438
	Public Const SINCLAIR1_LEFT As Integer = 175
	Public Const SINCLAIR1_PLEFT As Integer = 61438
	Public Const SINCLAIR1_RIGHT As Integer = 183
	Public Const SINCLAIR1_PRIGHT As Integer = 61438
	Public Const SINCLAIR1_FIRE As Integer = 190
	Public Const SINCLAIR1_PFIRE As Integer = 61438
	'Sinclair2 joystick directions
	Public Const SINCLAIR2_UP As Integer = 183
	Public Const SINCLAIR2_PUP As Integer = 63486
	Public Const SINCLAIR2_DOWN As Integer = 187
	Public Const SINCLAIR2_PDOWN As Integer = 63486
	Public Const SINCLAIR2_LEFT As Integer = 190
	Public Const SINCLAIR2_PLEFT As Integer = 63486
	Public Const SINCLAIR2_RIGHT As Integer = 189
	Public Const SINCLAIR2_PRIGHT As Integer = 63486
	Public Const SINCLAIR2_FIRE As Integer = 175
	Public Const SINCLAIR2_PFIRE As Integer = 63486
	'Fuller Box directions
	Public Const FULLER_UP As Integer = 1
	Public Const FULLER_PUP As Integer = 127
	Public Const FULLER_DOWN As Integer = 2
	Public Const FULLER_PDOWN As Integer = 127
	Public Const FULLER_LEFT As Integer = 4
	Public Const FULLER_PLEFT As Integer = 127
	Public Const FULLER_RIGHT As Integer = 8
	Public Const FULLER_PRIGHT As Integer = 127
	Public Const FULLER_FIRE As Integer = 128
	Public Const FULLER_PFIRE As Integer = 127
	'Last key
	Private lKey1 As Integer
	Private lKey2 As Integer
	'MM 12.03.2003 - Joystick support code
	'=================================================================================
	'Joystick validity Flags
	Public bJoystick1Valid As Boolean
	Public bJoystick2Valid As Boolean
	'=================================================================================
	'MM 03.02.2003 - END

	'MM JD
	'Maximal number of buttons
	Public Const JDT_MAXBUTTONS As Integer = 8
	'Joystick actions
	Public Const JDT_UP As Integer = 0
	Public Const JDT_DOWN As Integer = 1
	Public Const JDT_LEFT As Integer = 2
	Public Const JDT_RIGHT As Integer = 3
	Public Const JDT_BUTTON1 As Integer = 4
	Public Const JDT_BUTTON2 As Integer = 5
	Public Const JDT_BUTTON3 As Integer = 6
	Public Const JDT_BUTTON4 As Integer = 7
	Public Const JDT_BUTTON5 As Integer = 8
	Public Const JDT_BUTTON6 As Integer = 9
	Public Const JDT_BUTTON7 As Integer = 10
	Public Const JDT_BUTTON8 As Integer = 11
	Public Const JDT_BUTTON_BASE As Integer = 3
	'Joystick numbers
	Public Const JDT_JOYSTICK1 As Integer = 0
	Public Const JDT_JOYSTICK2 As Integer = 1
	'Joystick definition type
	Public Structure JOYSTICK_DEFINITION
		Dim sKey As String
		Dim lPort As Integer
		Dim lValue As Integer
		Dim lCumulate As Integer
	End Structure
	'Joystick definition table
	'That is a 3D table, wich describes the joystick actions, even for standart joysticks
	'- first dimension 1..0, the number of the joystick
	'- the second dimension 0..5, the joystick type (Enum ZXJOYSTICKS)
	'- the third dimension 0..11, the joystick actions (JDT_...)
	Public aJoystikDefinitionTable(1, 5, 11) As JOYSTICK_DEFINITION

	'MM 16.04.2003
	'True if Speccy in Full-Screen-Modus
	Public bFullScreen As Boolean

	Public glNewBorder As Integer
	Public glLastBorder As Integer

	Public lZXPrinterX As Integer
	Public lZXPrinterY As Integer
	Public lZXPrinterEncoder As Integer
	Private lZXPrinterMotorOn As Integer
	Private lZXPrinterStylusOn As Integer
	Private lRevBitValues(7) As Integer

	Public glAmigaMouseY(3) As Integer
	Public glAmigaMouseX(3) As Integer
	Private lOldAmigaX As Integer
	Private lOldAmigaY As Integer
	Private lAmigaXPtr As Integer
	Private lAmigaYPtr As Integer
	Function doKey(ByRef down As Boolean, ByRef ascii As Short, ByRef mods As Short) As Boolean
		Dim CAPS, SYMB As Boolean

		CAPS = mods And 1
		SYMB = mods And 2

		' // Change control versions of keys to lower case
		If (ascii >= 1) And (ascii <= &H27) And SYMB Then
			ascii = ascii + Asc("a") - 1
		End If

		If CAPS Then keyCAPS_V = (keyCAPS_V And (Not 1)) Else keyCAPS_V = (keyCAPS_V Or 1)
		If SYMB Then keyB_SPC = (keyB_SPC And (Not 2)) Else keyB_SPC = (keyB_SPC Or 2)

		Select Case ascii
			Case 8 ' Backspace
				If down Then
					key6_0 = (key6_0 And (Not 1))
					keyCAPS_V = (keyCAPS_V And (Not 1))
				Else
					key6_0 = (key6_0 Or 1)
					If Not CAPS Then
						keyCAPS_V = (keyCAPS_V Or 1)
					End If
				End If
			Case 65 ' A
				If down Then keyA_G = (keyA_G And (Not 1)) Else keyA_G = (keyA_G Or 1)
			Case 66 ' B
				If down Then keyB_SPC = (keyB_SPC And (Not 16)) Else keyB_SPC = (keyB_SPC Or 16)
			Case 67 ' C
				If down Then keyCAPS_V = (keyCAPS_V And (Not 8)) Else keyCAPS_V = (keyCAPS_V Or 8)
			Case 68 ' D
				If down Then keyA_G = (keyA_G And (Not 4)) Else keyA_G = (keyA_G Or 4)
			Case 69 ' E
				If down Then keyQ_T = (keyQ_T And (Not 4)) Else keyQ_T = (keyQ_T Or 4)
			Case 70 ' F
				If down Then keyA_G = (keyA_G And (Not 8)) Else keyA_G = (keyA_G Or 8)
			Case 71 ' G
				If down Then keyA_G = (keyA_G And (Not 16)) Else keyA_G = (keyA_G Or 16)
			Case 72 ' H
				If down Then keyH_ENT = (keyH_ENT And (Not 16)) Else keyH_ENT = (keyH_ENT Or 16)
			Case 73 ' I
				If down Then keyY_P = (keyY_P And (Not 4)) Else keyY_P = (keyH_ENT Or 4)
			Case 74 ' J
				If down Then keyH_ENT = (keyH_ENT And (Not 8)) Else keyH_ENT = (keyH_ENT Or 8)
			Case 75 ' K
				If down Then keyH_ENT = (keyH_ENT And (Not 4)) Else keyH_ENT = (keyH_ENT Or 4)
			Case 76 ' L
				If down Then keyH_ENT = (keyH_ENT And (Not 2)) Else keyH_ENT = (keyH_ENT Or 2)
			Case 77 ' M
				If down Then keyB_SPC = (keyB_SPC And (Not 4)) Else keyB_SPC = (keyB_SPC Or 4)
			Case 78 ' N
				If down Then keyB_SPC = (keyB_SPC And (Not 8)) Else keyB_SPC = (keyB_SPC Or 8)
			Case 79 ' O
				If down Then keyY_P = (keyY_P And (Not 2)) Else keyY_P = (keyY_P Or 2)
			Case 80 ' P
				If down Then keyY_P = (keyY_P And (Not 1)) Else keyY_P = (keyY_P Or 1)
			Case 81 ' Q
				If down Then keyQ_T = (keyQ_T And (Not 1)) Else keyQ_T = (keyQ_T Or 1)
			Case 82 ' R
				If down Then keyQ_T = (keyQ_T And (Not 8)) Else keyQ_T = (keyQ_T Or 8)
			Case 83 ' S
				If down Then keyA_G = (keyA_G And (Not 2)) Else keyA_G = (keyA_G Or 2)
			Case 84 ' T
				If down Then keyQ_T = (keyQ_T And (Not 16)) Else keyQ_T = (keyQ_T Or 16)
			Case 85 ' U
				If down Then keyY_P = (keyY_P And (Not 8)) Else keyY_P = (keyY_P Or 8)
			Case 86 ' V
				If down Then keyCAPS_V = (keyCAPS_V And (Not 16)) Else keyCAPS_V = (keyCAPS_V Or 16)
			Case 87 ' W
				If down Then keyQ_T = (keyQ_T And (Not 2)) Else keyQ_T = (keyQ_T Or 2)
			Case 88 ' X
				If down Then keyCAPS_V = (keyCAPS_V And (Not 4)) Else keyCAPS_V = (keyCAPS_V Or 4)
			Case 89 ' Y
				'MM 03.02.2003 - BEGIN
				If Not frmMainWnd.mnuOptions(7).Checked Then
					If down Then keyY_P = (keyY_P And (Not 16)) Else keyY_P = (keyY_P Or 16)
				Else
					If down Then keyCAPS_V = (keyCAPS_V And (Not 2)) Else keyCAPS_V = (keyCAPS_V Or 2)
				End If
				'MM 03.02.2003 - END
			Case 90 ' Z
				'MM 03.02.2003 - BEGIN
				If Not frmMainWnd.mnuOptions(7).Checked Then
					If down Then keyCAPS_V = (keyCAPS_V And (Not 2)) Else keyCAPS_V = (keyCAPS_V Or 2)
				Else
					If down Then keyY_P = (keyY_P And (Not 16)) Else keyY_P = (keyY_P Or 16)
				End If
				'MM 03.02.2003 - END
			Case 48 ' 0
				If down Then key6_0 = (key6_0 And (Not 1)) Else key6_0 = (key6_0 Or 1)
			Case 49 ' 1
				If down Then key1_5 = (key1_5 And (Not 1)) Else key1_5 = (key1_5 Or 1)
			Case 50 ' 2
				If down Then key1_5 = (key1_5 And (Not 2)) Else key1_5 = (key1_5 Or 2)
			Case 51 ' 3
				If down Then key1_5 = (key1_5 And (Not 4)) Else key1_5 = (key1_5 Or 4)
			Case 52 ' 4
				If down Then key1_5 = (key1_5 And (Not 8)) Else key1_5 = (key1_5 Or 8)
			Case 53 ' 5
				If down Then key1_5 = (key1_5 And (Not 16)) Else key1_5 = (key1_5 Or 16)
			Case 54 ' 6
				If down Then key6_0 = (key6_0 And (Not 16)) Else key6_0 = (key6_0 Or 16)
			Case 55 ' 7
				If down Then key6_0 = (key6_0 And (Not 8)) Else key6_0 = (key6_0 Or 8)
			Case 56 ' 8
				If down Then key6_0 = (key6_0 And (Not 4)) Else key6_0 = (key6_0 Or 4)
			Case 57 ' 9
				If down Then key6_0 = (key6_0 And (Not 2)) Else key6_0 = (key6_0 Or 2)
			Case 96 ' Keypad 0
				If down Then key6_0 = (key6_0 And (Not 1)) Else key6_0 = (key6_0 Or 1)
			Case 97 ' Keypad 1
				If down Then key1_5 = (key1_5 And (Not 1)) Else key1_5 = (key1_5 Or 1)
			Case 98 ' Keypad 2
				If down Then key1_5 = (key1_5 And (Not 2)) Else key1_5 = (key1_5 Or 2)
			Case 99 ' Keypad 3
				If down Then key1_5 = (key1_5 And (Not 4)) Else key1_5 = (key1_5 Or 4)
			Case 100 ' Keypad 4
				If down Then key1_5 = (key1_5 And (Not 8)) Else key1_5 = (key1_5 Or 8)
			Case 101 ' Keypad 5
				If down Then key1_5 = (key1_5 And (Not 16)) Else key1_5 = (key1_5 Or 16)
			Case 102 ' Keypad 6
				If down Then key6_0 = (key6_0 And (Not 16)) Else key6_0 = (key6_0 Or 16)
			Case 103 ' Keypad 7
				If down Then key6_0 = (key6_0 And (Not 8)) Else key6_0 = (key6_0 Or 8)
			Case 104 ' Keypad 8
				If down Then key6_0 = (key6_0 And (Not 4)) Else key6_0 = (key6_0 Or 4)
			Case 105 ' Keypad 9
				If down Then key6_0 = (key6_0 And (Not 2)) Else key6_0 = (key6_0 Or 2)
			Case 106 ' Keypad *
				If down Then
					keyB_SPC = (keyB_SPC And Not (18))
				Else
					If SYMB Then
						keyB_SPC = (keyB_SPC Or 16)
					Else
						keyB_SPC = (keyB_SPC Or 18)
					End If
				End If
			Case 107 ' Keypad +
				If down Then
					keyH_ENT = (keyH_ENT And (Not 4))
					keyB_SPC = (keyB_SPC And (Not 2))
				Else
					keyH_ENT = (keyH_ENT Or 4)
					If Not SYMB Then
						keyB_SPC = (keyB_SPC Or 2)
					End If
				End If
			Case 109 ' Keypad -
				If down Then
					keyH_ENT = (keyH_ENT And (Not 8))
					keyB_SPC = (keyB_SPC And (Not 2))
				Else
					keyH_ENT = (keyH_ENT Or 8)
					If Not SYMB Then
						keyB_SPC = (keyB_SPC Or 2)
					End If
				End If
			Case 110 ' Keypad .
				If down Then
					keyB_SPC = (keyB_SPC And (Not 6))
				Else
					If SYMB Then
						keyB_SPC = (keyB_SPC Or 4)
					Else
						keyB_SPC = (keyB_SPC Or 6)
					End If
				End If
			Case 111 ' Keypad /
				If down Then
					keyCAPS_V = (keyCAPS_V And (Not 16))
					keyB_SPC = (keyB_SPC And (Not 2))
				Else
					keyCAPS_V = (keyCAPS_V Or 16)
					If Not SYMB Then
						keyB_SPC = (keyB_SPC Or 2)
					End If
				End If
			Case 37 ' Left
				If down Then
					key1_5 = (key1_5 And (Not 16))
					keyCAPS_V = (keyCAPS_V And (Not 1))
				Else
					key1_5 = (key1_5 Or 16)
					If Not SYMB Then
						keyB_SPC = (keyB_SPC Or 2)
					End If
				End If
			Case 38 ' Up
				If down Then
					key6_0 = (key6_0 And (Not 8))
					keyCAPS_V = (keyCAPS_V And (Not 1))
				Else
					key6_0 = (key6_0 Or 8)
					If Not CAPS Then
						keyCAPS_V = (keyCAPS_V Or 1)
					End If
				End If
			Case 39 ' Right
				If down Then
					key6_0 = (key6_0 And (Not 4))
					keyCAPS_V = (keyCAPS_V And (Not 1))
				Else
					key6_0 = (key6_0 Or 4)
					If Not CAPS Then
						keyCAPS_V = (keyCAPS_V Or 1)
					End If
				End If
			Case 40 ' Down
				If down Then
					key6_0 = (key6_0 And (Not 16))
					keyCAPS_V = (keyCAPS_V And (Not 1))
				Else
					key6_0 = (key6_0 Or 16)
					If Not CAPS Then
						keyCAPS_V = (keyCAPS_V Or 1)
					End If
				End If
			Case 13 ' RETURN
				If down Then keyH_ENT = (keyH_ENT And (Not 1)) Else keyH_ENT = (keyH_ENT Or 1)
			Case 32 ' SPACE BAR
				If down Then keyB_SPC = (keyB_SPC And (Not 1)) Else keyB_SPC = (keyB_SPC Or 1)
			Case 187 ' =/+ key
				If down Then
					If CAPS Then
						keyH_ENT = (keyH_ENT And (Not 4))
					Else
						keyH_ENT = (keyH_ENT And (Not 2))
					End If
					keyB_SPC = (keyB_SPC And (Not 2))
					keyCAPS_V = (keyCAPS_V Or 1)
				Else
					keyH_ENT = (keyH_ENT Or 4)
					keyH_ENT = (keyH_ENT Or 2)
					keyB_SPC = (keyB_SPC Or 2)
				End If
			Case 189 ' -/_ key
				If down Then
					If CAPS Then
						key6_0 = (key6_0 And (Not 1))
					Else
						keyH_ENT = (keyH_ENT And (Not 8))
					End If
					keyB_SPC = (keyB_SPC And (Not 2))
					keyCAPS_V = (keyCAPS_V Or 1)
				Else
					key6_0 = (key6_0 Or 1) ' // Release the Spectrum's '0' key
					keyH_ENT = (keyH_ENT Or 8) ' // Release the Spectrum's 'J' key
					keyB_SPC = (keyB_SPC Or 2) ' // Release the Symbol Shift key
				End If
			Case 186 ' ;/: keys
				If down Then
					If CAPS Then
						keyCAPS_V = (keyCAPS_V And (Not 2))
					Else
						keyY_P = (keyY_P And (Not 2))
					End If
					keyB_SPC = (keyB_SPC And (Not 2))
					keyCAPS_V = (keyCAPS_V Or 1)
				Else
					keyCAPS_V = (keyCAPS_V Or 2)
					keyY_P = (keyY_P Or 2)
					keyB_SPC = (keyB_SPC Or 2)
				End If
			Case Else
				doKey = False
		End Select

		doKey = True
	End Function

	Public Sub Hook_LDBYTES()
		Dim l As Integer

		If LoadTAP(glMemAddrDiv256(regAF_), regIX, regDE) Then
			regAF_ = regAF_ Or 64 ' // Congraturation Load Sucsess!
		Else
			regAF_ = regAF_ And 190 ' // Load failed
		End If

		l = getAF()
		setAF(regAF_)
		regAF_ = l

		regPC = 1506
	End Sub




	Public Sub Hook_SABYTES()
		SaveTAPFileDlg()
	End Sub


	Public Sub InitReverseBitValues()
		lRevBitValues(0) = 128
		lRevBitValues(1) = 64
		lRevBitValues(2) = 32
		lRevBitValues(3) = 16
		lRevBitValues(4) = 8
		lRevBitValues(5) = 4
		lRevBitValues(6) = 2
		lRevBitValues(7) = 1
	End Sub

	Sub plot(ByRef addr As Integer)
		Dim i, lne, X As Integer

		If addr < 22528 Then
			' // Alter a pixel
			lne = (glMemAddrDiv256(addr) And &H7) Or (glMemAddrDiv4(addr) And &H38) Or (glMemAddrDiv32(addr) And &HC0)
			ScrnLines(lne, 32) = True
			ScrnLines(lne, addr And 31) = True
		Else
			' // Alter an attribute
			lne = glMemAddrDiv32(addr - 22528)
			X = addr Mod 32
			For i = lne * 8 To lne * 8 + 7
				ScrnLines(i, 32) = True
				ScrnLines(i, X) = True
			Next i
		End If
		If glUseScreen >= 1000 Then ScrnNeedRepaint = True
	End Sub

	Function inb(ByRef port As Integer) As Integer
		Dim p As POINTAPI
		Dim bPortDefined As Boolean
		Dim lCounter As Integer

		'MM JD
		'Init inb with the joystick values
		inb = JoystickInitIN(port)

		Dim res As Integer
		If (port And &HFF) = 254 Then

			res = &HFF

			If (port And &H8000) = 0 Then
				res = res And keyB_SPC
			End If
			If (port And &H4000) = 0 Then
				res = res And keyH_ENT
			End If
			If (port And &H2000) = 0 Then
				res = res And keyY_P
			End If
			If (port And &H1000) = 0 Then
				res = res And key6_0
			End If
			If (port And &H800) = 0 Then
				res = res And key1_5
			End If
			If (port And &H400) = 0 Then
				res = res And keyQ_T
			End If
			If (port And &H200) = 0 Then
				res = res And keyA_G
			End If
			If (port And &H100) = 0 Then
				res = res And keyCAPS_V
			End If
			If inb <> 0 Then
				inb = inb And (res And glKeyPortMask) Or glEarBit ' glEarBit holds tape state (0 or 64 only)
			Else
				inb = (res And glKeyPortMask) Or glEarBit ' glEarBit holds tape state (0 or 64 only)
			End If
			glTStates = glTStates + glContentionTable(-glTStates + 30)
		ElseIf port = &HFFFD Then
			If (glEmulatedModel And 3) <> 0 Then
				inb = AYPSG.Regs(glSoundRegister)
			End If
		ElseIf (port And &HFF) = &HFF Then
			If glEmulatedModel = 5 Then
				' // TC2048
				inb = glTC2048LastFFOut
			Else
				If (glTStates >= glTStatesAtTop) And (glTStates <= glTStatesAtBottom) Then
					inb = 0
				Else
					inb = 255
				End If
			End If
		ElseIf (port And &HFF) = 31 Then
			If glMouseType = MOUSE_AMIGA Then
				GetCursorPos(p)
				If p.X > lOldAmigaX Then
					lAmigaXPtr = lAmigaXPtr + 1
					If lAmigaXPtr = 4 Then lAmigaXPtr = 0
				ElseIf p.X < lOldAmigaX Then
					lAmigaXPtr = lAmigaXPtr - 1
					If lAmigaXPtr = -1 Then lAmigaXPtr = 3
				End If
				If p.y < lOldAmigaY Then
					lAmigaYPtr = lAmigaYPtr + 1
					If lAmigaYPtr = 4 Then lAmigaYPtr = 0
				ElseIf p.y > lOldAmigaY Then
					lAmigaYPtr = lAmigaYPtr - 1
					If lAmigaYPtr = -1 Then lAmigaYPtr = 3
				End If
				lOldAmigaX = p.X
				lOldAmigaY = p.y
				inb = glAmigaMouseX(lAmigaXPtr) Or glAmigaMouseY(lAmigaYPtr)

				If gbMouseGlobal Then
					inb = inb Or (-CShort((GetKeyState(VK_LBUTTON) And 256) = 256) * 16) Or (-CShort((GetKeyState(VK_RBUTTON) And 256) = 256) * 32)
				Else
					inb = inb Or (glMouseBtn * 16)
				End If
			Else
				'MM JD
				'The emulator does not need this line anymore
				'inb = 0&
			End If
		ElseIf (port And 4) = 0 Then
			inb = ZXPrinterIn()
			' // Kempston Mouse Interface
		ElseIf glMouseType = MOUSE_KEMPSTON Then
			GetCursorPos(p)
			If (port = 64479) Then
				inb = p.X Mod 256
			ElseIf (port = 65503) Then
				inb = (4000 - p.y) Mod 256
			ElseIf (port = 64223) Then
				If gbMouseGlobal Then
					inb = 255 + CShort((GetKeyState(VK_RBUTTON) And 256) = 256)
					inb = inb + (CShort((GetKeyState(VK_LBUTTON) And 256) = 256) * 2)
				Else
					inb = 255 - CShort(glMouseBtn And 1) * 2
					inb = inb - (glMouseBtn And 2) \ 2
				End If
			End If
		Else
			' // Unconnected port
			If (glTStates >= glTStatesAtTop) And (glTStates <= glTStatesAtBottom) And (glEmulatedModel <> 5) Then
				'If inb is not 0 here, this means, that that is a joystick port an this port
				'is connected. Othervise is inb alread 0, so the Speccy does not need this
				'line anymore
				'inb = 0& '// This should return a floating bus value, but zero suffices
				'         '// for commericial games that depend on the floating bus such
				'         '// as Cobra and Arkanoid
			Else
				'Only if the port ist'n the actual joystick port
				'IF the actual port is a valid joystick port and there is some move on the
				'joystick, the inb is not 0.
				If inb = 0 Then
					inb = 255
				End If
			End If
		End If
	End Function

	Sub outb(ByRef port As Integer, ByRef outbyte As Integer)
		If (port And 1) = 0 Then
			glLastFEOut = outbyte And &HFF
			If glUseScreen <> 1006 Then
				glNewBorder = glNormalColor(outbyte And &H7)
			End If

			If (outbyte And 16) Then
				glBeeperVal = 159
			Else
				glBeeperVal = 128
			End If
			glTStates = glTStates + glContentionTable(-glTStates + 30)
			Exit Sub
		ElseIf glMemPagingType <> 0 Then
			' // 128/+2 memory page operation
			If (port And 32770) = 0 Then
				' // RAM page
				glPageAt(3) = (outbyte And 7)
				' // Screen page
				If (outbyte And 8) Then
					If glUseScreen = 5 Then
						glUseScreen = 7
						initscreen()
					End If
				Else
					If glUseScreen = 7 Then
						glUseScreen = 5
						initscreen()
					End If
				End If
				' // ROM
				If (outbyte And 16) Then
					glPageAt(0) = 9
					glPageAt(4) = 9
				Else
					glPageAt(0) = 8
					glPageAt(4) = 8
				End If

				If (outbyte And 32) Then glMemPagingType = 0
				glLastOut7FFD = outbyte
			ElseIf (port And &HC002) = &HC000 Then
				glSoundRegister = outbyte And &HF
				Exit Sub
			ElseIf (port And &HC002) = &H8000 Then
				AYWriteReg(glSoundRegister, outbyte)
				Exit Sub
				'ElseIf port = &HBEFD& Then
				'    AYWriteReg glSoundRegister, outbyte
				' ElseIf port = &H1FFD& Then
				' // +2A/+3 special paging mode
			End If
		ElseIf (port And &H4) = 0 Then
			ZXPrinterOut(outbyte)
		ElseIf glEmulatedModel = 5 Then
			' // TC2048=May slow things down :(
			If (port And &HFF) = &HFF Then
				glTC2048LastFFOut = outbyte And &HFF
				If (outbyte And 7) = 0 Then
					' // screen 0
					glUseScreen = 5
					bmiBuffer.bmiHeader.biWidth = 256

					glNewBorder = glNormalColor(glLastFEOut And 7)
				ElseIf (outbyte And 7) = 1 Then
					' // screen 1
					glUseScreen = 1001
					bmiBuffer.bmiHeader.biWidth = 256

					glNewBorder = glNormalColor(glLastFEOut And 7)
				ElseIf (outbyte And 7) = 2 Then
					' // hi-colour
					glUseScreen = 1002
					bmiBuffer.bmiHeader.biWidth = 256

					glNewBorder = glNormalColor(glLastFEOut And 7)
				ElseIf (outbyte And 7) = 6 Then
					' // hi-res
					glUseScreen = 1006
					bmiBuffer.bmiHeader.biWidth = 512

					If (outbyte And 56) = 0 Then
						' // Black on white
						glTC2048HiResColour = 120
						glNewBorder = glBrightColor(7)
					ElseIf (outbyte And 56) = 8 Then
						' // Blue on yellow
						glTC2048HiResColour = 113
						glNewBorder = glBrightColor(6)
					ElseIf (outbyte And 56) = 16 Then
						' // Red on cyan
						glTC2048HiResColour = 106
						glNewBorder = glBrightColor(5)
					ElseIf (outbyte And 56) = 24 Then
						' // Magenta on green
						glTC2048HiResColour = 99
						glNewBorder = glBrightColor(4)
					ElseIf (outbyte And 56) = 32 Then
						' // Green on magenta
						glTC2048HiResColour = 92
						glNewBorder = glBrightColor(3)
					ElseIf (outbyte And 56) = 40 Then
						' // Cyan on red
						glTC2048HiResColour = 85
						glNewBorder = glBrightColor(2)
					ElseIf (outbyte And 56) = 48 Then
						' // Yellow on blue
						glTC2048HiResColour = 78
						glNewBorder = glBrightColor(1)
					ElseIf (outbyte And 56) = 56 Then
						' // White on black
						glTC2048HiResColour = 71
						glNewBorder = glBrightColor(0)
					End If
				End If
				initscreen()
			End If
		End If
	End Sub

	Sub plotTC2048HiResHiArea(ByRef addr As Integer)
		Dim lne As Integer

		' // Alter a pixel in the higher screen (odd columns)
		lne = (glMemAddrDiv256(addr) And &H7) Or (glMemAddrDiv4(addr) And &H38) Or (glMemAddrDiv32(addr) And &HC0)
		ScrnLines(lne, 64) = True
		ScrnLines(lne, (CShort(addr And 31) * 2) + 1) = True

		ScrnNeedRepaint = True
	End Sub

	Sub plotTC2048HiResLowArea(ByRef addr As Integer)
		Dim lne As Integer

		' // Alter a pixel in the lower screen (even columns)
		lne = (glMemAddrDiv256(addr) And &H7) Or (glMemAddrDiv4(addr) And &H38) Or (glMemAddrDiv32(addr) And &HC0)
		ScrnLines(lne, 64) = True
		ScrnLines(lne, CShort(addr And 31) * 2) = True

		ScrnNeedRepaint = True
	End Sub

	Public Sub refreshFlashChars()
		If glUseScreen > 8 Then
			TC2048refreshFlashChars()
			Exit Sub
		End If

		Dim lne, addr, i As Integer

		bFlashInverse = Not (bFlashInverse)

		For addr = 6144 To 6911
			If gRAMPage(glUseScreen, addr) And 128 Then
				lne = glMemAddrDiv32(addr - 6144)
				For i = lne * 8 To lne * 8 + 7
					ScrnLines(i, 32) = True
					ScrnLines(i, addr And 31) = True
				Next i
			End If
		Next addr
	End Sub




	Public Sub ScanlinePaint(ByRef lne As Integer)
		If glUseScreen >= 1000 Then Exit Sub

		'UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim abyte, X, lLneIndex, lColIndex, sbyte_Renamed, lIndex As Integer

		If ScrnLines(lne, 32) = True Then
			If lne < glTopMost Then glTopMost = lne
			If lne > glBottomMost Then glBottomMost = lne

			lLneIndex = glRowIndex(lne)
			lColIndex = glColIndex(lne)
			For X = 0 To 31
				If ScrnLines(lne, X) = True Then
					If X < glLeftMost Then glLeftMost = X
					If X > glRightMost Then glRightMost = X

					sbyte_Renamed = gRAMPage(glUseScreen, glScreenMem(lne, X))
					abyte = gRAMPage(glUseScreen, lLneIndex + X)

					If (abyte And 128) And (bFlashInverse) Then
						' // Swap fore- and back-colours
						abyte = abyte Xor 128
					End If

					lIndex = (lColIndex + X + X)
					glBufferBits(lIndex) = gtBitTable(sbyte_Renamed, abyte).dw0
					glBufferBits(lIndex + 1) = gtBitTable(sbyte_Renamed, abyte).dw1

					ScrnLines(lne, X) = False
				End If
			Next X
			ScrnLines(lne, 32) = False ' // Flag indicates this line has been rendered on the bitmap
			ScrnNeedRepaint = True
		End If
	End Sub

	Sub screenPaint()
		' // Only update screen if necessary
		If ScrnNeedRepaint = False Then Exit Sub

		' // TC2048=May slow things down :(
		If glUseScreen >= 1000 Then
			TC2048screenPaint()
			Exit Sub
		End If

		glLeftMost = glLeftMost * 8
		glRightMost = glRightMost * 8

		gpicDisplay.Hide()

		Dim i As Integer
		i = Marshal.SizeOf(glBufferBits(0)) * glBufferBits.Length
		Dim p As IntPtr
		p = Marshal.AllocHGlobal(i)
		Marshal.Copy(glBufferBits, 0, p, glBufferBits.Length)

		gpicDisplay.Size = New Size(bmiBuffer.bmiHeader.biWidth, bmiBuffer.bmiHeader.biHeight)

		'gpicGraphics = gpicDisplay.CreateGraphics()
		'gpicDC = gpicGraphics.GetHdc()

		'StretchDIBits(gpicDC,
		'			  glLeftMost * glDisplayXMultiplier,
		'			  (glBottomMost + 1) * glDisplayYMultiplier - 1,
		'			  (glRightMost - glLeftMost + 8) * glDisplayXMultiplier,
		'			  -(glBottomMost - glTopMost + 1) * glDisplayYMultiplier,
		'			  glLeftMost, glTopMost,
		'			  (glRightMost - glLeftMost) + 8,
		'			  glBottomMost - glTopMost + 1,
		'glBufferBits,
		'bmiBuffer,
		'DIB_RGB_COLORS, SRCCOPY)

		''StretchDIBits(gpicDC,
		''			  glLeftMost * glDisplayXMultiplier,
		''			  (glBottomMost + 1) * glDisplayYMultiplier - 1,
		''			  (glRightMost - glLeftMost + 8) * glDisplayXMultiplier,
		''			  -(glBottomMost - glTopMost + 1) * glDisplayYMultiplier,
		''			  glLeftMost, glTopMost,
		''			  (glRightMost - glLeftMost) + 8,
		''			  glBottomMost - glTopMost + 1,
		''p,
		''bmiBuffer,
		''DIB_RGB_COLORS, SRCCOPY)

		'gpicGraphics.ReleaseHdc(gpicDC)

		'Dim flag As Bitmap
		'Dim flagGraphics As Graphics
		'flag = New Bitmap(200, 100)
		'Dim red As Integer
		'Dim white As Integer
		'flagGraphics = Graphics.FromImage(flag)
		'red = 0
		'white = 11
		'While white <= 100
		'	flagGraphics.FillRectangle(Brushes.Red, 0, red, 200, 10)
		'	flagGraphics.FillRectangle(Brushes.White, 0, white, 200, 10)
		'	red += 20
		'	white += 20
		'End While
		'gpicDisplay.Image = flag

		'Dim b(glBufferBits.Length * 4) As Byte
		'For i = 0 To glBufferBits.Length - 1
		'	b(i * 4 + 0) = glBufferBits(i) Mod 256
		'	b(i * 4 + 1) = glBufferBits(i) / 256 Mod 256
		'	b(i * 4 + 2) = glBufferBits(i) / 256 / 256 Mod 256
		'	b(i * 4 + 3) = glBufferBits(i) / 256 / 256 / 256
		'Next
		'Dim pb As New System.IO.MemoryStream(b)
		'Dim gfx As Graphics
		'Dim bmp As Bitmap
		'gfx = Graphics.FromImage(Image.FromStream(pb))
		'bmp = New Bitmap(pb)
		'gpicDisplay.Image = bmp

		Dim bmp As New Bitmap(bmiBuffer.bmiHeader.biWidth, bmiBuffer.bmiHeader.biHeight, bmiBuffer.bmiHeader.biWidth, PixelFormat.Format8bppIndexed, p)
		Dim pp As Integer
		Dim cp As ColorPalette
		cp = bmp.Palette
		For pp = 0 To 15
			cp.Entries(pp) = Color.FromArgb(bmiBuffer.bmiColors(pp + 1).rgbReserved, bmiBuffer.bmiColors(pp).rgbRed, bmiBuffer.bmiColors(pp).rgbGreen, bmiBuffer.bmiColors(pp).rgbBlue)
		Next
		bmp.Palette = cp
		gpicDisplay.Image = bmp

		'Dim converter As New ImageConverter()
		'Dim img As Image
		'Converter.ConvertFrom(b)
		'img = DirectCast(converter.ConvertFrom(b), Image)
		'gpicDisplay.Image = img

		'Marshal.FreeHGlobal(p)

		gpicDisplay.Refresh()
		gpicDisplay.Show()

		glTopMost = 191
		glBottomMost = 0
		glLeftMost = 31
		glRightMost = 0
		
		ScrnNeedRepaint = False
	End Sub
	
	
	
	Private Sub SetZXPrinterPixel(ByRef X As Integer, ByRef y As Integer)
		Dim lElement As Integer
		If X = -1 Then X = 256
		
		lElement = y * 32 + X \ 8
		gcZXPrinterBits(lElement) = gcZXPrinterBits(lElement) Or lRevBitValues(X Mod 8)
	End Sub
	
	Sub TC2048PaintHiRes()
		Dim lne, X As Integer
		'UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim sbyte_Renamed As Integer
		Dim lTopMost, lLeftMost, lRightMost, lBottomMost As Integer
		
		' // Bob Woodring's (RGW) improvements to display speed (lookup table of colour values)
		Dim lIndex As Integer
		Dim lLneIndex As Integer
		Dim lColIndex As Integer
		
		'gpicDisplay.Visible = False
		
		lTopMost = 191
		lBottomMost = 0
		lLeftMost = 63
		lRightMost = 0
		
		
		For lne = 0 To 191
			If ScrnLines(lne, 64) = True Then
				If lne < lTopMost Then lTopMost = lne
				If lne > lBottomMost Then lBottomMost = lne
				' // RGW: Get line and column indexes from a lookup table for speed
				lLneIndex = glRowIndex(lne)
				lColIndex = glColIndex(lne) * 2
				For X = 0 To 63
					If ScrnLines(lne, X) = True Then
						If X < lLeftMost Then lLeftMost = X
						If X > lRightMost Then lRightMost = X
						
						sbyte_Renamed = gRAMPage(5, glScreenMemTC2048HiRes(lne, X))
						
						lIndex = (lColIndex + X + X)
						glBufferBits(lIndex) = gtBitTable(sbyte_Renamed, glTC2048HiResColour).dw0
						glBufferBits(lIndex + 1) = gtBitTable(sbyte_Renamed, glTC2048HiResColour).dw1
						ScrnLines(lne, X) = False
					End If
				Next X
				ScrnLines(lne, 64) = False
			End If
		Next lne
		
		lLeftMost = lLeftMost * 8
		lRightMost = lRightMost * 8
		
		StretchDIBits(gpicDC, lLeftMost * (glDisplayXMultiplier / 2), (lBottomMost + 1) * glDisplayYMultiplier - 1, (lRightMost - lLeftMost + 8) * (glDisplayXMultiplier / 2), -(lBottomMost - lTopMost + 1) * glDisplayYMultiplier, lLeftMost, lTopMost, (lRightMost - lLeftMost) + 8, lBottomMost - lTopMost + 1, glBufferBits(0), bmiBuffer, DIB_RGB_COLORS, SRCCOPY)
		gpicDisplay.Refresh()
		'gpicDisplay.Visible = True
		ScrnNeedRepaint = False
	End Sub
	
	Public Sub TC2048refreshFlashChars()
		Dim lScrn, lne, addr, i, lOffset As Integer
		
		If glUseScreen = 1006 Then Exit Sub
		
		If glUseScreen = 1001 Then
			lOffset = 8192
		ElseIf glUseScreen = 1002 Then 
			' // HiColour
			bFlashInverse = Not (bFlashInverse)
			
			For addr = 8192 To 14335
				If gRAMPage(5, addr) And 128 Then
					lne = glMemAddrDiv32(addr - 8192)
					For i = lne * 8 To lne * 8 + 7
						ScrnLines(i, 32) = True
						ScrnLines(i, addr And 31) = True
					Next i
					ScrnNeedRepaint = True
				End If
			Next addr
			
			Exit Sub
		End If
		
		bFlashInverse = Not (bFlashInverse)
		
		For addr = 6144 To 6911
			If gRAMPage(5, addr + lOffset) And 128 Then
				lne = glMemAddrDiv32(addr - 6144)
				For i = lne * 8 To lne * 8 + 7
					ScrnLines(i, 32) = True
					ScrnLines(i, addr And 31) = True
				Next i
				ScrnNeedRepaint = True
			End If
		Next addr
	End Sub
	
	Sub TC2048screenPaint()
		If glUseScreen = 1001 Then
			TC2048ScreenPaintScrn1()
		ElseIf glUseScreen = 1002 Then 
			TC2048PaintHiColour()
		ElseIf glUseScreen = 1006 Then 
			TC2048PaintHiRes()
		End If
	End Sub
	
	Sub TC2048PaintHiColour()
		Dim lne, X As Integer
		'UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim sbyte_Renamed, abyte As Integer
		Dim lTopMost, lLeftMost, lRightMost, lBottomMost As Integer
		
		' // Bob Woodring's (RGW) improvements to display speed (lookup table of colour values)
		Dim lIndex As Integer
		Dim lLneIndex As Integer
		Dim lColIndex As Integer
		
		'gpicDisplay.Visible = False
		
		lTopMost = 191
		lBottomMost = 0
		lLeftMost = 31
		lRightMost = 0
		
		For lne = 0 To 191
			If ScrnLines(lne, 32) = True Then
				If lne < lTopMost Then lTopMost = lne
				If lne > lBottomMost Then lBottomMost = lne
				' // RGW: Get line and column indexes from a lookup table for speed
				lLneIndex = glRowIndex(lne)
				lColIndex = glColIndex(lne)
				For X = 0 To 31
					If ScrnLines(lne, X) = True Then
						If X < lLeftMost Then lLeftMost = X
						If X > lRightMost Then lRightMost = X
						
						' // All screen memory is in the bottom 16K of RAM (page 5)
						sbyte_Renamed = gRAMPage(5, glScreenMem(lne, X))
						abyte = gRAMPage(5, glScreenMem(lne, X) + 8192)
						
						If (abyte And 128) And (bFlashInverse) Then
							' // Swap fore- and back-colours
							abyte = abyte Xor 128
						End If
						
						lIndex = (lColIndex + X + X)
						glBufferBits(lIndex) = gtBitTable(sbyte_Renamed, abyte).dw0
						glBufferBits(lIndex + 1) = gtBitTable(sbyte_Renamed, abyte).dw1
						ScrnLines(lne, X) = False
					End If
				Next X
				ScrnLines(lne, 32) = False
			End If
		Next lne
		
		lLeftMost = lLeftMost * 8
		lRightMost = lRightMost * 8
		StretchDIBits(gpicDC, lLeftMost * glDisplayXMultiplier, (lBottomMost + 1) * glDisplayYMultiplier - 1, (lRightMost - lLeftMost + 8) * glDisplayXMultiplier, -(lBottomMost - lTopMost + 1) * glDisplayYMultiplier, lLeftMost, lTopMost, (lRightMost - lLeftMost) + 8, lBottomMost - lTopMost + 1, glBufferBits(0), bmiBuffer, DIB_RGB_COLORS, SRCCOPY)
		gpicDisplay.Refresh()
		'gpicDisplay.Visible = True
		ScrnNeedRepaint = False
	End Sub
	
	Sub TC2048ScreenPaintScrn1()
		Dim lne, X As Integer
		'UPGRADE_NOTE: sbyte was upgraded to sbyte_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim sbyte_Renamed, abyte As Integer
		Dim lTopMost, lLeftMost, lRightMost, lBottomMost As Integer
		
		' // Bob Woodring's (RGW) improvements to display speed (lookup table of colour values)
		Dim lIndex As Integer
		Dim lLneIndex As Integer
		Dim lColIndex As Integer
		
		lTopMost = 191
		lBottomMost = 0
		lLeftMost = 31
		lRightMost = 0
		
		For lne = 0 To 191
			If ScrnLines(lne, 32) = True Then
				If lne < lTopMost Then lTopMost = lne
				If lne > lBottomMost Then lBottomMost = lne
				' // RGW: Get line and column indexes from a lookup table for speed
				lLneIndex = glRowIndex(lne)
				lColIndex = glColIndex(lne)
				For X = 0 To 31
					If ScrnLines(lne, X) = True Then
						If X < lLeftMost Then lLeftMost = X
						If X > lRightMost Then lRightMost = X
						
						' // All screen memory is in the bottom 16K of RAM (page 5)
						sbyte_Renamed = gRAMPage(5, glScreenMem(lne, X) + 8192)
						abyte = gRAMPage(5, lLneIndex + X + 8192)
						
						If (abyte And 128) And (bFlashInverse) Then
							' // Swap fore- and back-colours
							abyte = abyte Xor 128
						End If
						
						lIndex = (lColIndex + X + X)
						glBufferBits(lIndex) = gtBitTable(sbyte_Renamed, abyte).dw0
						glBufferBits(lIndex + 1) = gtBitTable(sbyte_Renamed, abyte).dw1
						ScrnLines(lne, X) = False
					End If
				Next X
				ScrnLines(lne, 32) = False
			End If
		Next lne
		
		lLeftMost = lLeftMost * 8
		lRightMost = lRightMost * 8
		StretchDIBits(gpicDC, lLeftMost * glDisplayXMultiplier, (lBottomMost + 1) * glDisplayYMultiplier - 1, (lRightMost - lLeftMost + 8) * glDisplayXMultiplier, -(lBottomMost - lTopMost + 1) * glDisplayYMultiplier, lLeftMost, lTopMost, (lRightMost - lLeftMost) + 8, lBottomMost - lTopMost + 1, glBufferBits(0), bmiBuffer, DIB_RGB_COLORS, SRCCOPY)
		gpicDisplay.Refresh()
		
		ScrnNeedRepaint = False
	End Sub
	
	
	Private Function ZXPrinterIn() As Integer
		' //  (64) D6 = 0 if ZXPrinter is present, else 1
		' // (128) D7 = 1 if the stylus in on the paper
		' //   (1) D0 = 0/1 toggle from the encoder disk
		
		If frmZXPrinter.Visible = False Then
			' // Unconnected port
			ZXPrinterIn = 64
			Exit Function
		End If
		
		If lZXPrinterMotorOn Then
			' // Flip the encoder disk bit
			If lZXPrinterEncoder = 1 Then lZXPrinterEncoder = 0 Else lZXPrinterEncoder = 1
			' // For every 0>1 cycle of the encoder disk, draw a pixel if the stylus is on
			If (lZXPrinterEncoder = 0) Or (lZXPrinterX >= 257) Then
				If lZXPrinterStylusOn Then
					If lZXPrinterX > 128 Then
						SetZXPrinterPixel(lZXPrinterX - 2, lZXPrinterY)
					Else
						SetZXPrinterPixel(lZXPrinterX - 1, lZXPrinterY)
					End If
				End If
				lZXPrinterX = lZXPrinterX + 1
			End If
		End If
		
		If lZXPrinterX >= 0 And lZXPrinterX < 128 Then
			' // Stylus 1 is over paper
			ZXPrinterIn = 128 Or lZXPrinterEncoder
		ElseIf lZXPrinterX = 128 Then 
			' // Stylus 1 has left the paper
			ZXPrinterIn = lZXPrinterEncoder
			lZXPrinterX = 129
		ElseIf lZXPrinterX > 128 And lZXPrinterX <= 256 Then 
			' // Stylus 2 is over paper
			ZXPrinterIn = 128 Or lZXPrinterEncoder
		Else
			' // Stylus 2 has left the paper, advance the paper one pixel row
			' // and set the position of Stylus 1 to the start of the next row
			lZXPrinterEncoder = 0
			ZXPrinterIn = lZXPrinterEncoder
			lZXPrinterX = 0
			lZXPrinterY = lZXPrinterY + 1
			If lZXPrinterY >= glZXPrinterBMPHeight Then
				glZXPrinterBMPHeight = lZXPrinterY + 32
				ReDim Preserve gcZXPrinterBits(glZXPrinterBMPHeight * 32)
				bmiZXPrinter.bmiHeader.biHeight = glZXPrinterBMPHeight
			End If
			
			If (lZXPrinterY Mod 8) = 0 Then
				' // Every 8 rows, update the visible display
				If frmZXPrinter.picView.Height > lZXPrinterY Then
					'UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
					'StretchDIBitsMono(frmZXPrinter.picView.hdc, 0, frmZXPrinter.picView.Height, 256, -lZXPrinterY - 1, 0, 0, 256, lZXPrinterY + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
				Else
					'UPGRADE_ISSUE: PictureBox property picView.hdc was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
					'StretchDIBitsMono(frmZXPrinter.picView.hdc, 0, frmZXPrinter.picView.Height, 256, -frmZXPrinter.picView.Height - 1, 0, lZXPrinterY - frmZXPrinter.picView.Height, 256, frmZXPrinter.picView.Height + 1, gcZXPrinterBits(0), bmiZXPrinter, DIB_RGB_COLORS, SRCCOPY)
				End If
				frmZXPrinter.picView.Refresh()
				
				' // Set up the scroll bar properties for the visible display
				' // to allow scrolling back over the material printed so far
				If lZXPrinterY > frmZXPrinter.picView.Height Then
					frmZXPrinter.vs.Minimum = frmZXPrinter.picView.Height \ 8
					frmZXPrinter.vs.Maximum = (lZXPrinterY \ 8 + frmZXPrinter.vs.LargeChange - 1)
					frmZXPrinter.vs.Value = lZXPrinterY \ 8
				Else
					frmZXPrinter.vs.Minimum = 0
					frmZXPrinter.vs.Maximum = (0 + frmZXPrinter.vs.LargeChange - 1)
				End If
			End If
		End If
	End Function
	
	Private Sub ZXPrinterOut(ByRef b As Integer)
		If (b And 4) Then
			lZXPrinterMotorOn = False
		Else
			lZXPrinterMotorOn = True
		End If
		
		If (b And 128) Then
			lZXPrinterStylusOn = True
		Else
			lZXPrinterStylusOn = False
		End If
	End Sub
	
	
	'MM 03.02.2003 - BEGIN
	Private Function PCJoystickToZXJoystick(ByVal lPCJoystick As Integer, ByVal lPCJoystickFire As Integer) As Integer
		
		'API return value
		Dim lResult As Integer
		'Joystick capablities
		Dim uJoyCaps As JOYCAPS
		'Extended joystick-info
		Dim uJoyInfoEx As JOYINFOEX
		'Minimal Up-Value
		Dim lMinimalUp As Integer
		'Minimal Down-Value
		Dim lMinimalDown As Integer
		'Minimal Left-Value
		Dim lMinimalLeft As Integer
		'Minimal Right-Value
		Dim lMinimalRight As Integer
		'Middle X
		Dim lMiddleX As Integer
		'Middle Y
		Dim lMiddleY As Integer
		'Joystick-Value
		Dim lJoyRes As Integer
		
		'Init
		PCJoystickToZXJoystick = 0
		
		'vbSpec reads the joystick capabilities -- the user could theoretically
		'unplugg a joystick and plug another with another capabilities. This
		'code supports analog joysticks only -- I didn't have any digital :)
		lResult = joyGetDevCaps(lPCJoystick, uJoyCaps, Len(uJoyCaps))
		'The uJoyInfoEx will be preared to get the joystick informations
		'The lenght of the structure muss be transfered
		uJoyInfoEx.dwSize = Len(uJoyInfoEx)
		'All joystick information should be got
		uJoyInfoEx.dwFlags = JOY_RETURNALL
		'Joystickinfos will be loaded into uJoyInfoEx
		'Remarks: the joystick must be pluggen as Joystick1
		lResult = joyGetPosEx(lPCJoystick, uJoyInfoEx)
		'Calculate the middle X and y
		lMiddleX = Fix((uJoyCaps.wXmax - uJoyCaps.wXmin) / 2)
		lMiddleY = Fix((uJoyCaps.wYmax - uJoyCaps.wYmin) / 2)
		'Calculate minimal values for the directions
		lMinimalUp = lMiddleY - Fix((uJoyCaps.wYmax - uJoyCaps.wYmin) / 4)
		lMinimalDown = lMiddleY + Fix((uJoyCaps.wYmax - uJoyCaps.wYmin) / 4)
		lMinimalLeft = lMiddleX - Fix((uJoyCaps.wXmax - uJoyCaps.wXmin) / 4)
		lMinimalRight = lMiddleX + Fix((uJoyCaps.wXmax - uJoyCaps.wXmin) / 4)
		'Init joystick value
		lJoyRes = 0
		'Up
		If uJoyInfoEx.dwYpos < lMinimalUp Then
			lJoyRes = CInt(lJoyRes Or zxjdUp)
		End If
		'Down
		If uJoyInfoEx.dwYpos > lMinimalDown Then
			lJoyRes = CInt(lJoyRes Or zxjdDown)
		End If
		'Left
		If uJoyInfoEx.dwXpos < lMinimalLeft Then
			lJoyRes = CInt(lJoyRes Or zxjdLeft)
		End If
		'Right
		If uJoyInfoEx.dwXpos > lMinimalRight Then
			lJoyRes = CInt(lJoyRes Or zxjdRight)
		End If
		'Fire
		If (uJoyInfoEx.dwButtons And CInt(2 ^ lPCJoystickFire)) > 0 Then
			lJoyRes = CInt(lJoyRes Or zxjdFire)
		End If
		'Button
		If uJoyInfoEx.dwButtons <> 0 Then
			lJoyRes = CInt(lJoyRes Or (uJoyInfoEx.dwButtons * zxjdButtonBase))
		End If
		'Return value
		PCJoystickToZXJoystick = lJoyRes
	End Function
	'MM 03.02.2003 - END
	
	'MM JD
	Private Function JoystickInitIN(ByVal port As Integer) As Integer
		
		'ZX-joystick position
		Dim lZXJoystickPosition As Integer
		'Joystick value (as the result of IN)
		Dim lJoyRes As Integer
		'Joystick definition
		Dim jdAction As JOYSTICK_DEFINITION
		'Counter
		Dim lCounter As Integer
		'TMP-Value
		Dim lTMP As Integer
		Dim sTMP As String
		'Result
		Dim lRes As Integer
		
		'Initialise
		lRes = 0
		
		'If Joystick 1 is valid then
		If bJoystick1Valid Then
			'Get joystick position
			lZXJoystickPosition = PCJoystickToZXJoystick(JOYSTICKID1, lPCJoystick1Fire)
			'Up
			If (lZXJoystickPosition And zxjdUp) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_UP).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Down
			If (lZXJoystickPosition And zxjdDown) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_DOWN).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Left
			If (lZXJoystickPosition And zxjdLeft) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_LEFT).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Right
			If (lZXJoystickPosition And zxjdRight) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_RIGHT).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Buttons
			If (lZXJoystickPosition And &HFFFFFFE0) > 0 Then
				'Get button number
				lCounter = CInt(CShort(lZXJoystickPosition And &HFFFFFFE0) / 32)
				lCounter = CInt(System.Math.Log(lCounter) / System.Math.Log(2)) + 1
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lCounter).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Finish
			If lZXJoystickPosition = 0 Then
				If lKey1 > 0 Then
					lRes = 0
					lKey1 = 0
				End If
			End If
		End If
		
		'If Joystick 2 is valid then
		If bJoystick2Valid Then
			'Get joystick position
			lZXJoystickPosition = PCJoystickToZXJoystick(JOYSTICKID2, lPCJoystick2Fire)
			'Up
			If (lZXJoystickPosition And zxjdUp) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_UP).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Down
			If (lZXJoystickPosition And zxjdDown) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_DOWN).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Left
			If (lZXJoystickPosition And zxjdLeft) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_LEFT).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Right
			If (lZXJoystickPosition And zxjdRight) > 0 Then
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_RIGHT).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Buttons
			If (lZXJoystickPosition And &HFFFFFFE0) > 0 Then
				'Get button number
				lCounter = CInt(CShort(lZXJoystickPosition And &HFFFFFFE0) / 32)
				lCounter = CInt(System.Math.Log(lCounter) / System.Math.Log(2)) + 1
				'Get Joystick definition
				'UPGRADE_WARNING: Couldn't resolve default property of object jdAction. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdAction = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter)
				'If the joystick had been
				If ActionDefined(jdAction) Then
					'If the actual port is the up-port of the joystikc
					If (((port And &HFF) = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter).sKey = vbNullString)) Or ((port = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter).lPort) And (aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter).sKey <> vbNullString)) Then
						'Set value
						lTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter).lValue
						sTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lCounter).sKey
						If sTMP <> vbNullString Then
							If lRes = 0 Then
								lRes = lTMP
							Else
								lRes = lRes And lTMP
							End If
						Else
							lRes = lRes Or lTMP
						End If
						lKey1 = 1
					End If
				End If
			End If
			'Finish
			If lZXJoystickPosition = 0 Then
				If lKey1 > 0 Then
					lRes = 0
					lKey1 = 0
				End If
			End If
		End If
		
		'Return value
		JoystickInitIN = lRes
	End Function
	
	'MM JD
	'Get if the actual action has been definet
	Private Function ActionDefined(ByRef jdAction As JOYSTICK_DEFINITION) As Boolean
		'An action is defined if you have port-and-value-pair or a key
		ActionDefined = ((jdAction.lPort <> 0) And (jdAction.lValue <> 0))
	End Function
	
	'MM JD
	'Translate key strokes in port and in values
	Public Sub KeyStrokeToPortValue(ByVal lKeyCode As Integer, ByVal lShift As Integer, ByRef sKey As String, ByRef lPort As Integer, ByRef lValue As Integer)
		
		'Initialise
		sKey = vbNullString
		lPort = 0
		lValue = 0
		
		'Delete current definition
		If lKeyCode = 46 And lShift = 0 Then
			sKey = vbNullString
			lPort = 0
			lValue = 0
			Exit Sub
		End If
		'Symbol shift
		If lKeyCode = 17 And lShift = 2 Then
			sKey = "SYM"
			lPort = 32766
			lValue = 189
			Exit Sub
		End If
		'Caps shift
		If lKeyCode = 16 And lShift = 1 Then
			sKey = "CAP"
			lPort = 65278
			lValue = 190
			Exit Sub
		End If
		'Space
		If lKeyCode = 32 And lShift = 0 Then
			sKey = "SPA"
			lPort = 32766
			lValue = 190
			Exit Sub
		End If
		'The normal keys
		sKey = Chr(lKeyCode)
		'First row
		If sKey = "1" Then
			lPort = 63486
			lValue = 190
			Exit Sub
		End If
		If sKey = "2" Then
			lPort = 63486
			lValue = 189
			Exit Sub
		End If
		If sKey = "3" Then
			lPort = 63486
			lValue = 187
			Exit Sub
		End If
		If sKey = "4" Then
			lPort = 63486
			lValue = 183
			Exit Sub
		End If
		If sKey = "5" Then
			lPort = 63486
			lValue = 175
			Exit Sub
		End If
		If sKey = "6" Then
			lPort = 61438
			lValue = 175
			Exit Sub
		End If
		If sKey = "7" Then
			lPort = 61438
			lValue = 183
			Exit Sub
		End If
		If sKey = "8" Then
			lPort = 61438
			lValue = 187
			Exit Sub
		End If
		If sKey = "9" Then
			lPort = 61438
			lValue = 189
			Exit Sub
		End If
		If sKey = "0" Then
			lPort = 61438
			lValue = 190
			Exit Sub
		End If
		'Second row
		If sKey = "Q" Then
			lPort = 64510
			lValue = 190
			Exit Sub
		End If
		If sKey = "W" Then
			lPort = 64510
			lValue = 189
			Exit Sub
		End If
		If sKey = "E" Then
			lPort = 64510
			lValue = 187
			Exit Sub
		End If
		If sKey = "R" Then
			lPort = 64510
			lValue = 183
			Exit Sub
		End If
		If sKey = "T" Then
			lPort = 64510
			lValue = 175
			Exit Sub
		End If
		If sKey = "Y" Then
			lPort = 57342
			lValue = 175
			Exit Sub
		End If
		If sKey = "U" Then
			lPort = 57342
			lValue = 183
			Exit Sub
		End If
		If sKey = "I" Then
			lPort = 57342
			lValue = 187
			Exit Sub
		End If
		If sKey = "O" Then
			lPort = 57342
			lValue = 189
			Exit Sub
		End If
		If sKey = "P" Then
			lPort = 57342
			lValue = 190
			Exit Sub
		End If
		'Third row
		If sKey = "A" Then
			lPort = 65022
			lValue = 190
			Exit Sub
		End If
		If sKey = "S" Then
			lPort = 65022
			lValue = 189
			Exit Sub
		End If
		If sKey = "D" Then
			lPort = 65022
			lValue = 187
			Exit Sub
		End If
		If sKey = "F" Then
			lPort = 65022
			lValue = 183
			Exit Sub
		End If
		If sKey = "G" Then
			lPort = 65022
			lValue = 175
			Exit Sub
		End If
		If sKey = "H" Then
			lPort = 49150
			lValue = 175
			Exit Sub
		End If
		If sKey = "J" Then
			lPort = 49150
			lValue = 183
			Exit Sub
		End If
		If sKey = "K" Then
			lPort = 49150
			lValue = 187
			Exit Sub
		End If
		If sKey = "L" Then
			lPort = 49150
			lValue = 189
			Exit Sub
		End If
		'Fourth row
		If sKey = "Z" Then
			lPort = 65278
			lValue = 189
			Exit Sub
		End If
		If sKey = "X" Then
			lPort = 65278
			lValue = 187
			Exit Sub
		End If
		If sKey = "C" Then
			lPort = 65278
			lValue = 183
			Exit Sub
		End If
		If sKey = "V" Then
			lPort = 65278
			lValue = 175
			Exit Sub
		End If
		If sKey = "B" Then
			lPort = 32766
			lValue = 175
			Exit Sub
		End If
		If sKey = "N" Then
			lPort = 32766
			lValue = 183
			Exit Sub
		End If
		If sKey = "M" Then
			lPort = 32766
			lValue = 187
			Exit Sub
		End If
		'This key is not a Speccy-key
		sKey = vbNullString
		lPort = 0
		lValue = 0
	End Sub
	
	'MM JD
	'Translate ports and values in keystrokes
	Public Sub PortValueToKeyStroke(ByVal lPort As Integer, ByVal lValue As Integer, ByRef sKey As String)
		
		'Initialise
		sKey = vbNullString
		
		'First row, left
		If lPort = 63486 Then
			If lValue = 190 Then
				sKey = "1"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "2"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "3"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "4"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "5"
				Exit Sub
			End If
		End If
		'First row, right
		If lPort = 61438 Then
			If lValue = 190 Then
				sKey = "0"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "9"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "8"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "7"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "6"
				Exit Sub
			End If
		End If
		'Second row, left
		If lPort = 64510 Then
			If lValue = 190 Then
				sKey = "Q"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "W"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "E"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "R"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "T"
				Exit Sub
			End If
		End If
		'Second row, right
		If lPort = 57342 Then
			If lValue = 190 Then
				sKey = "P"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "O"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "I"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "U"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "Y"
				Exit Sub
			End If
		End If
		'Third row, left
		If lPort = 65022 Then
			If lValue = 190 Then
				sKey = "A"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "S"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "D"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "F"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "G"
				Exit Sub
			End If
		End If
		'Third row, right
		If lPort = 49150 Then
			If lValue = 190 Then
				sKey = "RET"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "L"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "K"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "J"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "H"
				Exit Sub
			End If
		End If
		'Fourth row, left
		If lPort = 65278 Then
			If lValue = 190 Then
				sKey = "CAP"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "Z"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "X"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "C"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "V"
				Exit Sub
			End If
		End If
		'Fourth row, right
		If lPort = 32766 Then
			If lValue = 190 Then
				sKey = "SPA"
				Exit Sub
			End If
			If lValue = 189 Then
				sKey = "SYM"
				Exit Sub
			End If
			If lValue = 187 Then
				sKey = "M"
				Exit Sub
			End If
			If lValue = 183 Then
				sKey = "N"
				Exit Sub
			End If
			If lValue = 175 Then
				sKey = "B"
				Exit Sub
			End If
		End If
		'That is not a key
		sKey = vbNullString
	End Sub
End Module