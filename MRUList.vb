Option Strict Off
Option Explicit On
Friend Class MRUList
	' /*******************************************************************************
	'   MRUList.cls within vbSpec.vbp
	'
	'   Most Recently Used file list handling
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2002 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	Private Const MaxMRUFiles As Short = 5
	Private MRUCount As Integer
	Private MRU(MaxMRUFiles) As String
	
	Public Function AddMRUFile(ByRef sFile As String) As Integer
		Dim l As Integer
		Dim s As String
		
		' // Empty filename, ignore
		If sFile = "" Then Exit Function
		
		' // If file is already in MRU, bring it to the top of the list
		For l = 1 To MaxMRUFiles
			If LCase(sFile) = LCase(MRU(l)) Then
				s = MRU(1)
				MRU(1) = sFile
				MRU(l) = s
				Exit Function
			End If
		Next l
		
		' // Move all the MRU files down one and insert the new file
		' // into position 1
		For l = MaxMRUFiles To 2 Step -1
			MRU(l) = MRU(l - 1)
		Next l
		MRU(1) = sFile
		
		' // Set the MRUCount appropriately
		MRUCount = 0
		For l = 1 To MaxMRUFiles
			If MRU(l) <> "" Then MRUCount = MRUCount + 1
		Next l
		
		AddMRUFile = MRUCount
	End Function
	
	
	Public Function GetMRUCount() As Integer
		GetMRUCount = MRUCount
	End Function
	
	Public Function GetMRUFile(ByRef l As Integer) As String
		If l > 0 And l <= MRUCount Then
			GetMRUFile = MRU(l)
		End If
	End Function
	
	Public Function GetMRUMax() As Integer
		GetMRUMax = MaxMRUFiles
	End Function
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		Dim l As Integer
		Dim s As String
		
		MRUCount = 0
		' // Read files in from registry
		For l = 1 To MaxMRUFiles
			s = GetSetting("Grok", "vbSpec", "MRU" & CStr(l), "")
			If s <> "" Then
				MRUCount = MRUCount + 1
				MRU(MRUCount) = s
			End If
		Next l
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		Dim l, c As Integer
		
		c = 0
		' // Read files in from registry
		For l = 1 To MaxMRUFiles
			If MRU(l) <> "" Then
				c = c + 1
				SaveSetting("Grok", "vbSpec", "MRU" & CStr(c), MRU(l))
			End If
		Next l
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class