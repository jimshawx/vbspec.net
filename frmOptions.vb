Option Strict Off
Option Explicit On
Friend Class frmOptions
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmOptions.frm within vbSpec.vbp
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'   Joystick support code: Miklos Muhi <vbspec@muhi.org>
	'
	'   Copyright (C)1999-2000 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	' // Identifies the emulated model at the time
	' // the dialog is displayed. We need to reboot
	Private lOriginalModel As Integer ' // the Spectrum if this changes.
	
	'MM 12.03.2003 - Joystick support code
	'=================================================================================
	'The first activation flag
	Private bIsFirstActivate As Boolean
	'Property value -- wich PC-joytick will be set up
	Private lPCJoystick As PCJOYSTICKS
	'So many Buttons will be supported
	Private lSupportedButtons As Integer
	'Supported PC-Joysticks
	Public Enum PCJOYSTICKS
		pcjInvalid = -1
		pcjJoystick1 = 0
		pcjJoystick2 = 1
	End Enum
	'=================================================================================
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Close()
	End Sub
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		
		'Variables
		Dim jdTMP As JOYSTICK_DEFINITION
		
		If optSpeed50.Checked Then
			glInterruptDelay = 40
		ElseIf optSpeed100.Checked Then 
			glInterruptDelay = 20
		ElseIf optSpeed200.Checked Then 
			glInterruptDelay = 10
		ElseIf optSpeedFastest.Checked Then 
			glInterruptDelay = 0
		End If
		SaveSetting("Grok", "vbSpec", "InterruptDelay", CStr(glInterruptDelay))
		
		glEmulatedModel = VB6.GetItemData(cboModel, cboModel.SelectedIndex)
		If (glEmulatedModel <> lOriginalModel) Or ((chkSEBasic.CheckState = 1) <> gbSEBasicROM) Then
			' // Model or ROM has been changed - reboot the Spectrum
			SaveSetting("Grok", "vbSpec", "SEBasicROM", CStr(chkSEBasic.CheckState))
			SaveSetting("Grok", "vbSpec", "EmulatedModel", CStr(glEmulatedModel))
			Z80Reset()
		End If
		
		SaveSetting("Grok", "vbSpec", "MouseType", CStr(cboMouseType.SelectedIndex))
		SaveSetting("Grok", "vbSpec", "SoundEnabled", CStr(chkSound.CheckState))
		
		gbSEBasicROM = -chkSEBasic.CheckState
		glMouseType = cboMouseType.SelectedIndex
		
		If optMouseGlobal.Checked Then
			gbMouseGlobal = True
		Else
			gbMouseGlobal = False
		End If
		SaveSetting("Grok", "vbSpec", "MouseGlobal", CStr(gbMouseGlobal))
		
		If glMouseType = MOUSE_NONE Then
			frmMainWnd.picDisplay.Cursor = System.Windows.Forms.Cursors.Default
		Else
			'UPGRADE_ISSUE: PictureBox property picDisplay.MousePointer does not support custom mousepointers. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="45116EAB-7060-405E-8ABE-9DBB40DC2E86"'
			'frmMainWnd.picDisplay.Cursor = vbCustom
		End If
		
		If gbSoundEnabled Then
			If chkSound.CheckState = 0 Then
				CloseWaveOut()
			End If
		Else
			If chkSound.CheckState = 1 Then
				gbSoundEnabled = InitializeWaveOut
			End If
		End If
		
		'MM 12.03.2003 - Joystick support code
		'=================================================================================
		'Save values
		If modSpectrum.bJoystick1Valid Then
			modSpectrum.lPCJoystick1Is = VB6.GetItemData(cmbZXJoystick(0), cmbZXJoystick(0).SelectedIndex)
			If (modSpectrum.lPCJoystick1Is <> modSpectrum.ZXJOYSTICKS.zxjInvalid) And (modSpectrum.lPCJoystick1Is <> modSpectrum.ZXJOYSTICKS.zxjUserDefined) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object jdTMP. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdTMP = aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.lPCJoystick1Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick1Fire + 1)
				aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.lPCJoystick1Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick1Fire + 1).sKey = vbNullString
				aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.lPCJoystick1Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick1Fire + 1).lPort = 0
				aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.lPCJoystick1Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick1Fire + 1).lValue = 0
				modSpectrum.lPCJoystick1Fire = VB6.GetItemData(cmbFire(0), cmbFire(0).SelectedIndex)
				'UPGRADE_WARNING: Couldn't resolve default property of object aJoystikDefinitionTable(JDT_JOYSTICK1, lPCJoystick1Is, JDT_BUTTON_BASE + lPCJoystick1Fire + 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.lPCJoystick1Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick1Fire + 1) = jdTMP
			End If
		End If
		If modSpectrum.bJoystick2Valid Then
			modSpectrum.lPCJoystick2Is = VB6.GetItemData(cmbZXJoystick(1), cmbZXJoystick(1).SelectedIndex)
			If (modSpectrum.lPCJoystick2Is <> modSpectrum.ZXJOYSTICKS.zxjInvalid) And (modSpectrum.lPCJoystick2Is <> modSpectrum.ZXJOYSTICKS.zxjUserDefined) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object jdTMP. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				jdTMP = aJoystikDefinitionTable(JDT_JOYSTICK2, modSpectrum.lPCJoystick2Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick2Fire + 1)
				aJoystikDefinitionTable(JDT_JOYSTICK2, modSpectrum.lPCJoystick2Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick2Fire + 1).sKey = vbNullString
				aJoystikDefinitionTable(JDT_JOYSTICK2, modSpectrum.lPCJoystick2Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick2Fire + 1).lPort = 0
				aJoystikDefinitionTable(JDT_JOYSTICK2, modSpectrum.lPCJoystick2Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick2Fire + 1).lValue = 0
				modSpectrum.lPCJoystick2Fire = VB6.GetItemData(cmbFire(1), cmbFire(1).SelectedIndex)
				'UPGRADE_WARNING: Couldn't resolve default property of object aJoystikDefinitionTable(JDT_JOYSTICK2, lPCJoystick2Is, JDT_BUTTON_BASE + lPCJoystick2Fire + 1). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				aJoystikDefinitionTable(JDT_JOYSTICK2, modSpectrum.lPCJoystick2Is, JDT_BUTTON_BASE + modSpectrum.lPCJoystick2Fire + 1) = jdTMP
			End If
		End If
		'=================================================================================
		
		Me.Close()
	End Sub
	
	Private Sub frmOptions_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Select Case glInterruptDelay
			Case 40
				' // 40ms delay = 50% Spectrum speed
				optSpeed50.Checked = True
			Case 20
				' // 20ms delay = 100% Spectrum speed
				optSpeed100.Checked = True
			Case 10
				' // 10ms delay = 200% Spectrum speed
				optSpeed200.Checked = True
			Case 0
				' // 0 delay = run as fast as possible
				optSpeedFastest.Checked = True
		End Select
		
		cboModel.Items.Add(New VB6.ListBoxItem("ZX Spectrum 48K", 0))
		cboModel.Items.Add(New VB6.ListBoxItem("ZX Spectrum 128", 1))
		cboModel.Items.Add(New VB6.ListBoxItem("ZX Spectrum +2", 2))
		cboModel.Items.Add(New VB6.ListBoxItem("Timex TC2048", 5))
		
		lOriginalModel = glEmulatedModel
		Select Case glEmulatedModel
			Case 0 ' // 48K
				cboModel.SelectedIndex = 0
			Case 1 ' // 128K
				cboModel.SelectedIndex = 1
			Case 2 ' // +2
				cboModel.SelectedIndex = 2
			Case 5 ' // TC2048
				cboModel.SelectedIndex = 3
		End Select
		
		If gbSoundEnabled Then chkSound.CheckState = System.Windows.Forms.CheckState.Checked Else chkSound.CheckState = System.Windows.Forms.CheckState.Unchecked
		If gbSEBasicROM Then chkSEBasic.CheckState = System.Windows.Forms.CheckState.Checked Else chkSEBasic.CheckState = System.Windows.Forms.CheckState.Unchecked
		
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If Dir(My.Application.Info.DirectoryPath & "\sebasic.rom") = "" Then chkSEBasic.Enabled = False
		
		If glMouseType = MOUSE_KEMPSTON Then
			cboMouseType.SelectedIndex = 1
		ElseIf glMouseType = MOUSE_AMIGA Then 
			cboMouseType.SelectedIndex = 2
		Else
			cboMouseType.SelectedIndex = 0
		End If
		
		If gbMouseGlobal Then optMouseGlobal.Checked = True Else optMouseVB.Checked = True
		
		'MM 12.03.2003 - Joystick support code
		'=================================================================================
		'Set up the controles
		With cmbZXJoystick(0)
			.Items.Clear()
			.Items.Add(New VB6.ListBoxItem("(Invalid)", modSpectrum.ZXJOYSTICKS.zxjInvalid))
			.Items.Add(New VB6.ListBoxItem("Kempston joystick", modSpectrum.ZXJOYSTICKS.zxjKempston))
			.Items.Add(New VB6.ListBoxItem("Cursor joystick", modSpectrum.ZXJOYSTICKS.zxjCursor))
			.Items.Add(New VB6.ListBoxItem("Sinclair 1", modSpectrum.ZXJOYSTICKS.zxjSinclair1))
			.Items.Add(New VB6.ListBoxItem("Sinclair 2", modSpectrum.ZXJOYSTICKS.zxjSinclair2))
			.Items.Add(New VB6.ListBoxItem("Fuller Box", modSpectrum.ZXJOYSTICKS.zxjFullerBox))
			'MM JD
			.Items.Add(New VB6.ListBoxItem("User defined", modSpectrum.ZXJOYSTICKS.zxjUserDefined))
		End With
		With cmbZXJoystick(0)
			.Items.Clear()
			.Items.Add(New VB6.ListBoxItem("(Invalid)", modSpectrum.ZXJOYSTICKS.zxjInvalid))
			.Items.Add(New VB6.ListBoxItem("Kempston joystick", modSpectrum.ZXJOYSTICKS.zxjKempston))
			.Items.Add(New VB6.ListBoxItem("Cursor joystick", modSpectrum.ZXJOYSTICKS.zxjCursor))
			.Items.Add(New VB6.ListBoxItem("Sinclair 1", modSpectrum.ZXJOYSTICKS.zxjSinclair1))
			.Items.Add(New VB6.ListBoxItem("Sinclair 2", modSpectrum.ZXJOYSTICKS.zxjSinclair2))
			.Items.Add(New VB6.ListBoxItem("Fuller Box", modSpectrum.ZXJOYSTICKS.zxjFullerBox))
			'MM JD
			.Items.Add(New VB6.ListBoxItem("User defined", modSpectrum.ZXJOYSTICKS.zxjUserDefined))
		End With
		'The first activation starts
		bIsFirstActivate = True
		'=================================================================================
	End Sub
	
	'MM 12.03.2003 - Joystick support code
	'=================================================================================
	'This code executes every time the window becomes visible
	'UPGRADE_WARNING: Form event frmOptions.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmOptions_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		
		'Help-Variable
		Dim iCounter As Short
		
		'By the first activate only
		If bIsFirstActivate Then
			'Validity check
			lblZXJoystick(0).Enabled = modSpectrum.bJoystick1Valid
			lblFire(0).Enabled = modSpectrum.bJoystick1Valid
			cmbZXJoystick(0).Enabled = modSpectrum.bJoystick1Valid
			cmbFire(0).Enabled = modSpectrum.bJoystick1Valid
			cmdDefine(0).Enabled = modSpectrum.bJoystick1Valid
			lblZXJoystick(1).Enabled = modSpectrum.bJoystick2Valid
			lblFire(1).Enabled = modSpectrum.bJoystick2Valid
			cmbZXJoystick(1).Enabled = modSpectrum.bJoystick2Valid
			cmbFire(1).Enabled = modSpectrum.bJoystick2Valid
			cmdDefine(1).Enabled = modSpectrum.bJoystick2Valid
			'If joystick1 is valid
			If modSpectrum.bJoystick1Valid Then
				'Get supported buttons
				lSupportedButtons = modSpectrum.lPCJoystick1Buttons
				'Initialise Combos
				With cmbFire(0)
					'Add invalid button
					.Items.Add("(Invalid)")
					'Initialise ItemData
					'UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
					'VB6.SetItemData(cmbFire(0), cmbFire(0).NewIndex, -1)
					'Add valid buttons
					For iCounter = 1 To lSupportedButtons
						'Add button
						.Items.Add("Button " & Trim(CStr(iCounter)))
						'Initialise ItemData
						'UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
						'VB6.SetItemData(cmbFire(0), cmbFire(0).NewIndex, iCounter - 1)
					Next iCounter
				End With
				'Refresh the form
				Me.Refresh()
				'Joystick1 values
				If modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjInvalid Then
					cmbZXJoystick(0).SelectedIndex = 0
				Else
					cmbZXJoystick(0).SelectedIndex = modSpectrum.lPCJoystick1Is + 1
				End If
				If modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid Then
					cmbFire(0).SelectedIndex = 0
				Else
					cmbFire(0).SelectedIndex = modSpectrum.lPCJoystick1Fire + 1
				End If
			End If
			'If joystick2 is valid
			If modSpectrum.bJoystick2Valid Then
				'Get supported buttons
				lSupportedButtons = modSpectrum.lPCJoystick1Buttons
				'Initialise Combos
				With cmbFire(1)
					'Add invalid button
					.Items.Add("(Invalid)")
					'Initialise ItemData
					'UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
					'VB6.SetItemData(cmbFire(1), cmbFire(1).NewIndex, -1)
					'Add valid buttons
					For iCounter = 1 To lSupportedButtons
						'Add button
						.Items.Add("Button " & Trim(CStr(iCounter)))
						'Initialise ItemData
						'UPGRADE_ISSUE: ComboBox property cmbFire.Item.NewIndex was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="F649E068-7137-45E5-AC20-4D80A3CC70AC"'
						'VB6.SetItemData(cmbFire(1), cmbFire(1).NewIndex, iCounter - 1)
					Next iCounter
				End With
				'Refresh the form
				Me.Refresh()
				'Joystick2 Values
				If modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjInvalid Then
					cmbZXJoystick(1).SelectedIndex = 0
				Else
					cmbZXJoystick(1).SelectedIndex = modSpectrum.lPCJoystick2Is + 1
				End If
				If modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid Then
					cmbFire(1).SelectedIndex = 0
				Else
					cmbFire(1).SelectedIndex = modSpectrum.lPCJoystick2Fire + 1
				End If
			End If
			'The first activate ends here
			bIsFirstActivate = False
		End If
	End Sub
	'=================================================================================
	
	'MM JD
	'User Joystick definition support
	Private Sub cmdDefine_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDefine.Click
		Dim Index As Short = cmdDefine.GetIndex(eventSender)
		'Variables
		Dim frmDefJoystickWnd As frmDefJoystick
		Dim lCounter As Integer
		'You cannot redefine an invalid joystick
		If VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex) = modSpectrum.ZXJOYSTICKS.zxjInvalid Then
			Exit Sub
		End If
		'Error handel
		On Error GoTo CMDDEFINECLICK_ERROR
		'Initialise
		frmDefJoystickWnd = New frmDefJoystick
		'UPGRADE_ISSUE: Load statement is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="B530EFF2-3132-48F8-B8BC-D88AF543D321"'
		'Load(frmDefJoystickWnd)
		frmDefJoystickWnd.JoystickNo = Index + 1
		frmDefJoystickWnd.JoystickSetting = VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex)
		frmDefJoystickWnd.FireButton = cmbFire(Index).SelectedIndex
		'Show
		frmDefJoystickWnd.ShowDialog()
		'If the changes where taken over
		If frmDefJoystickWnd.OK Then
			'That is user defined
			For lCounter = 0 To (cmbZXJoystick(Index).Items.Count - 1)
				If VB6.GetItemData(cmbZXJoystick(Index), lCounter) = modSpectrum.ZXJOYSTICKS.zxjUserDefined Then
					cmbZXJoystick(Index).SelectedIndex = lCounter
				End If
			Next lCounter
		End If
		'Set to Nothing
		If Not frmDefJoystickWnd Is Nothing Then
			'UPGRADE_NOTE: Object frmDefJoystickWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			frmDefJoystickWnd = Nothing
		End If
		'The End
		Exit Sub
CMDDEFINECLICK_ERROR: 
		'Report error
		MsgBox(Err.Description, MsgBoxStyle.Information + MsgBoxStyle.OKOnly)
		'Set to Nothing
		If Not frmDefJoystickWnd Is Nothing Then
			'UPGRADE_NOTE: Object frmDefJoystickWnd may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
			frmDefJoystickWnd = Nothing
		End If
	End Sub
	
	'MM JD
	'You cannot redefine an invalid joystick
	'UPGRADE_WARNING: Event cmbZXJoystick.SelectedIndexChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub cmbZXJoystick_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbZXJoystick.SelectedIndexChanged
		Dim Index As Short = cmbZXJoystick.GetIndex(eventSender)
		cmdDefine(Index).Enabled = CBool(VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex) <> modSpectrum.ZXJOYSTICKS.zxjInvalid)
		cmbFire(Index).Enabled = CBool(VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex) <> modSpectrum.ZXJOYSTICKS.zxjInvalid) And CBool(VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex) <> modSpectrum.ZXJOYSTICKS.zxjUserDefined)
		lblFire(Index).Enabled = CBool(VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex) <> modSpectrum.ZXJOYSTICKS.zxjInvalid) And CBool(VB6.GetItemData(cmbZXJoystick(Index), cmbZXJoystick(Index).SelectedIndex) <> modSpectrum.ZXJOYSTICKS.zxjUserDefined)
	End Sub






	'Private Sub ts1_ClickEvent(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles ts1.ClickEvent
	'Dim l As Integer

	'For l = 1 To ts1.Tabs.count
	'	picFrame(l).Visible = ts1.Tabs(l).Selected
	'Next l
	'End Sub


	'Private Sub ts1_MouseDownEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxMSComctlLib.ITabStripEvents_MouseDownEvent) Handles ts1.MouseDownEvent
	'	ts1_ClickEvent(ts1, New System.EventArgs())
	'End Sub
End Class