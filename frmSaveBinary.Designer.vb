<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmSaveBinary
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents _cboBase_1 As System.Windows.Forms.ComboBox
	Public WithEvents txtLength As System.Windows.Forms.TextBox
	Public WithEvents txtFile As System.Windows.Forms.TextBox
	Public WithEvents cmdOpen As System.Windows.Forms.Button
	Public WithEvents txtAddr As System.Windows.Forms.TextBox
	Public WithEvents _cboBase_0 As System.Windows.Forms.ComboBox
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public dlgCommonOpen As System.Windows.Forms.OpenFileDialog
	Public dlgCommonSave As System.Windows.Forms.SaveFileDialog
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents cboBase As Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmSaveBinary))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me._cboBase_1 = New System.Windows.Forms.ComboBox
		Me.txtLength = New System.Windows.Forms.TextBox
		Me.txtFile = New System.Windows.Forms.TextBox
		Me.cmdOpen = New System.Windows.Forms.Button
		Me.txtAddr = New System.Windows.Forms.TextBox
		Me._cboBase_0 = New System.Windows.Forms.ComboBox
		Me.cmdOK = New System.Windows.Forms.Button
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.dlgCommonOpen = New System.Windows.Forms.OpenFileDialog
		Me.dlgCommonSave = New System.Windows.Forms.SaveFileDialog
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.cboBase = New Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(components)
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.cboBase, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Save Binary Data"
		Me.ClientSize = New System.Drawing.Size(421, 92)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmSaveBinary"
		Me._cboBase_1.Size = New System.Drawing.Size(65, 21)
		Me._cboBase_1.Location = New System.Drawing.Point(352, 32)
		Me._cboBase_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._cboBase_1.TabIndex = 10
		Me._cboBase_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cboBase_1.BackColor = System.Drawing.SystemColors.Window
		Me._cboBase_1.CausesValidation = True
		Me._cboBase_1.Enabled = True
		Me._cboBase_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._cboBase_1.IntegralHeight = True
		Me._cboBase_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._cboBase_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cboBase_1.Sorted = False
		Me._cboBase_1.TabStop = True
		Me._cboBase_1.Visible = True
		Me._cboBase_1.Name = "_cboBase_1"
		Me.txtLength.AutoSize = False
		Me.txtLength.Size = New System.Drawing.Size(73, 21)
		Me.txtLength.Location = New System.Drawing.Point(276, 32)
		Me.txtLength.TabIndex = 9
		Me.txtLength.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLength.AcceptsReturn = True
		Me.txtLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLength.BackColor = System.Drawing.SystemColors.Window
		Me.txtLength.CausesValidation = True
		Me.txtLength.Enabled = True
		Me.txtLength.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLength.HideSelection = True
		Me.txtLength.ReadOnly = False
		Me.txtLength.Maxlength = 0
		Me.txtLength.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLength.MultiLine = False
		Me.txtLength.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLength.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLength.TabStop = True
		Me.txtLength.Visible = True
		Me.txtLength.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLength.Name = "txtLength"
		Me.txtFile.AutoSize = False
		Me.txtFile.Size = New System.Drawing.Size(301, 21)
		Me.txtFile.Location = New System.Drawing.Point(88, 4)
		Me.txtFile.TabIndex = 3
		Me.txtFile.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFile.AcceptsReturn = True
		Me.txtFile.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtFile.BackColor = System.Drawing.SystemColors.Window
		Me.txtFile.CausesValidation = True
		Me.txtFile.Enabled = True
		Me.txtFile.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtFile.HideSelection = True
		Me.txtFile.ReadOnly = False
		Me.txtFile.Maxlength = 0
		Me.txtFile.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtFile.MultiLine = False
		Me.txtFile.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtFile.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtFile.TabStop = True
		Me.txtFile.Visible = True
		Me.txtFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtFile.Name = "txtFile"
		Me.cmdOpen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOpen.Text = "..."
		Me.cmdOpen.Size = New System.Drawing.Size(25, 21)
		Me.cmdOpen.Location = New System.Drawing.Point(392, 4)
		Me.cmdOpen.TabIndex = 4
		Me.cmdOpen.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOpen.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOpen.CausesValidation = True
		Me.cmdOpen.Enabled = True
		Me.cmdOpen.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOpen.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOpen.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOpen.TabStop = True
		Me.cmdOpen.Name = "cmdOpen"
		Me.txtAddr.AutoSize = False
		Me.txtAddr.Size = New System.Drawing.Size(73, 21)
		Me.txtAddr.Location = New System.Drawing.Point(88, 32)
		Me.txtAddr.TabIndex = 6
		Me.txtAddr.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtAddr.AcceptsReturn = True
		Me.txtAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtAddr.BackColor = System.Drawing.SystemColors.Window
		Me.txtAddr.CausesValidation = True
		Me.txtAddr.Enabled = True
		Me.txtAddr.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAddr.HideSelection = True
		Me.txtAddr.ReadOnly = False
		Me.txtAddr.Maxlength = 0
		Me.txtAddr.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAddr.MultiLine = False
		Me.txtAddr.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAddr.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAddr.TabStop = True
		Me.txtAddr.Visible = True
		Me.txtAddr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtAddr.Name = "txtAddr"
		Me._cboBase_0.Size = New System.Drawing.Size(65, 21)
		Me._cboBase_0.Location = New System.Drawing.Point(164, 32)
		Me._cboBase_0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._cboBase_0.TabIndex = 7
		Me._cboBase_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cboBase_0.BackColor = System.Drawing.SystemColors.Window
		Me._cboBase_0.CausesValidation = True
		Me._cboBase_0.Enabled = True
		Me._cboBase_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._cboBase_0.IntegralHeight = True
		Me._cboBase_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._cboBase_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cboBase_0.Sorted = False
		Me._cboBase_0.TabStop = True
		Me._cboBase_0.Visible = True
		Me._cboBase_0.Name = "_cboBase_0"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(81, 25)
		Me.cmdOK.Location = New System.Drawing.Point(248, 60)
		Me.cmdOK.TabIndex = 0
		Me.cmdOK.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(81, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(336, 60)
		Me.cmdCancel.TabIndex = 1
		Me.cmdCancel.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label3.Text = "&Length:"
		Me.Label3.Size = New System.Drawing.Size(41, 13)
		Me.Label3.Location = New System.Drawing.Point(232, 36)
		Me.Label3.TabIndex = 8
		Me.Label3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label1.Text = "&Filename:"
		Me.Label1.Size = New System.Drawing.Size(53, 17)
		Me.Label1.Location = New System.Drawing.Point(32, 8)
		Me.Label1.TabIndex = 2
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label2.Text = "Memory &Address:"
		Me.Label2.Size = New System.Drawing.Size(81, 13)
		Me.Label2.Location = New System.Drawing.Point(4, 36)
		Me.Label2.TabIndex = 5
		Me.Label2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Controls.Add(_cboBase_1)
		Me.Controls.Add(txtLength)
		Me.Controls.Add(txtFile)
		Me.Controls.Add(cmdOpen)
		Me.Controls.Add(txtAddr)
		Me.Controls.Add(_cboBase_0)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label1)
		Me.Controls.Add(Label2)
		Me.cboBase.SetIndex(_cboBase_1, CType(1, Short))
		Me.cboBase.SetIndex(_cboBase_0, CType(0, Short))
		CType(Me.cboBase, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class