<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmPoke
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdPeek As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	Public WithEvents cmdPoke As System.Windows.Forms.Button
	Public WithEvents txtValue As System.Windows.Forms.TextBox
	Public WithEvents txtAddress As System.Windows.Forms.TextBox
	Public WithEvents lblValue As System.Windows.Forms.Label
	Public WithEvents lblAddress As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPoke))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdPeek = New System.Windows.Forms.Button
		Me.cmdCancel = New System.Windows.Forms.Button
		Me.cmdPoke = New System.Windows.Forms.Button
		Me.txtValue = New System.Windows.Forms.TextBox
		Me.txtAddress = New System.Windows.Forms.TextBox
		Me.lblValue = New System.Windows.Forms.Label
		Me.lblAddress = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "Poke Memory"
		Me.ClientSize = New System.Drawing.Size(312, 85)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmPoke"
		Me.cmdPeek.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdPeek.Text = "Peek"
		Me.cmdPeek.Size = New System.Drawing.Size(109, 22)
		Me.cmdPeek.Location = New System.Drawing.Point(196, 6)
		Me.cmdPeek.TabIndex = 6
		Me.cmdPeek.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdPeek.BackColor = System.Drawing.SystemColors.Control
		Me.cmdPeek.CausesValidation = True
		Me.cmdPeek.Enabled = True
		Me.cmdPeek.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdPeek.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdPeek.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdPeek.TabStop = True
		Me.cmdPeek.Name = "cmdPeek"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "&Close"
		Me.cmdCancel.Size = New System.Drawing.Size(109, 22)
		Me.cmdCancel.Location = New System.Drawing.Point(196, 54)
		Me.cmdCancel.TabIndex = 5
		Me.ToolTip1.SetToolTip(Me.cmdCancel, "This button closes the window (the others don't)")
		Me.cmdCancel.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		Me.cmdPoke.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdPoke.Text = "&Poke"
		Me.AcceptButton = Me.cmdPoke
		Me.cmdPoke.Size = New System.Drawing.Size(109, 22)
		Me.cmdPoke.Location = New System.Drawing.Point(196, 30)
		Me.cmdPoke.TabIndex = 4
		Me.ToolTip1.SetToolTip(Me.cmdPoke, "The poke-operation will be completed and the controls reset.")
		Me.cmdPoke.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdPoke.BackColor = System.Drawing.SystemColors.Control
		Me.cmdPoke.CausesValidation = True
		Me.cmdPoke.Enabled = True
		Me.cmdPoke.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdPoke.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdPoke.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdPoke.TabStop = True
		Me.cmdPoke.Name = "cmdPoke"
		Me.txtValue.AutoSize = False
		Me.txtValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtValue.Size = New System.Drawing.Size(87, 21)
		Me.txtValue.Location = New System.Drawing.Point(99, 29)
		Me.txtValue.TabIndex = 3
		Me.ToolTip1.SetToolTip(Me.txtValue, "Here comes the value that must be poked (0..255)")
		Me.txtValue.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtValue.AcceptsReturn = True
		Me.txtValue.BackColor = System.Drawing.SystemColors.Window
		Me.txtValue.CausesValidation = True
		Me.txtValue.Enabled = True
		Me.txtValue.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtValue.HideSelection = True
		Me.txtValue.ReadOnly = False
		Me.txtValue.Maxlength = 0
		Me.txtValue.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtValue.MultiLine = False
		Me.txtValue.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtValue.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtValue.TabStop = True
		Me.txtValue.Visible = True
		Me.txtValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtValue.Name = "txtValue"
		Me.txtAddress.AutoSize = False
		Me.txtAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		Me.txtAddress.Size = New System.Drawing.Size(87, 21)
		Me.txtAddress.Location = New System.Drawing.Point(99, 6)
		Me.txtAddress.TabIndex = 1
		Me.ToolTip1.SetToolTip(Me.txtAddress, "Enter here a number greater than 16384 (Spectrum RAM)")
		Me.txtAddress.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtAddress.AcceptsReturn = True
		Me.txtAddress.BackColor = System.Drawing.SystemColors.Window
		Me.txtAddress.CausesValidation = True
		Me.txtAddress.Enabled = True
		Me.txtAddress.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtAddress.HideSelection = True
		Me.txtAddress.ReadOnly = False
		Me.txtAddress.Maxlength = 0
		Me.txtAddress.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtAddress.MultiLine = False
		Me.txtAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtAddress.TabStop = True
		Me.txtAddress.Visible = True
		Me.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtAddress.Name = "txtAddress"
		Me.lblValue.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblValue.Text = "&Value"
		Me.lblValue.Size = New System.Drawing.Size(84, 13)
		Me.lblValue.Location = New System.Drawing.Point(9, 34)
		Me.lblValue.TabIndex = 2
		Me.lblValue.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblValue.BackColor = System.Drawing.SystemColors.Control
		Me.lblValue.Enabled = True
		Me.lblValue.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblValue.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblValue.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblValue.UseMnemonic = True
		Me.lblValue.Visible = True
		Me.lblValue.AutoSize = False
		Me.lblValue.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblValue.Name = "lblValue"
		Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblAddress.Text = "&Address"
		Me.lblAddress.Size = New System.Drawing.Size(84, 13)
		Me.lblAddress.Location = New System.Drawing.Point(9, 10)
		Me.lblAddress.TabIndex = 0
		Me.lblAddress.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblAddress.BackColor = System.Drawing.SystemColors.Control
		Me.lblAddress.Enabled = True
		Me.lblAddress.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblAddress.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblAddress.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblAddress.UseMnemonic = True
		Me.lblAddress.Visible = True
		Me.lblAddress.AutoSize = False
		Me.lblAddress.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblAddress.Name = "lblAddress"
		Me.Controls.Add(cmdPeek)
		Me.Controls.Add(cmdCancel)
		Me.Controls.Add(cmdPoke)
		Me.Controls.Add(txtValue)
		Me.Controls.Add(txtAddress)
		Me.Controls.Add(lblValue)
		Me.Controls.Add(lblAddress)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class