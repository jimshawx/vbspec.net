<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmOptions
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents _cmdDefine_0 As System.Windows.Forms.Button
	Public WithEvents _cmdDefine_1 As System.Windows.Forms.Button
	Public WithEvents _cmbZXJoystick_1 As System.Windows.Forms.ComboBox
	Public WithEvents _cmbFire_1 As System.Windows.Forms.ComboBox
	Public WithEvents _cmbZXJoystick_0 As System.Windows.Forms.ComboBox
	Public WithEvents _cmbFire_0 As System.Windows.Forms.ComboBox
	Public WithEvents _lblZXJoystick_1 As System.Windows.Forms.Label
	Public WithEvents _lblFire_1 As System.Windows.Forms.Label
	Public WithEvents _lblZXJoystick_0 As System.Windows.Forms.Label
	Public WithEvents _lblFire_0 As System.Windows.Forms.Label
	Public WithEvents _picFrame_3 As System.Windows.Forms.Panel
	Public WithEvents cboMouseType As System.Windows.Forms.ComboBox
	Public WithEvents optMouseGlobal As System.Windows.Forms.RadioButton
	Public WithEvents optMouseVB As System.Windows.Forms.RadioButton
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents Label1 As System.Windows.Forms.Label
	Public WithEvents _picFrame_2 As System.Windows.Forms.Panel
	Public WithEvents chkSEBasic As System.Windows.Forms.CheckBox
	Public WithEvents optSpeed200 As System.Windows.Forms.RadioButton
	Public WithEvents optSpeed50 As System.Windows.Forms.RadioButton
	Public WithEvents optSpeed100 As System.Windows.Forms.RadioButton
	Public WithEvents optSpeedFastest As System.Windows.Forms.RadioButton
	Public WithEvents fraStd As System.Windows.Forms.GroupBox
	Public WithEvents chkSound As System.Windows.Forms.CheckBox
	Public WithEvents cboModel As System.Windows.Forms.ComboBox
	Public WithEvents lblStatic As System.Windows.Forms.Label
	Public WithEvents _picFrame_1 As System.Windows.Forms.Panel
	Public WithEvents cmdOK As System.Windows.Forms.Button
	Public WithEvents cmdCancel As System.Windows.Forms.Button
	'Public WithEvents ts1 As AxMSComctlLib.AxTabStrip
	Public WithEvents cmbFire As Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray
	Public WithEvents cmbZXJoystick As Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray
	Public WithEvents cmdDefine As Microsoft.VisualBasic.Compatibility.VB6.ButtonArray
	Public WithEvents lblFire As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents lblZXJoystick As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents picFrame As Microsoft.VisualBasic.Compatibility.VB6.PanelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmOptions))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me._picFrame_3 = New System.Windows.Forms.Panel
		Me._cmdDefine_0 = New System.Windows.Forms.Button
		Me._cmdDefine_1 = New System.Windows.Forms.Button
		Me._cmbZXJoystick_1 = New System.Windows.Forms.ComboBox
		Me._cmbFire_1 = New System.Windows.Forms.ComboBox
		Me._cmbZXJoystick_0 = New System.Windows.Forms.ComboBox
		Me._cmbFire_0 = New System.Windows.Forms.ComboBox
		Me._lblZXJoystick_1 = New System.Windows.Forms.Label
		Me._lblFire_1 = New System.Windows.Forms.Label
		Me._lblZXJoystick_0 = New System.Windows.Forms.Label
		Me._lblFire_0 = New System.Windows.Forms.Label
		Me._picFrame_2 = New System.Windows.Forms.Panel
		Me.cboMouseType = New System.Windows.Forms.ComboBox
		Me.Frame1 = New System.Windows.Forms.GroupBox
		Me.optMouseGlobal = New System.Windows.Forms.RadioButton
		Me.optMouseVB = New System.Windows.Forms.RadioButton
		Me.Label1 = New System.Windows.Forms.Label
		Me._picFrame_1 = New System.Windows.Forms.Panel
		Me.chkSEBasic = New System.Windows.Forms.CheckBox
		Me.fraStd = New System.Windows.Forms.GroupBox
		Me.optSpeed200 = New System.Windows.Forms.RadioButton
		Me.optSpeed50 = New System.Windows.Forms.RadioButton
		Me.optSpeed100 = New System.Windows.Forms.RadioButton
		Me.optSpeedFastest = New System.Windows.Forms.RadioButton
		Me.chkSound = New System.Windows.Forms.CheckBox
		Me.cboModel = New System.Windows.Forms.ComboBox
		Me.lblStatic = New System.Windows.Forms.Label
		Me.cmdOK = New System.Windows.Forms.Button
		Me.cmdCancel = New System.Windows.Forms.Button
		'Me.ts1 = New AxMSComctlLib.AxTabStrip
		Me.cmbFire = New Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(components)
		Me.cmbZXJoystick = New Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(components)
		Me.cmdDefine = New Microsoft.VisualBasic.Compatibility.VB6.ButtonArray(components)
		Me.lblFire = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.lblZXJoystick = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(components)
		Me.picFrame = New Microsoft.VisualBasic.Compatibility.VB6.PanelArray(components)
		Me._picFrame_3.SuspendLayout()
		Me._picFrame_2.SuspendLayout()
		Me.Frame1.SuspendLayout()
		Me._picFrame_1.SuspendLayout()
		Me.fraStd.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		'CType(Me.ts1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbFire, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmbZXJoystick, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.cmdDefine, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.lblFire, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.lblZXJoystick, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.picFrame, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "General Settings"
		Me.ClientSize = New System.Drawing.Size(355, 263)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmOptions"
		Me._picFrame_3.Size = New System.Drawing.Size(337, 185)
		Me._picFrame_3.Location = New System.Drawing.Point(8, 32)
		Me._picFrame_3.TabIndex = 19
		Me._picFrame_3.TabStop = False
		Me._picFrame_3.Visible = False
		Me._picFrame_3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._picFrame_3.Dock = System.Windows.Forms.DockStyle.None
		Me._picFrame_3.BackColor = System.Drawing.SystemColors.Control
		Me._picFrame_3.CausesValidation = True
		Me._picFrame_3.Enabled = True
		Me._picFrame_3.ForeColor = System.Drawing.SystemColors.ControlText
		Me._picFrame_3.Cursor = System.Windows.Forms.Cursors.Default
		Me._picFrame_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picFrame_3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picFrame_3.Name = "_picFrame_3"
		Me._cmdDefine_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me._cmdDefine_0.Text = "Define..."
		Me._cmdDefine_0.Size = New System.Drawing.Size(73, 25)
		Me._cmdDefine_0.Location = New System.Drawing.Point(256, 52)
		Me._cmdDefine_0.TabIndex = 24
		Me._cmdDefine_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cmdDefine_0.BackColor = System.Drawing.SystemColors.Control
		Me._cmdDefine_0.CausesValidation = True
		Me._cmdDefine_0.Enabled = True
		Me._cmdDefine_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._cmdDefine_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._cmdDefine_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cmdDefine_0.TabStop = True
		Me._cmdDefine_0.Name = "_cmdDefine_0"
		Me._cmdDefine_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me._cmdDefine_1.Text = "Define..."
		Me._cmdDefine_1.Size = New System.Drawing.Size(73, 25)
		Me._cmdDefine_1.Location = New System.Drawing.Point(256, 135)
		Me._cmdDefine_1.TabIndex = 29
		Me._cmdDefine_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cmdDefine_1.BackColor = System.Drawing.SystemColors.Control
		Me._cmdDefine_1.CausesValidation = True
		Me._cmdDefine_1.Enabled = True
		Me._cmdDefine_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._cmdDefine_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._cmdDefine_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cmdDefine_1.TabStop = True
		Me._cmdDefine_1.Name = "_cmdDefine_1"
		Me._cmbZXJoystick_1.Size = New System.Drawing.Size(189, 21)
		Me._cmbZXJoystick_1.Location = New System.Drawing.Point(140, 87)
		Me._cmbZXJoystick_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._cmbZXJoystick_1.TabIndex = 26
		Me._cmbZXJoystick_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cmbZXJoystick_1.BackColor = System.Drawing.SystemColors.Window
		Me._cmbZXJoystick_1.CausesValidation = True
		Me._cmbZXJoystick_1.Enabled = True
		Me._cmbZXJoystick_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._cmbZXJoystick_1.IntegralHeight = True
		Me._cmbZXJoystick_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._cmbZXJoystick_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cmbZXJoystick_1.Sorted = False
		Me._cmbZXJoystick_1.TabStop = True
		Me._cmbZXJoystick_1.Visible = True
		Me._cmbZXJoystick_1.Name = "_cmbZXJoystick_1"
		Me._cmbFire_1.Size = New System.Drawing.Size(189, 21)
		Me._cmbFire_1.Location = New System.Drawing.Point(140, 111)
		Me._cmbFire_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._cmbFire_1.TabIndex = 28
		Me._cmbFire_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cmbFire_1.BackColor = System.Drawing.SystemColors.Window
		Me._cmbFire_1.CausesValidation = True
		Me._cmbFire_1.Enabled = True
		Me._cmbFire_1.ForeColor = System.Drawing.SystemColors.WindowText
		Me._cmbFire_1.IntegralHeight = True
		Me._cmbFire_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._cmbFire_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cmbFire_1.Sorted = False
		Me._cmbFire_1.TabStop = True
		Me._cmbFire_1.Visible = True
		Me._cmbFire_1.Name = "_cmbFire_1"
		Me._cmbZXJoystick_0.Size = New System.Drawing.Size(189, 21)
		Me._cmbZXJoystick_0.Location = New System.Drawing.Point(140, 4)
		Me._cmbZXJoystick_0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._cmbZXJoystick_0.TabIndex = 21
		Me._cmbZXJoystick_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cmbZXJoystick_0.BackColor = System.Drawing.SystemColors.Window
		Me._cmbZXJoystick_0.CausesValidation = True
		Me._cmbZXJoystick_0.Enabled = True
		Me._cmbZXJoystick_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._cmbZXJoystick_0.IntegralHeight = True
		Me._cmbZXJoystick_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._cmbZXJoystick_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cmbZXJoystick_0.Sorted = False
		Me._cmbZXJoystick_0.TabStop = True
		Me._cmbZXJoystick_0.Visible = True
		Me._cmbZXJoystick_0.Name = "_cmbZXJoystick_0"
		Me._cmbFire_0.Size = New System.Drawing.Size(189, 21)
		Me._cmbFire_0.Location = New System.Drawing.Point(140, 28)
		Me._cmbFire_0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me._cmbFire_0.TabIndex = 23
		Me._cmbFire_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._cmbFire_0.BackColor = System.Drawing.SystemColors.Window
		Me._cmbFire_0.CausesValidation = True
		Me._cmbFire_0.Enabled = True
		Me._cmbFire_0.ForeColor = System.Drawing.SystemColors.WindowText
		Me._cmbFire_0.IntegralHeight = True
		Me._cmbFire_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._cmbFire_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._cmbFire_0.Sorted = False
		Me._cmbFire_0.TabStop = True
		Me._cmbFire_0.Visible = True
		Me._cmbFire_0.Name = "_cmbFire_0"
		Me._lblZXJoystick_1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me._lblZXJoystick_1.Text = "Joystick 2 Emulates:"
		Me._lblZXJoystick_1.Size = New System.Drawing.Size(137, 14)
		Me._lblZXJoystick_1.Location = New System.Drawing.Point(0, 91)
		Me._lblZXJoystick_1.TabIndex = 25
		Me._lblZXJoystick_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblZXJoystick_1.BackColor = System.Drawing.SystemColors.Control
		Me._lblZXJoystick_1.Enabled = True
		Me._lblZXJoystick_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblZXJoystick_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblZXJoystick_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblZXJoystick_1.UseMnemonic = True
		Me._lblZXJoystick_1.Visible = True
		Me._lblZXJoystick_1.AutoSize = False
		Me._lblZXJoystick_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblZXJoystick_1.Name = "_lblZXJoystick_1"
		Me._lblFire_1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me._lblFire_1.Text = "Joystick 2 Fire Button:"
		Me._lblFire_1.Size = New System.Drawing.Size(137, 14)
		Me._lblFire_1.Location = New System.Drawing.Point(0, 115)
		Me._lblFire_1.TabIndex = 27
		Me._lblFire_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblFire_1.BackColor = System.Drawing.SystemColors.Control
		Me._lblFire_1.Enabled = True
		Me._lblFire_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblFire_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblFire_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblFire_1.UseMnemonic = True
		Me._lblFire_1.Visible = True
		Me._lblFire_1.AutoSize = False
		Me._lblFire_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblFire_1.Name = "_lblFire_1"
		Me._lblZXJoystick_0.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me._lblZXJoystick_0.Text = "Joystick 1 Emulates:"
		Me._lblZXJoystick_0.Size = New System.Drawing.Size(137, 14)
		Me._lblZXJoystick_0.Location = New System.Drawing.Point(0, 8)
		Me._lblZXJoystick_0.TabIndex = 20
		Me._lblZXJoystick_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblZXJoystick_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblZXJoystick_0.Enabled = True
		Me._lblZXJoystick_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblZXJoystick_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblZXJoystick_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblZXJoystick_0.UseMnemonic = True
		Me._lblZXJoystick_0.Visible = True
		Me._lblZXJoystick_0.AutoSize = False
		Me._lblZXJoystick_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblZXJoystick_0.Name = "_lblZXJoystick_0"
		Me._lblFire_0.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me._lblFire_0.Text = "Joystick 1 Fire Button:"
		Me._lblFire_0.Size = New System.Drawing.Size(137, 14)
		Me._lblFire_0.Location = New System.Drawing.Point(0, 32)
		Me._lblFire_0.TabIndex = 22
		Me._lblFire_0.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._lblFire_0.BackColor = System.Drawing.SystemColors.Control
		Me._lblFire_0.Enabled = True
		Me._lblFire_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._lblFire_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._lblFire_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._lblFire_0.UseMnemonic = True
		Me._lblFire_0.Visible = True
		Me._lblFire_0.AutoSize = False
		Me._lblFire_0.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._lblFire_0.Name = "_lblFire_0"
		Me._picFrame_2.Size = New System.Drawing.Size(337, 185)
		Me._picFrame_2.Location = New System.Drawing.Point(8, 32)
		Me._picFrame_2.TabIndex = 18
		Me._picFrame_2.TabStop = False
		Me._picFrame_2.Visible = False
		Me._picFrame_2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._picFrame_2.Dock = System.Windows.Forms.DockStyle.None
		Me._picFrame_2.BackColor = System.Drawing.SystemColors.Control
		Me._picFrame_2.CausesValidation = True
		Me._picFrame_2.Enabled = True
		Me._picFrame_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._picFrame_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._picFrame_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picFrame_2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picFrame_2.Name = "_picFrame_2"
		Me.cboMouseType.Size = New System.Drawing.Size(189, 21)
		Me.cboMouseType.Location = New System.Drawing.Point(140, 4)
		Me.cboMouseType.Items.AddRange(New Object(){"Nothing", "Kempston Mouse Interface", "Amiga Mouse in Kempston Joyport"})
		Me.cboMouseType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboMouseType.TabIndex = 13
		Me.cboMouseType.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboMouseType.BackColor = System.Drawing.SystemColors.Window
		Me.cboMouseType.CausesValidation = True
		Me.cboMouseType.Enabled = True
		Me.cboMouseType.ForeColor = System.Drawing.SystemColors.WindowText
		Me.cboMouseType.IntegralHeight = True
		Me.cboMouseType.Cursor = System.Windows.Forms.Cursors.Default
		Me.cboMouseType.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cboMouseType.Sorted = False
		Me.cboMouseType.TabStop = True
		Me.cboMouseType.Visible = True
		Me.cboMouseType.Name = "cboMouseType"
		Me.Frame1.Text = "Respond to Mouse Buttons"
		Me.Frame1.Size = New System.Drawing.Size(321, 65)
		Me.Frame1.Location = New System.Drawing.Point(8, 32)
		Me.Frame1.TabIndex = 14
		Me.Frame1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Visible = True
		Me.Frame1.Padding = New System.Windows.Forms.Padding(0)
		Me.Frame1.Name = "Frame1"
		Me.optMouseGlobal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optMouseGlobal.Text = "&Always"
		Me.optMouseGlobal.Size = New System.Drawing.Size(145, 13)
		Me.optMouseGlobal.Location = New System.Drawing.Point(12, 40)
		Me.optMouseGlobal.TabIndex = 16
		Me.optMouseGlobal.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optMouseGlobal.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optMouseGlobal.BackColor = System.Drawing.SystemColors.Control
		Me.optMouseGlobal.CausesValidation = True
		Me.optMouseGlobal.Enabled = True
		Me.optMouseGlobal.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optMouseGlobal.Cursor = System.Windows.Forms.Cursors.Default
		Me.optMouseGlobal.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optMouseGlobal.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optMouseGlobal.TabStop = True
		Me.optMouseGlobal.Checked = False
		Me.optMouseGlobal.Visible = True
		Me.optMouseGlobal.Name = "optMouseGlobal"
		Me.optMouseVB.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optMouseVB.Text = "&Only when Windows pointer is over vbSpec window"
		Me.optMouseVB.Size = New System.Drawing.Size(277, 13)
		Me.optMouseVB.Location = New System.Drawing.Point(12, 20)
		Me.optMouseVB.TabIndex = 15
		Me.optMouseVB.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optMouseVB.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optMouseVB.BackColor = System.Drawing.SystemColors.Control
		Me.optMouseVB.CausesValidation = True
		Me.optMouseVB.Enabled = True
		Me.optMouseVB.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optMouseVB.Cursor = System.Windows.Forms.Cursors.Default
		Me.optMouseVB.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optMouseVB.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optMouseVB.TabStop = True
		Me.optMouseVB.Checked = False
		Me.optMouseVB.Visible = True
		Me.optMouseVB.Name = "optMouseVB"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label1.Text = "Windows &Mouse Emulates:"
		Me.Label1.Size = New System.Drawing.Size(137, 17)
		Me.Label1.Location = New System.Drawing.Point(0, 8)
		Me.Label1.TabIndex = 12
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me._picFrame_1.Size = New System.Drawing.Size(337, 185)
		Me._picFrame_1.Location = New System.Drawing.Point(8, 32)
		Me._picFrame_1.TabIndex = 17
		Me._picFrame_1.TabStop = False
		Me._picFrame_1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me._picFrame_1.Dock = System.Windows.Forms.DockStyle.None
		Me._picFrame_1.BackColor = System.Drawing.SystemColors.Control
		Me._picFrame_1.CausesValidation = True
		Me._picFrame_1.Enabled = True
		Me._picFrame_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._picFrame_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._picFrame_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._picFrame_1.Visible = True
		Me._picFrame_1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me._picFrame_1.Name = "_picFrame_1"
		Me.chkSEBasic.Text = "Use SE Basic ROM"
		Me.chkSEBasic.Size = New System.Drawing.Size(129, 17)
		Me.chkSEBasic.Location = New System.Drawing.Point(12, 32)
		Me.chkSEBasic.TabIndex = 5
		Me.chkSEBasic.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chkSEBasic.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkSEBasic.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkSEBasic.BackColor = System.Drawing.SystemColors.Control
		Me.chkSEBasic.CausesValidation = True
		Me.chkSEBasic.Enabled = True
		Me.chkSEBasic.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkSEBasic.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkSEBasic.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkSEBasic.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkSEBasic.TabStop = True
		Me.chkSEBasic.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkSEBasic.Visible = True
		Me.chkSEBasic.Name = "chkSEBasic"
		Me.fraStd.Text = "Emulation Speed"
		Me.fraStd.Size = New System.Drawing.Size(325, 105)
		Me.fraStd.Location = New System.Drawing.Point(8, 72)
		Me.fraStd.TabIndex = 7
		Me.fraStd.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.fraStd.BackColor = System.Drawing.SystemColors.Control
		Me.fraStd.Enabled = True
		Me.fraStd.ForeColor = System.Drawing.SystemColors.ControlText
		Me.fraStd.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.fraStd.Visible = True
		Me.fraStd.Padding = New System.Windows.Forms.Padding(0)
		Me.fraStd.Name = "fraStd"
		Me.optSpeed200.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeed200.Text = "&Double - Limit emulation to 200% real Spectrum speed"
		Me.optSpeed200.Size = New System.Drawing.Size(289, 17)
		Me.optSpeed200.Location = New System.Drawing.Point(12, 60)
		Me.optSpeed200.TabIndex = 10
		Me.optSpeed200.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optSpeed200.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeed200.BackColor = System.Drawing.SystemColors.Control
		Me.optSpeed200.CausesValidation = True
		Me.optSpeed200.Enabled = True
		Me.optSpeed200.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optSpeed200.Cursor = System.Windows.Forms.Cursors.Default
		Me.optSpeed200.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optSpeed200.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optSpeed200.TabStop = True
		Me.optSpeed200.Checked = False
		Me.optSpeed200.Visible = True
		Me.optSpeed200.Name = "optSpeed200"
		Me.optSpeed50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeed50.Text = "&Slow - Limit emulation to 50% of real Spectrum speed"
		Me.optSpeed50.Size = New System.Drawing.Size(293, 17)
		Me.optSpeed50.Location = New System.Drawing.Point(12, 20)
		Me.optSpeed50.TabIndex = 8
		Me.optSpeed50.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optSpeed50.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeed50.BackColor = System.Drawing.SystemColors.Control
		Me.optSpeed50.CausesValidation = True
		Me.optSpeed50.Enabled = True
		Me.optSpeed50.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optSpeed50.Cursor = System.Windows.Forms.Cursors.Default
		Me.optSpeed50.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optSpeed50.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optSpeed50.TabStop = True
		Me.optSpeed50.Checked = False
		Me.optSpeed50.Visible = True
		Me.optSpeed50.Name = "optSpeed50"
		Me.optSpeed100.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeed100.Text = "&Real - Limit emulation to 100% real Spectrum speed"
		Me.optSpeed100.Size = New System.Drawing.Size(289, 17)
		Me.optSpeed100.Location = New System.Drawing.Point(12, 40)
		Me.optSpeed100.TabIndex = 9
		Me.optSpeed100.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optSpeed100.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeed100.BackColor = System.Drawing.SystemColors.Control
		Me.optSpeed100.CausesValidation = True
		Me.optSpeed100.Enabled = True
		Me.optSpeed100.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optSpeed100.Cursor = System.Windows.Forms.Cursors.Default
		Me.optSpeed100.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optSpeed100.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optSpeed100.TabStop = True
		Me.optSpeed100.Checked = False
		Me.optSpeed100.Visible = True
		Me.optSpeed100.Name = "optSpeed100"
		Me.optSpeedFastest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeedFastest.Text = "&Fastest - Do not limit emulation speed"
		Me.optSpeedFastest.Size = New System.Drawing.Size(285, 17)
		Me.optSpeedFastest.Location = New System.Drawing.Point(12, 80)
		Me.optSpeedFastest.TabIndex = 11
		Me.optSpeedFastest.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optSpeedFastest.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optSpeedFastest.BackColor = System.Drawing.SystemColors.Control
		Me.optSpeedFastest.CausesValidation = True
		Me.optSpeedFastest.Enabled = True
		Me.optSpeedFastest.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optSpeedFastest.Cursor = System.Windows.Forms.Cursors.Default
		Me.optSpeedFastest.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optSpeedFastest.Appearance = System.Windows.Forms.Appearance.Normal
		Me.optSpeedFastest.TabStop = True
		Me.optSpeedFastest.Checked = False
		Me.optSpeedFastest.Visible = True
		Me.optSpeedFastest.Name = "optSpeedFastest"
		Me.chkSound.Text = "Enable sound output"
		Me.chkSound.Size = New System.Drawing.Size(141, 17)
		Me.chkSound.Location = New System.Drawing.Point(12, 52)
		Me.chkSound.TabIndex = 6
		Me.chkSound.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.chkSound.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.chkSound.FlatStyle = System.Windows.Forms.FlatStyle.Standard
		Me.chkSound.BackColor = System.Drawing.SystemColors.Control
		Me.chkSound.CausesValidation = True
		Me.chkSound.Enabled = True
		Me.chkSound.ForeColor = System.Drawing.SystemColors.ControlText
		Me.chkSound.Cursor = System.Windows.Forms.Cursors.Default
		Me.chkSound.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.chkSound.Appearance = System.Windows.Forms.Appearance.Normal
		Me.chkSound.TabStop = True
		Me.chkSound.CheckState = System.Windows.Forms.CheckState.Unchecked
		Me.chkSound.Visible = True
		Me.chkSound.Name = "chkSound"
		Me.cboModel.Size = New System.Drawing.Size(129, 21)
		Me.cboModel.Location = New System.Drawing.Point(92, 4)
		Me.cboModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.cboModel.TabIndex = 4
		Me.cboModel.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cboModel.BackColor = System.Drawing.SystemColors.Window
		Me.cboModel.CausesValidation = True
		Me.cboModel.Enabled = True
		Me.cboModel.ForeColor = System.Drawing.SystemColors.WindowText
		Me.cboModel.IntegralHeight = True
		Me.cboModel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cboModel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cboModel.Sorted = False
		Me.cboModel.TabStop = True
		Me.cboModel.Visible = True
		Me.cboModel.Name = "cboModel"
		Me.lblStatic.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblStatic.Text = "Emulated model:"
		Me.lblStatic.Size = New System.Drawing.Size(85, 17)
		Me.lblStatic.Location = New System.Drawing.Point(4, 8)
		Me.lblStatic.TabIndex = 3
		Me.lblStatic.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblStatic.BackColor = System.Drawing.SystemColors.Control
		Me.lblStatic.Enabled = True
		Me.lblStatic.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblStatic.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblStatic.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblStatic.UseMnemonic = True
		Me.lblStatic.Visible = True
		Me.lblStatic.AutoSize = False
		Me.lblStatic.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblStatic.Name = "lblStatic"
		Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdOK.Text = "OK"
		Me.AcceptButton = Me.cmdOK
		Me.cmdOK.Size = New System.Drawing.Size(73, 25)
		Me.cmdOK.Location = New System.Drawing.Point(196, 228)
		Me.cmdOK.TabIndex = 0
		Me.cmdOK.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
		Me.cmdOK.CausesValidation = True
		Me.cmdOK.Enabled = True
		Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdOK.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdOK.TabStop = True
		Me.cmdOK.Name = "cmdOK"
		Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.cmdCancel
		Me.cmdCancel.Text = "Cancel"
		Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
		Me.cmdCancel.Location = New System.Drawing.Point(276, 228)
		Me.cmdCancel.TabIndex = 1
		Me.cmdCancel.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
		Me.cmdCancel.CausesValidation = True
		Me.cmdCancel.Enabled = True
		Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdCancel.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdCancel.TabStop = True
		Me.cmdCancel.Name = "cmdCancel"
		'ts1.OcxState = CType(resources.GetObject("ts1.OcxState"), System.Windows.Forms.AxHost.State)
		'Me.ts1.Size = New System.Drawing.Size(345, 217)
		'Me.ts1.Location = New System.Drawing.Point(4, 4)
		'Me.ts1.TabIndex = 2
		'Me.ts1.Name = "ts1"
		Me.Controls.Add(_picFrame_3)
		Me.Controls.Add(_picFrame_2)
		Me.Controls.Add(_picFrame_1)
		Me.Controls.Add(cmdOK)
		Me.Controls.Add(cmdCancel)
		'Me.Controls.Add(ts1)
		Me._picFrame_3.Controls.Add(_cmdDefine_0)
		Me._picFrame_3.Controls.Add(_cmdDefine_1)
		Me._picFrame_3.Controls.Add(_cmbZXJoystick_1)
		Me._picFrame_3.Controls.Add(_cmbFire_1)
		Me._picFrame_3.Controls.Add(_cmbZXJoystick_0)
		Me._picFrame_3.Controls.Add(_cmbFire_0)
		Me._picFrame_3.Controls.Add(_lblZXJoystick_1)
		Me._picFrame_3.Controls.Add(_lblFire_1)
		Me._picFrame_3.Controls.Add(_lblZXJoystick_0)
		Me._picFrame_3.Controls.Add(_lblFire_0)
		Me._picFrame_2.Controls.Add(cboMouseType)
		Me._picFrame_2.Controls.Add(Frame1)
		Me._picFrame_2.Controls.Add(Label1)
		Me.Frame1.Controls.Add(optMouseGlobal)
		Me.Frame1.Controls.Add(optMouseVB)
		Me._picFrame_1.Controls.Add(chkSEBasic)
		Me._picFrame_1.Controls.Add(fraStd)
		Me._picFrame_1.Controls.Add(chkSound)
		Me._picFrame_1.Controls.Add(cboModel)
		Me._picFrame_1.Controls.Add(lblStatic)
		Me.fraStd.Controls.Add(optSpeed200)
		Me.fraStd.Controls.Add(optSpeed50)
		Me.fraStd.Controls.Add(optSpeed100)
		Me.fraStd.Controls.Add(optSpeedFastest)
		Me.cmbFire.SetIndex(_cmbFire_1, CType(1, Short))
		Me.cmbFire.SetIndex(_cmbFire_0, CType(0, Short))
		Me.cmbZXJoystick.SetIndex(_cmbZXJoystick_1, CType(1, Short))
		Me.cmbZXJoystick.SetIndex(_cmbZXJoystick_0, CType(0, Short))
		Me.cmdDefine.SetIndex(_cmdDefine_0, CType(0, Short))
		Me.cmdDefine.SetIndex(_cmdDefine_1, CType(1, Short))
		Me.lblFire.SetIndex(_lblFire_1, CType(1, Short))
		Me.lblFire.SetIndex(_lblFire_0, CType(0, Short))
		Me.lblZXJoystick.SetIndex(_lblZXJoystick_1, CType(1, Short))
		Me.lblZXJoystick.SetIndex(_lblZXJoystick_0, CType(0, Short))
		Me.picFrame.SetIndex(_picFrame_3, CType(3, Short))
		Me.picFrame.SetIndex(_picFrame_2, CType(2, Short))
		Me.picFrame.SetIndex(_picFrame_1, CType(1, Short))
		CType(Me.picFrame, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.lblZXJoystick, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.lblFire, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmdDefine, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbZXJoystick, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.cmbFire, System.ComponentModel.ISupportInitialize).EndInit()
		'CType(Me.ts1, System.ComponentModel.ISupportInitialize).EndInit()
		Me._picFrame_3.ResumeLayout(False)
		Me._picFrame_2.ResumeLayout(False)
		Me.Frame1.ResumeLayout(False)
		Me._picFrame_1.ResumeLayout(False)
		Me.fraStd.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class