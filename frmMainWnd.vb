Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports Microsoft.VisualBasic.PowerPacks
Friend Class frmMainWnd
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmMainWnd.frm within vbSpec.vbp
	'
	'   Main application window. Contains the Spectrum display output, processes
	'   Keypresses, and contains a timer object used for maintaining the emulator
	'   at 50 frames/sec on a fast enough machine, and a common dialog control for
	'   use by file open/save operations.
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2001  Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	'MM 16.04.2003
	Private lPopUp As Integer
	Private sNewCaption As String
	
	'That is a new caption property. If the Speccy is in full screen modus, the normal
	'caption of the form should not be set. The Speccy saves the data an restores it,
	'if the full screen modus is off
	Public WriteOnly Property NewCaption() As String
		Set(ByVal Value As String)
			If modSpectrum.bFullScreen Then
				sNewCaption = Value
			Else
				Me.Text = Value
			End If
		End Set
	End Property
	
	'This method sets the caption to an empty string and saves the original caption
	Public Sub FullScreenOn()
		sNewCaption = Me.Text
		Me.Text = vbNullString
		Me.mnuFileMain.Visible = False
		Me.mnuOptionsMain.Visible = False
		Me.mnuHelpMain.Visible = False
		Me.picStatus.Visible = False
		'Me.Line1(1).Visible = False
	End Sub
	
	'This method restores the original caption
	Public Sub FullScreenOff()
		bFullScreen = False
		Me.NewCaption = sNewCaption
		Me.mnuFileMain.Visible = True
		Me.mnuOptionsMain.Visible = True
		Me.mnuHelpMain.Visible = True
		Me.picStatus.Visible = True
		'Me.Line1(1).Visible = True
		Me.SetBounds(0, 0, 0, 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y)
	End Sub
	
	Public Sub FileCreateNewTap()
		Dim sName As String
		
		On Error Resume Next
		
		err.Clear()
		dlgCommonOpen.Title = "Create New Tap File"
		dlgCommonSave.Title = "Create New Tap File"
		dlgCommonOpen.DefaultExt = ".tap"
		dlgCommonSave.DefaultExt = ".tap"
		dlgCommonOpen.FileName = "*.tap"
		dlgCommonSave.FileName = "*.tap"
		'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		dlgCommonOpen.Filter = "Tape files (*.tap)|*.tap|All Files|*.*"
		dlgCommonSave.Filter = "Tape files (*.tap)|*.tap|All Files|*.*"
		'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		dlgCommonOpen.ShowReadOnly = False
		'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
		'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		dlgCommonSave.OverwritePrompt = True
		dlgCommonOpen.CheckPathExists = True
		dlgCommonSave.CheckPathExists = True
		'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
		'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
		'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'dlgCommon.CancelError = True
		dlgCommonSave.ShowDialog()
		dlgCommonOpen.FileName = dlgCommonSave.FileName
		'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		If Err.Number = DialogResult.Cancel Then
			Exit Sub
		End If
		
		sName = dlgCommonOpen.FileName
		
		If (sName <> "") Then
			If ghTAPFile > 0 Then FileClose(ghTAPFile)
			
			StopTape() ' // Stop the TZX tape player
			
			Kill(sName)
			
			ghTAPFile = FreeFile
			FileOpen(ghTAPFile, sName, OpenMode.Binary)
			
			gsTAPFileName = sName
			
			Me.NewCaption = My.Application.Info.ProductName & " - " & GetFilePart(sName)
			
			gMRU.AddMRUFile(sName)
			SetMRUMenu()
		End If
	End Sub
	
	Public Sub FileOpenDialog(Optional ByRef sName As String = "")
		On Error Resume Next
		
		If sName = "" Then
			err.Clear()
			dlgCommonOpen.Title = "Open Snapshot or ROM image"
			dlgCommonSave.Title = "Open Snapshot or ROM image"
			dlgCommonOpen.DefaultExt = ".sna"
			dlgCommonSave.DefaultExt = ".sna"
			dlgCommonOpen.FileName = ""
			dlgCommonSave.FileName = ""
			'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			dlgCommonOpen.Filter = "All Spectrum Files (*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr)|*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr|SNA snapshots (*.sna)|*.sna|Z80 snapshots (*.z80)|*.z80|ROM images (*.rom)|*.rom|Tape files (*.tap;*.tzx)|*.tap;*.tzx|Screen images (*.scr)|*.scr|All Files|*.*"
			dlgCommonSave.Filter = "All Spectrum Files (*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr)|*.sna;*.z80;*.tap;*.tzx;*.rom;*.scr|SNA snapshots (*.sna)|*.sna|Z80 snapshots (*.z80)|*.z80|ROM images (*.rom)|*.rom|Tape files (*.tap;*.tzx)|*.tap;*.tzx|Screen images (*.scr)|*.scr|All Files|*.*"
			'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.CheckFileExists which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
			dlgCommonOpen.CheckFileExists = True
			dlgCommonOpen.CheckPathExists = True
			dlgCommonSave.CheckPathExists = True
			'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
			'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
			'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
			'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			'dlgCommon.CancelError = True
			dlgCommonOpen.ShowDialog()
			dlgCommonSave.FileName = dlgCommonOpen.FileName
			'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
			If Err.Number = DialogResult.Cancel Then
				Exit Sub
			End If
			
			sName = dlgCommonOpen.FileName
		End If
		
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		If (sName <> "") And (Dir(sName) <> "") Then
			gMRU.AddMRUFile(sName)
			
			Select Case LCase(VB.Right(sName, 4))
				Case ".z80"
					LoadZ80Snap(sName)
				Case ".rom"
					Z80Reset()
					If glEmulatedModel <> 0 And glEmulatedModel <> 5 Then
						SetEmulatedModel(0, False)
					End If
					LoadROM(sName)
				Case ".sna"
					LoadSNASnap(sName)
				Case ".tap"
					OpenTAPFile(sName)
				Case ".tzx"
					OpenTZXFile(sName)
				Case ".scr"
					LoadScreenSCR(sName)
				Case Else
					' try opening it as a SNA file
					LoadSNASnap(sName)
			End Select
			SetMRUMenu()
		End If
	End Sub
	
	
	
	
	Private Sub FileSaveAsDialog()
		On Error Resume Next
		
		err.Clear()
		dlgCommonOpen.Title = "Save As"
		dlgCommonSave.Title = "Save As"
		dlgCommonOpen.DefaultExt = ".z80"
		dlgCommonSave.DefaultExt = ".z80"
		dlgCommonOpen.FileName = ""
		dlgCommonSave.FileName = ""
		'UPGRADE_WARNING: Filter has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		dlgCommonOpen.Filter = "Z80 snapshot (*.z80)|*.z80|SNA snapshot (*.sna)|*.sna|ROM image (*.rom)|*.rom|Screen Bitmap (*.bmp)|*.bmp|Screen Image (*.scr)|*.scr"
		dlgCommonSave.Filter = "Z80 snapshot (*.z80)|*.z80|SNA snapshot (*.sna)|*.sna|ROM image (*.rom)|*.rom|Screen Bitmap (*.bmp)|*.bmp|Screen Image (*.scr)|*.scr"
		'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		dlgCommonOpen.CheckPathExists = True
		dlgCommonSave.CheckPathExists = True
		'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonSave.OverwritePrompt which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		dlgCommonSave.OverwritePrompt = True
		'UPGRADE_ISSUE: Constant cdlOFNExplorer was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNExplorer
		'UPGRADE_ISSUE: Constant cdlOFNLongNames was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNLongNames
		'UPGRADE_WARNING: MSComDlg.CommonDialog property dlgCommon.Flags was upgraded to dlgCommonOpen.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'UPGRADE_WARNING: FileOpenConstants constant FileOpenConstants.cdlOFNHideReadOnly was upgraded to OpenFileDialog.ShowReadOnly which has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		'dlgCommonOpen.ShowReadOnly = False
		'UPGRADE_ISSUE: Constant cdlOFNNoReadOnlyReturn was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property dlgCommon.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		'dlgCommon.Flags = MSComDlg.FileOpenConstants.cdlOFNNoReadOnlyReturn
		'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		'dlgCommon.CancelError = True
		dlgCommonSave.ShowDialog()
		dlgCommonOpen.FileName = dlgCommonSave.FileName
		'UPGRADE_WARNING: The CommonDialog CancelError property is not supported in Visual Basic .NET. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="8B377936-3DF7-4745-AA26-DD00FA5B9BE1"'
		If Err.Number = DialogResult.Cancel Then
			Exit Sub
		End If
		
		If dlgCommonOpen.FileName <> "" Then
			gMRU.AddMRUFile((dlgCommonOpen.FileName))
			
			Select Case LCase(VB.Right(dlgCommonOpen.FileName, 4))
				Case ".z80"
					SaveZ80Snap((dlgCommonOpen.FileName))
				Case ".rom"
					SaveROM((dlgCommonOpen.FileName))
				Case ".sna"
					SaveSNASnap((dlgCommonOpen.FileName))
				Case ".bmp"
					SaveScreenBMP((dlgCommonOpen.FileName))
				Case ".scr"
					SaveScreenSCR((dlgCommonOpen.FileName))
				Case Else
					' save it as a Z80 file
					SaveZ80Snap((dlgCommonOpen.FileName))
			End Select
			SetMRUMenu()
		End If
		
	End Sub
	
	
	
	Private Function SetMRUMenu() As Object
		Dim l As Integer
		
		For l = 10 To 15
			mnuFile(l).Visible = False
		Next l
		
		For l = 1 To gMRU.GetMRUCount
			If Len(gMRU.GetMRUFile(l)) <= 40 Then
				mnuFile(10 + l).Text = "&" & CStr(l) & " " & gMRU.GetMRUFile(l)
			Else
				mnuFile(10 + l).Text = "&" & CStr(l) & " " & GetFilePart(gMRU.GetMRUFile(l))
			End If
			mnuFile(10 + l).Visible = True
		Next l
		
		If gMRU.GetMRUCount > 0 Then mnuFile(10).Visible = True
	End Function
	
	Private Sub frmMainWnd_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If (Shift And 4) Then Exit Sub
		doKey(True, KeyCode, Shift)
		'MM 16.04.2003
		If (KeyCode = 27) And (Shift = 1) And bFullScreen Then
			lPopUp = -1
			'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'Me.PopupMenu(mnuFullScreenMenu)
			'If lPopUp = 0 Then
			'	'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'	Me.PopupMenu(mnuFileMain)
			'End If
			'If lPopUp = 1 Then
			'	'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'	Me.PopupMenu(mnuOptionsMain)
			'End If
			'If lPopUp = 2 Then
			'	'UPGRADE_ISSUE: Form method frmMainWnd.PopupMenu was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
			'	Me.PopupMenu(mnuHelpMain)
			'End If
		End If
	End Sub
	
	'MM 16.04.2003
	Public Sub mnuFullScreenFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFullScreenFile.Click
		lPopUp = 0
	End Sub
	Public Sub mnuFullScreenOptions_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFullScreenOptions.Click
		lPopUp = 1
	End Sub
	Public Sub mnuFullScreenHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFullScreenHelp.Click
		lPopUp = 2
	End Sub
	Public Sub mnuFullNormalView_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFullNormalView.Click
		Me.FullScreenOff()
		modMain.SetDisplaySize(256, 192)
		Me.Resize_Renamed()
	End Sub
	
	Private Sub frmMainWnd_KeyUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		doKey(False, KeyCode, Shift)
	End Sub
	
	
	Private Sub frmMainWnd_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		Dim X, y As Integer
		Dim lCounter As Integer
		'MM JD
		Dim sKey As String
		
		gMRU = New MRUList
		SetMRUMenu()
		
		X = Val(GetSetting("Grok", "vbSpec", "MainWndX", "-1"))
		y = Val(GetSetting("Grok", "vbSpec", "MainWndY", "-1"))

		If X >= 0 And X <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width) - 16 Then
			Me.Left = X
		End If
		If y >= 0 And y <= (System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height) - 16 Then
			Me.Top = y
		End If

		'MM JD
		'Load standard joystick tables
		'Joystick 1
		'Kepmston
		PortValueToKeyStroke(KEMPSTON_PUP, KEMPSTON_UP, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_UP).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_UP).lPort = KEMPSTON_PUP
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_UP).lValue = KEMPSTON_UP
		PortValueToKeyStroke(KEMPSTON_PDOWN, KEMPSTON_DOWN, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_DOWN).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_DOWN).lPort = KEMPSTON_PDOWN
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_DOWN).lValue = KEMPSTON_DOWN
		PortValueToKeyStroke(KEMPSTON_PLEFT, KEMPSTON_LEFT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_LEFT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_LEFT).lPort = KEMPSTON_PLEFT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_LEFT).lValue = KEMPSTON_LEFT
		PortValueToKeyStroke(KEMPSTON_PRIGHT, KEMPSTON_RIGHT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_RIGHT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_RIGHT).lPort = KEMPSTON_PRIGHT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_RIGHT).lValue = KEMPSTON_RIGHT
		PortValueToKeyStroke(KEMPSTON_PFIRE, KEMPSTON_FIRE, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_BUTTON1).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_BUTTON1).lPort = KEMPSTON_PFIRE
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_BUTTON1).lValue = KEMPSTON_FIRE
		For lCounter = 2 To JDT_MAXBUTTONS
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_BUTTON_BASE + lCounter).sKey = vbNullString
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_BUTTON_BASE + lCounter).lPort = 0
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjKempston, JDT_BUTTON_BASE + lCounter).lValue = 0
		Next lCounter
		'Cursor
		PortValueToKeyStroke(CURSOR_PUP, CURSOR_UP, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_UP).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_UP).lPort = CURSOR_PUP
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_UP).lValue = CURSOR_UP
		PortValueToKeyStroke(CURSOR_PDOWN, CURSOR_DOWN, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_DOWN).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_DOWN).lPort = CURSOR_PDOWN
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_DOWN).lValue = CURSOR_DOWN
		PortValueToKeyStroke(CURSOR_PLEFT, CURSOR_LEFT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_LEFT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_LEFT).lPort = CURSOR_PLEFT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_LEFT).lValue = CURSOR_LEFT
		PortValueToKeyStroke(CURSOR_PRIGHT, CURSOR_RIGHT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_RIGHT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_RIGHT).lPort = CURSOR_PRIGHT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_RIGHT).lValue = CURSOR_RIGHT
		PortValueToKeyStroke(CURSOR_PFIRE, CURSOR_FIRE, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_BUTTON1).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_BUTTON1).lPort = CURSOR_PFIRE
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_BUTTON1).lValue = CURSOR_FIRE
		For lCounter = 2 To JDT_MAXBUTTONS
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_BUTTON_BASE + lCounter).sKey = vbNullString
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_BUTTON_BASE + lCounter).lPort = 0
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjCursor, JDT_BUTTON_BASE + lCounter).lValue = 0
		Next lCounter
		'Sinclair 1
		PortValueToKeyStroke(SINCLAIR1_PUP, SINCLAIR1_UP, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_UP).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_UP).lPort = SINCLAIR1_PUP
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_UP).lValue = SINCLAIR1_UP
		PortValueToKeyStroke(SINCLAIR1_PDOWN, SINCLAIR1_DOWN, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_DOWN).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_DOWN).lPort = SINCLAIR1_PDOWN
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_DOWN).lValue = SINCLAIR1_DOWN
		PortValueToKeyStroke(SINCLAIR1_PLEFT, SINCLAIR1_LEFT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_LEFT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_LEFT).lPort = SINCLAIR1_PLEFT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_LEFT).lValue = SINCLAIR1_LEFT
		PortValueToKeyStroke(SINCLAIR1_PRIGHT, SINCLAIR1_RIGHT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_RIGHT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_RIGHT).lPort = SINCLAIR1_PRIGHT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_RIGHT).lValue = SINCLAIR1_RIGHT
		PortValueToKeyStroke(SINCLAIR1_PFIRE, SINCLAIR1_FIRE, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_BUTTON1).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_BUTTON1).lPort = SINCLAIR1_PFIRE
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_BUTTON1).lValue = SINCLAIR1_FIRE
		For lCounter = 2 To JDT_MAXBUTTONS
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_BUTTON_BASE + lCounter).sKey = vbNullString
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_BUTTON_BASE + lCounter).lPort = 0
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair1, JDT_BUTTON_BASE + lCounter).lValue = 0
		Next lCounter
		'Sinclair 2
		PortValueToKeyStroke(SINCLAIR2_PUP, SINCLAIR2_UP, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_UP).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_UP).lPort = SINCLAIR2_PUP
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_UP).lValue = SINCLAIR2_UP
		PortValueToKeyStroke(SINCLAIR2_PDOWN, SINCLAIR2_DOWN, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_DOWN).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_DOWN).lPort = SINCLAIR2_PDOWN
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_DOWN).lValue = SINCLAIR2_DOWN
		PortValueToKeyStroke(SINCLAIR2_PLEFT, SINCLAIR2_LEFT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_LEFT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_LEFT).lPort = SINCLAIR2_PLEFT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_LEFT).lValue = SINCLAIR2_LEFT
		PortValueToKeyStroke(SINCLAIR2_PRIGHT, SINCLAIR2_RIGHT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_RIGHT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_RIGHT).lPort = SINCLAIR2_PRIGHT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_RIGHT).lValue = SINCLAIR2_RIGHT
		PortValueToKeyStroke(SINCLAIR2_PFIRE, SINCLAIR2_FIRE, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_BUTTON1).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_BUTTON1).lPort = SINCLAIR2_PFIRE
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_BUTTON1).lValue = SINCLAIR2_FIRE
		For lCounter = 2 To JDT_MAXBUTTONS
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_BUTTON_BASE + lCounter).sKey = vbNullString
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_BUTTON_BASE + lCounter).lPort = 0
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjSinclair2, JDT_BUTTON_BASE + lCounter).lValue = 0
		Next lCounter
		'Fuller box
		PortValueToKeyStroke(FULLER_PUP, FULLER_UP, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_UP).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_UP).lPort = FULLER_PUP
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_UP).lValue = FULLER_UP
		PortValueToKeyStroke(FULLER_PDOWN, FULLER_DOWN, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_DOWN).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_DOWN).lPort = FULLER_PDOWN
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_DOWN).lValue = FULLER_DOWN
		PortValueToKeyStroke(FULLER_PLEFT, FULLER_LEFT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_LEFT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_LEFT).lPort = FULLER_PLEFT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_LEFT).lValue = FULLER_LEFT
		PortValueToKeyStroke(FULLER_PRIGHT, FULLER_RIGHT, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_RIGHT).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_RIGHT).lPort = FULLER_PRIGHT
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_RIGHT).lValue = FULLER_RIGHT
		PortValueToKeyStroke(FULLER_PFIRE, FULLER_FIRE, sKey)
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_BUTTON1).sKey = sKey
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_BUTTON1).lPort = FULLER_PFIRE
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_BUTTON1).lValue = FULLER_FIRE
		For lCounter = 2 To JDT_MAXBUTTONS
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_BUTTON_BASE + lCounter).sKey = vbNullString
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_BUTTON_BASE + lCounter).lPort = 0
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjFullerBox, JDT_BUTTON_BASE + lCounter).lValue = 0
		Next lCounter
		'User defined
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_UP).sKey = vbNullString
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_UP).lPort = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_UP).lValue = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_DOWN).sKey = vbNullString
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_DOWN).lPort = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_DOWN).lValue = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_LEFT).sKey = vbNullString
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_LEFT).lPort = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_LEFT).lValue = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_RIGHT).sKey = vbNullString
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_RIGHT).lPort = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_RIGHT).lValue = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON1).sKey = vbNullString
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON1).lPort = 0
		aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON1).lValue = 0
		For lCounter = 2 To JDT_MAXBUTTONS
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON_BASE + lCounter).sKey = vbNullString
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON_BASE + lCounter).lPort = 0
			aJoystikDefinitionTable(JDT_JOYSTICK1, modSpectrum.ZXJOYSTICKS.zxjUserDefined, JDT_BUTTON_BASE + lCounter).lValue = 0
		Next lCounter

		'MM 12.03.2003 - Joystick support code
		'=================================================================================
		'API call result
		Dim lResult As Integer
		Dim uJoyCap As JOYCAPS

		'Init
		modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjInvalid
		modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjInvalid
		modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid
		modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid
		modSpectrum.bJoystick1Valid = False
		modSpectrum.bJoystick2Valid = False

		'Errorhandler
		On Error GoTo LOAD_ERROR

		'Try to initiate PC-joystick nr. 1
		lResult = modSpectrum.joyGetDevCaps(modSpectrum.JOYSTICKID1, uJoyCap, Len(uJoyCap))
		'Init succeded
		If lResult = modSpectrum.JOYERR_OK Then
			'PC-joystick nr. 1 can be set up
			modSpectrum.bJoystick1Valid = True
			'Init for Kempston
			modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjKempston
			modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbButton1
			'Number of buttons
			modSpectrum.lPCJoystick1Buttons = uJoyCap.wNumButtons
			'Init for PC-joystick nr.1 failed
		Else
			'There is no posibility to set up the PC-joystick nr. 1
			modSpectrum.bJoystick1Valid = False
			modSpectrum.lPCJoystick1Is = modSpectrum.ZXJOYSTICKS.zxjInvalid
			modSpectrum.lPCJoystick1Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid
			'Number of buttons
			modSpectrum.lPCJoystick1Buttons = -1
		End If

		'Try to initiate PC-joystick nr. 2
		lResult = modSpectrum.joyGetDevCaps(modSpectrum.JOYSTICKID2, uJoyCap, Len(uJoyCap))
		'Init succeded
		If lResult = modSpectrum.JOYERR_OK Then
			'PC-joystick nr. 2 can be set up
			modSpectrum.bJoystick2Valid = True
			'Init for Kempston
			modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjKempston
			modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbButton1
			'Number of buttons
			modSpectrum.lPCJoystick2Buttons = uJoyCap.wNumButtons
			'Init for PC-joystick nr.2 failed
		Else
			'There is no posibility to set up the PC-joystick nr. 2
			modSpectrum.bJoystick2Valid = False
			modSpectrum.lPCJoystick2Is = modSpectrum.ZXJOYSTICKS.zxjInvalid
			modSpectrum.lPCJoystick2Fire = modSpectrum.PCJOYBUTTONS.pcjbInvalid
			'Number of buttons
			modSpectrum.lPCJoystick2Buttons = -1
		End If

		'All right
		Exit Sub
LOAD_ERROR:

		'MM 03.02.2003 -- END
		'=================================================================================

	End Sub
	'UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
	'UPGRADE_ISSUE: Form event Form.OLEDragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub Form_OLEDragDrop(ByRef Data As Object, ByRef Effect As Integer, ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef y As Single)
		'UPGRADE_ISSUE: Constant vbCFFiles was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: DataObject method Data.GetFormat was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'If Data.GetFormat(vbCFFiles) Then
		'	'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'	If (LCase(VB.Right(Data.Files(1), 4)) = ".sna") Or (LCase(VB.Right(Data.Files(1), 4)) = ".z80") Or (LCase(VB.Right(Data.Files(1), 4)) = ".tap") Or (LCase(VB.Right(Data.Files(1), 4)) = ".rom") Or (LCase(VB.Right(Data.Files(1), 4)) = ".scr") Then
		'		If (Effect And System.Windows.Forms.DragDropEffects.Copy) Then
		'			'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'			Select Case LCase(VB.Right(Data.Files(1), 4))
		'				Case ".z80"
		'					System.Windows.Forms.Application.DoEvents()
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					If Dir(Data.Files(1)) <> "" Then LoadZ80Snap(Data.Files(1))
		'				Case ".rom"
		'					System.Windows.Forms.Application.DoEvents()
		'					Z80Reset()
		'					If glEmulatedModel <> 0 And glEmulatedModel <> 5 Then
		'						SetEmulatedModel(0, False)
		'					End If
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					If Dir(Data.Files(1)) <> "" Then LoadROM(Data.Files(1))
		'				Case ".sna"
		'					System.Windows.Forms.Application.DoEvents()
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					If Dir(Data.Files(1)) <> "" Then LoadSNASnap(Data.Files(1))
		'				Case ".tap"
		'					System.Windows.Forms.Application.DoEvents()
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					If Dir(Data.Files(1)) <> "" Then OpenTAPFile(Data.Files(1))
		'				Case ".scr"
		'					System.Windows.Forms.Application.DoEvents()
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		'					'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'					If Dir(Data.Files(1)) <> "" Then LoadScreenSCR(Data.Files(1))
		'			End Select
		'		End If
		'	End If
		'End If
	End Sub

	'UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
	'UPGRADE_ISSUE: Form event Form.OLEDragOver was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub Form_OLEDragOver(ByRef Data As Object, ByRef Effect As Integer, ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef y As Single, ByRef State As Short)
		'UPGRADE_ISSUE: Constant vbCFFiles was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: DataObject method Data.GetFormat was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'If Data.GetFormat(vbCFFiles) Then
		'	'UPGRADE_ISSUE: DataObject property Data.Files was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
		'	If (LCase(VB.Right(Data.Files(1), 4)) = ".sna") Or (LCase(VB.Right(Data.Files(1), 4)) = ".z80") Or (LCase(VB.Right(Data.Files(1), 4)) = ".tap") Or (LCase(VB.Right(Data.Files(1), 4)) = ".rom") Or (LCase(VB.Right(Data.Files(1), 4)) = ".scr") Then
		'		If (Effect And System.Windows.Forms.DragDropEffects.Copy) Then
		'			Effect = System.Windows.Forms.DragDropEffects.Copy
		'		Else
		'			Effect = System.Windows.Forms.DragDropEffects.None
		'		End If
		'	Else
		'		Effect = System.Windows.Forms.DragDropEffects.None
		'	End If
		'End If
	End Sub

	Private Sub frmMainWnd_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If Not (frmZXPrinter.Visible) Then
			SaveSetting("Grok", "vbSpec", "EmulateZXPrinter", "0")
		End If

		If Not (frmTapePlayer.Visible) Then
			SaveSetting("Grok", "vbSpec", "TapeControlsVisible", "0")
		End If
		eventArgs.Cancel = Cancel
	End Sub

	'MM 16.04.2003
	'UPGRADE_WARNING: Event frmMainWnd.Resize may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub frmMainWnd_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
		Resize_Renamed()
	End Sub
	'UPGRADE_WARNING: Form event frmMainWnd.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmMainWnd_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		Resize_Renamed()
	End Sub

	Private Sub frmMainWnd_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		If gbSoundEnabled Then CloseWaveOut()

		'UPGRADE_NOTE: Object gMRU may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		gMRU = Nothing

		SaveSetting("Grok", "vbSpec", "MainWndX", CStr(Me.Left))
		SaveSetting("Grok", "vbSpec", "MainWndY", CStr(Me.Top))

		timeEndPeriod(1)
		End
	End Sub
	
	
	Public Sub mnuFile_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuFile.Click
		Dim Index As Short = mnuFile.GetIndex(eventSender)
		Dim lWndProc As Integer
		mnuFileMain.Visible = Not bFullScreen
		Select Case Index
			Case 1 ' Open
				If gbSoundEnabled Then waveOutReset(glphWaveOut)
				FileOpenDialog()
			Case 2 ' Save As
				If gbSoundEnabled Then waveOutReset(glphWaveOut)
				FileSaveAsDialog()
			Case 4 ' Create blank TAP file
				If gbSoundEnabled Then waveOutReset(glphWaveOut)
				FileCreateNewTap()
			Case 6 ' Load Binary
				frmLoadBinary.ShowDialog()
			Case 7 ' Save Binary
				frmSaveBinary.ShowDialog()
			Case 9 ' Reset
				Z80Reset()
			Case Else
				If mnuFile(Index).Text = "E&xit" Then
					Me.Close()
				Else
					' // MRU File
					FileOpenDialog(gMRU.GetMRUFile(Index - 10))
				End If
		End Select
	End Sub
	
	Public Sub mnuHelp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuHelp.Click
		Dim Index As Short = mnuHelp.GetIndex(eventSender)
		mnuHelpMain.Visible = Not bFullScreen
		Select Case Index
			Case 1 ' // Show spectrum keyboard layout
				frmKeyboard.Show()
			Case 3 ' // About... Dialog
				frmAbout.ShowDialog()
		End Select
	End Sub
	
	Public Sub mnuOptions_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles mnuOptions.Click
		Dim Index As Short = mnuOptions.GetIndex(eventSender)
		mnuOptionsMain.Visible = Not bFullScreen
		Select Case Index
			Case 1
				frmOptions.ShowDialog()
			Case 2
				frmDisplayOpt.ShowDialog()
				frmMainWnd_Resize(Me, New System.EventArgs())
			Case 4
				If mnuOptions(Index).Checked Then
					frmTapePlayer.Hide()
					mnuOptions(Index).Checked = False
				Else
					' // GS: Fix for tapeplayer window always-on-top
					VB6.ShowForm(frmTapePlayer, 0, Me)
					mnuOptions(Index).Checked = True
				End If
				SaveSetting("Grok", "vbSpec", "TapeControlsVisible", IIf(mnuOptions(Index).Checked, "-1", "0"))
			Case 5
				If mnuOptions(Index).Checked Then
					frmZXPrinter.Hide()
					mnuOptions(Index).Checked = False
				Else
					VB6.ShowForm(frmZXPrinter, 0, Me)
					mnuOptions(Index).Checked = True
				End If
				SaveSetting("Grok", "vbSpec", "EmulateZXPrinter", IIf(mnuOptions(Index).Checked, "-1", "0"))
				'MM 03.02.2003 - BEGIN
			Case 7
				mnuOptions(Index).Checked = Not mnuOptions(Index).Checked
			Case 8
				frmPoke.ShowDialog()
				'MM 03.02.2003 - END
		End Select
	End Sub
	
	Private Sub picDisplay_MouseDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picDisplay.MouseDown
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		glMouseBtn = glMouseBtn Or Button
	End Sub
	
	Private Sub picDisplay_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picDisplay.MouseUp
		Dim Button As Short = eventArgs.Button \ &H100000
		Dim Shift As Short = System.Windows.Forms.Control.ModifierKeys \ &H10000
		Dim X As Single = eventArgs.X
		Dim y As Single = eventArgs.Y
		glMouseBtn = glMouseBtn Xor Button
	End Sub
	
	
	'UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
	'UPGRADE_ISSUE: PictureBox event picDisplay.OLEDragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub picDisplay_OLEDragDrop(ByRef Data As Object, ByRef Effect As Integer, ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef y As Single)
		'UPGRADE_ISSUE: Form event Form.OLEDragDrop was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		Form_OLEDragDrop(Data, Effect, Button, Shift, X, y)
	End Sub
	
	'UPGRADE_ISSUE: VBRUN.DataObject type was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
	'UPGRADE_ISSUE: PictureBox event picDisplay.OLEDragOver was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
	Private Sub picDisplay_OLEDragOver(ByRef Data As Object, ByRef Effect As Integer, ByRef Button As Short, ByRef Shift As Short, ByRef X As Single, ByRef y As Single, ByRef State As Short)
		'UPGRADE_ISSUE: Form event Form.OLEDragOver was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="ABD9AF39-7E24-4AFF-AD8D-3675C1AA3054"'
		Form_OLEDragOver(Data, Effect, Button, Shift, X, y, State)
	End Sub
	
	'Resize operations
	'UPGRADE_NOTE: Resize was upgraded to Resize_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Sub Resize_Renamed()
		'Variables
		Dim lDisplayWidth, lDisplayHeight As Integer
		Dim lNewLeft, lNewTop As Integer
		'Get display width and height
		lDisplayWidth = Me.ClientRectangle.Width
		lDisplayHeight = Me.ClientRectangle.Height - picStatus.Height
		'Center the paper
		lNewLeft = CInt((lDisplayWidth - picDisplay.Width) / 2)
		lNewTop = CInt((lDisplayHeight - picDisplay.Height) / 2)
		'Preserve minimum size
		If lNewLeft < 12 Then
			If glDisplayXMultiplier = 1 Then
				Me.Width = 288
				Exit Sub
			End If
			If glDisplayXMultiplier = 2 Then
				Me.Width = 544
				Exit Sub
			End If
			If glDisplayXMultiplier = 3 Then
				Me.Width = 800
				Exit Sub
			End If
		End If
		If lNewTop < 14 Then
			If glDisplayYMultiplier = 1 Then
				Me.Height = 288
				Exit Sub
			End If
			If glDisplayYMultiplier = 2 Then
				Me.Height = 480
				Exit Sub
			End If
			If glDisplayYMultiplier = 3 Then
				Me.Height = 672
				Exit Sub
			End If
		End If
		'Set size
		picDisplay.Top = lNewTop
		picDisplay.Left = lNewLeft
	End Sub
End Class