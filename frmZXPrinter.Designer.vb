<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmZXPrinter
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents cmdClear As System.Windows.Forms.Button
	Public WithEvents optAlphacom As System.Windows.Forms.RadioButton
	Public WithEvents optZX As System.Windows.Forms.RadioButton
	Public WithEvents tmrFormFeed As System.Windows.Forms.Timer
	Public WithEvents imgAlpha As System.Windows.Forms.PictureBox
	Public WithEvents imgSinclair As System.Windows.Forms.PictureBox
	Public WithEvents picLogo As System.Windows.Forms.Panel
	Public WithEvents cmdFF As System.Windows.Forms.Button
	Public WithEvents cmdSave As System.Windows.Forms.Button
	Public WithEvents vs As System.Windows.Forms.VScrollBar
	Public dlgSaveOpen As System.Windows.Forms.OpenFileDialog
	Public dlgSaveSave As System.Windows.Forms.SaveFileDialog
	Public WithEvents picView As System.Windows.Forms.Panel
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmZXPrinter))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.cmdClear = New System.Windows.Forms.Button
		Me.optAlphacom = New System.Windows.Forms.RadioButton
		Me.optZX = New System.Windows.Forms.RadioButton
		Me.tmrFormFeed = New System.Windows.Forms.Timer(components)
		Me.picLogo = New System.Windows.Forms.Panel
		Me.imgAlpha = New System.Windows.Forms.PictureBox
		Me.imgSinclair = New System.Windows.Forms.PictureBox
		Me.cmdFF = New System.Windows.Forms.Button
		Me.cmdSave = New System.Windows.Forms.Button
		Me.vs = New System.Windows.Forms.VScrollBar
		Me.picView = New System.Windows.Forms.Panel
		Me.dlgSaveOpen = New System.Windows.Forms.OpenFileDialog
		Me.dlgSaveSave = New System.Windows.Forms.SaveFileDialog
		Me.picLogo.SuspendLayout()
		Me.picView.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
		Me.Text = "ZX Printer"
		Me.ClientSize = New System.Drawing.Size(273, 270)
		Me.Location = New System.Drawing.Point(4, 20)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmZXPrinter"
		Me.cmdClear.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdClear.Size = New System.Drawing.Size(25, 23)
		Me.cmdClear.Location = New System.Drawing.Point(60, 4)
		Me.cmdClear.Image = CType(resources.GetObject("cmdClear.Image"), System.Drawing.Image)
		Me.cmdClear.TabIndex = 7
		Me.ToolTip1.SetToolTip(Me.cmdClear, "Clear Output")
		Me.cmdClear.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdClear.BackColor = System.Drawing.SystemColors.Control
		Me.cmdClear.CausesValidation = True
		Me.cmdClear.Enabled = True
		Me.cmdClear.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdClear.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdClear.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdClear.TabStop = True
		Me.cmdClear.Name = "cmdClear"
		Me.optAlphacom.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.optAlphacom.Size = New System.Drawing.Size(81, 20)
		Me.optAlphacom.Location = New System.Drawing.Point(188, 4)
		Me.optAlphacom.Image = CType(resources.GetObject("optAlphacom.Image"), System.Drawing.Image)
		Me.optAlphacom.Appearance = System.Windows.Forms.Appearance.Button
		Me.optAlphacom.TabIndex = 6
		Me.optAlphacom.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optAlphacom.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optAlphacom.BackColor = System.Drawing.SystemColors.Control
		Me.optAlphacom.CausesValidation = True
		Me.optAlphacom.Enabled = True
		Me.optAlphacom.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optAlphacom.Cursor = System.Windows.Forms.Cursors.Default
		Me.optAlphacom.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optAlphacom.TabStop = True
		Me.optAlphacom.Checked = False
		Me.optAlphacom.Visible = True
		Me.optAlphacom.Name = "optAlphacom"
		Me.optZX.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.optZX.Size = New System.Drawing.Size(81, 20)
		Me.optZX.Location = New System.Drawing.Point(100, 4)
		Me.optZX.Image = CType(resources.GetObject("optZX.Image"), System.Drawing.Image)
		Me.optZX.Appearance = System.Windows.Forms.Appearance.Button
		Me.optZX.TabIndex = 5
		Me.ToolTip1.SetToolTip(Me.optZX, "ZX Printer")
		Me.optZX.Checked = True
		Me.optZX.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.optZX.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.optZX.BackColor = System.Drawing.SystemColors.Control
		Me.optZX.CausesValidation = True
		Me.optZX.Enabled = True
		Me.optZX.ForeColor = System.Drawing.SystemColors.ControlText
		Me.optZX.Cursor = System.Windows.Forms.Cursors.Default
		Me.optZX.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.optZX.TabStop = True
		Me.optZX.Visible = True
		Me.optZX.Name = "optZX"
		Me.tmrFormFeed.Enabled = False
		Me.tmrFormFeed.Interval = 2000
		Me.picLogo.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.picLogo.BackColor = System.Drawing.Color.Black
		Me.picLogo.Size = New System.Drawing.Size(273, 38)
		Me.picLogo.Location = New System.Drawing.Point(0, 232)
		Me.picLogo.TabIndex = 4
		Me.picLogo.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.picLogo.CausesValidation = True
		Me.picLogo.Enabled = True
		Me.picLogo.ForeColor = System.Drawing.SystemColors.ControlText
		Me.picLogo.Cursor = System.Windows.Forms.Cursors.Default
		Me.picLogo.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picLogo.TabStop = True
		Me.picLogo.Visible = True
		Me.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picLogo.Name = "picLogo"
		Me.imgAlpha.Size = New System.Drawing.Size(206, 26)
		Me.imgAlpha.Location = New System.Drawing.Point(4, 8)
		Me.imgAlpha.Image = CType(resources.GetObject("imgAlpha.Image"), System.Drawing.Image)
		Me.imgAlpha.Visible = False
		Me.imgAlpha.Enabled = True
		Me.imgAlpha.Cursor = System.Windows.Forms.Cursors.Default
		Me.imgAlpha.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.imgAlpha.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.imgAlpha.Name = "imgAlpha"
		Me.imgSinclair.Size = New System.Drawing.Size(206, 26)
		Me.imgSinclair.Location = New System.Drawing.Point(12, 4)
		Me.imgSinclair.Image = CType(resources.GetObject("imgSinclair.Image"), System.Drawing.Image)
		Me.imgSinclair.Enabled = True
		Me.imgSinclair.Cursor = System.Windows.Forms.Cursors.Default
		Me.imgSinclair.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal
		Me.imgSinclair.Visible = True
		Me.imgSinclair.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.imgSinclair.Name = "imgSinclair"
		Me.cmdFF.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdFF.Size = New System.Drawing.Size(25, 23)
		Me.cmdFF.Location = New System.Drawing.Point(32, 4)
		Me.cmdFF.Image = CType(resources.GetObject("cmdFF.Image"), System.Drawing.Image)
		Me.cmdFF.TabIndex = 3
		Me.ToolTip1.SetToolTip(Me.cmdFF, "Form Feed")
		Me.cmdFF.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdFF.BackColor = System.Drawing.SystemColors.Control
		Me.cmdFF.CausesValidation = True
		Me.cmdFF.Enabled = True
		Me.cmdFF.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdFF.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdFF.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdFF.TabStop = True
		Me.cmdFF.Name = "cmdFF"
		Me.cmdSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		Me.cmdSave.Size = New System.Drawing.Size(25, 23)
		Me.cmdSave.Location = New System.Drawing.Point(4, 4)
		Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
		Me.cmdSave.TabIndex = 2
		Me.ToolTip1.SetToolTip(Me.cmdSave, "Save Output")
		Me.cmdSave.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdSave.BackColor = System.Drawing.SystemColors.Control
		Me.cmdSave.CausesValidation = True
		Me.cmdSave.Enabled = True
		Me.cmdSave.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdSave.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdSave.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdSave.TabStop = True
		Me.cmdSave.Name = "cmdSave"
		Me.vs.Size = New System.Drawing.Size(17, 177)
		Me.vs.LargeChange = 8
		Me.vs.Location = New System.Drawing.Point(256, 32)
		Me.vs.SmallChange = 8
		Me.vs.TabIndex = 1
		Me.vs.TabStop = False
		Me.vs.CausesValidation = True
		Me.vs.Enabled = True
		Me.vs.Maximum = 32774
		Me.vs.Minimum = 0
		Me.vs.Cursor = System.Windows.Forms.Cursors.Default
		Me.vs.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.vs.Value = 0
		Me.vs.Visible = True
		Me.vs.Name = "vs"
		Me.picView.BackColor = System.Drawing.Color.FromARGB(192, 192, 192)
		Me.picView.ForeColor = System.Drawing.Color.Black
		Me.picView.Size = New System.Drawing.Size(256, 177)
		Me.picView.Location = New System.Drawing.Point(0, 32)
		Me.picView.TabIndex = 0
		Me.picView.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.picView.Dock = System.Windows.Forms.DockStyle.None
		Me.picView.CausesValidation = True
		Me.picView.Enabled = True
		Me.picView.Cursor = System.Windows.Forms.Cursors.Default
		Me.picView.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.picView.TabStop = True
		Me.picView.Visible = True
		Me.picView.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.picView.Name = "picView"
		Me.dlgSaveOpen.DefaultExt = "bmp"
		Me.dlgSaveSave.DefaultExt = "bmp"
		Me.dlgSaveOpen.Title = "Save Output"
		Me.dlgSaveSave.Title = "Save Output"
		Me.dlgSaveOpen.FileName = "untitled"
		Me.dlgSaveSave.FileName = "untitled"
		Me.dlgSaveOpen.Filter = "Windows Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*"
		Me.dlgSaveSave.Filter = "Windows Bitmap (*.bmp)|*.bmp|All Files (*.*)|*.*"
		Me.Controls.Add(cmdClear)
		Me.Controls.Add(optAlphacom)
		Me.Controls.Add(optZX)
		Me.Controls.Add(picLogo)
		Me.Controls.Add(cmdFF)
		Me.Controls.Add(cmdSave)
		Me.Controls.Add(vs)
		Me.Controls.Add(picView)
		Me.picLogo.Controls.Add(imgAlpha)
		Me.picLogo.Controls.Add(imgSinclair)
		Me.picLogo.ResumeLayout(False)
		Me.picView.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class