Option Strict Off
Option Explicit On
Friend Class frmDefButton
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmDefButton.frm within vbSpec.vbp
	'
	'   "Options->General settings->Joystick->Define" dialog for vbSpec
	'
	'   Author: Miklos Muhi <vbspec@muhi.org>
	'   http://www.muhi.org/vbspec/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	'Variables
	Private bIsFirstActivate As Boolean
	'Button
	Private lButton As Integer
	'If the Form was Closed with OK
	Private bOK As Boolean
	'If there was any change
	Private bEdit As Boolean
	'Button definitions
	Private sButton As String
	Private lButtonPort As Integer
	Private lButtonValue As Integer
	'Keys, ports and  values
	Private sKey As String
	Private lPort, lValue As Integer
	'Edit flags
	Private bKey, bPortValue As Boolean
	
	'This property provides the number of the button wich you set up
	Friend WriteOnly Property Button() As Integer
		Set(ByVal Value As Integer)
			lButton = Value
		End Set
	End Property
	'Provides, if the form was closed with OK
	Friend ReadOnly Property OK() As Boolean
		Get
			OK = bOK
		End Get
	End Property
	'Provider the button data
	Friend ReadOnly Property ButtonChar() As String
		Get
			ButtonChar = sButton
		End Get
	End Property
	Friend ReadOnly Property ButtonPort() As Integer
		Get
			ButtonPort = lButtonPort
		End Get
	End Property
	Friend ReadOnly Property ButtonValue() As Integer
		Get
			ButtonValue = lButtonValue
		End Get
	End Property
	
	'The form will be loaded
	Private Sub frmDefButton_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		'Setup the controls
		lblButton.Text = vbNullString
		txtFire.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtFire.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtFire.Maxlength = 3
		txtFire.ReadOnly = True
		txtPortFire.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtPortFire.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtPortFire.Maxlength = 5
		txtValueFire.Text = vbNullString
		'UPGRADE_WARNING: TextBox property txtValueFire.MaxLength has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		txtValueFire.Maxlength = 3
		'Default values
		lButton = 1
		bOK = False
		bEdit = False
		sButton = vbNullString
		lButtonPort = 0
		lButtonValue = 0
		bKey = False
		bPortValue = False
		'Now it's time for the first activation
		bIsFirstActivate = True
	End Sub
	
	'The form will be activated
	'UPGRADE_WARNING: Form event frmDefButton.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmDefButton_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		'Only by the first activation
		If bIsFirstActivate Then
			'If there is no valid joystick
			If Not (modSpectrum.bJoystick1Valid Or modSpectrum.bJoystick2Valid) Then
				'Close window
				Me.Close()
				Exit Sub
			End If
			'Set label caption
			lblButton.Text = "Button " & Trim(CStr(lButton)) & " translated into:"
			'The first activation ends here
			bIsFirstActivate = False
		End If
	End Sub
	
	'The form will be unloaded
	Private Sub frmDefButton_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		'Variables
		Dim vbmrValue As MsgBoxResult
		'If there are unsaved data
		If bEdit Then
			'Ask the user
			vbmrValue = MsgBox("Do you want to save your changes?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1)
			'Analyse
			Select Case vbmrValue
				'Save changes and exit
				Case MsgBoxResult.Yes
					'Save changes
					OKButton_Click(OKButton, New System.EventArgs())
					'Do not save changes
				Case MsgBoxResult.No
					'No saved data
					bOK = False
					'Cancel
				Case MsgBoxResult.Cancel
					'UPGRADE_ISSUE: Event parameter Cancel was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FB723E3C-1C06-4D2B-B083-E6CD0D334DA8"'
					'Cancel = 1
			End Select
		End If
	End Sub
	
	'Close and save
	Private Sub OKButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles OKButton.Click
		'Save changes
		sButton = txtFire.Text
		If IsNumeric(txtPortFire.Text) Then
			lButtonPort = CInt(txtPortFire.Text)
		Else
			lButtonPort = 0
		End If
		If IsNumeric(txtValueFire.Text) Then
			lButtonValue = CInt(txtValueFire.Text)
		Else
			lButtonValue = 0
		End If
		'Changes over
		bEdit = False
		'Close window
		bOK = True
		Me.Close()
	End Sub
	
	'Close without save
	Private Sub CancelButton_Renamed_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles CancelButton_Renamed.Click
		bOK = False
		Me.Close()
	End Sub
	
	'There was a change
	Private Sub DefaultChange()
		bEdit = True
	End Sub
	
	'Default focus hanlder
	Private Sub DefaultFocusHanlder(ByRef oTextBox As System.Windows.Forms.TextBox)
		If oTextBox.Text <> vbNullString Then
			oTextBox.SelectionStart = 0
			oTextBox.SelectionLength = Len(oTextBox.Text)
		End If
	End Sub
	
	'Define per keystroke
	Private Sub txtFire_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles txtFire.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
		Dim Shift As Short = eventArgs.KeyData \ &H10000
		If Not bPortValue Then
			bKey = True
			KeyStrokeToPortValue(KeyCode, Shift, sKey, lPort, lValue)
			txtFire.Text = sKey
			txtPortFire.Text = Trim(CStr(lPort))
			txtValueFire.Text = Trim(CStr(lValue))
			bKey = False
		End If
	End Sub
	'UPGRADE_WARNING: Event txtFire.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtFire_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtFire.TextChanged
		DefaultChange()
	End Sub
	'UPGRADE_WARNING: Event txtPortFire.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtPortFire_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortFire.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortFire.Text) And IsNumeric(txtValueFire.Text)) Then
				txtFire.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortFire.Text), CInt(txtValueFire.Text), sKey)
			txtFire.Text = sKey
			bPortValue = False
		End If
		DefaultChange()
	End Sub
	'UPGRADE_WARNING: Event txtValueFire.TextChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub txtValueFire_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueFire.TextChanged
		If Not bKey Then
			bPortValue = True
			If Not (IsNumeric(txtPortFire.Text) And IsNumeric(txtValueFire.Text)) Then
				txtFire.Text = vbNullString
				Exit Sub
			End If
			PortValueToKeyStroke(CInt(txtPortFire.Text), CInt(txtValueFire.Text), sKey)
			txtFire.Text = sKey
			bPortValue = False
		End If
		DefaultChange()
	End Sub
	
	'Focus
	Private Sub txtPortFire_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtPortFire.Enter
		DefaultFocusHanlder(txtPortFire)
	End Sub
	Private Sub txtValueFire_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtValueFire.Enter
		DefaultFocusHanlder(txtValueFire)
	End Sub
End Class