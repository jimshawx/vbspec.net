Option Strict Off
Option Explicit On
Friend Class frmDisplayOpt
	Inherits System.Windows.Forms.Form
	' /*******************************************************************************
	'   frmDisplayOpt.frm within vbSpec.vbp
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'   Full screen support: Miklos Muhi <vbspec@muhi.org>
	'
	'   Copyright (C)1999-2002 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	Private Sub cmdCancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdCancel.Click
		Me.Close()
	End Sub
	
	
	Private Sub cmdOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdOK.Click
		'MM 16.04.2003
		Dim lFullHeight, lFullWidth As Integer
		Dim lYFactor, lXFactor, lCommonFactor As Integer
		
		If optNormal.Checked Then
			'MM 16.04.2003
			bFullScreen = False
			SetDisplaySize(256, 192)
		ElseIf optDoubleWidth.Checked Then 
			'MM 16.04.2003
			bFullScreen = False
			SetDisplaySize(512, 192)
		ElseIf optDoubleHeight.Checked Then 
			'MM 16.04.2003
			bFullScreen = False
			SetDisplaySize(256, 384)
		ElseIf optDouble.Checked Then 
			'MM 16.04.2003
			bFullScreen = False
			SetDisplaySize(512, 384)
		ElseIf optTriple.Checked Then 
			'MM 16.04.2003
			bFullScreen = False
			SetDisplaySize(768, 576)
			
			'MM 16.04.2003
		ElseIf optFullScreen.Checked Then
			lXFactor = Fix(CInt(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width)) / 256
			lYFactor = Fix(CInt(System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height)) / 176
			lCommonFactor = lXFactor
			If lYFactor < lXFactor Then
				lCommonFactor = lYFactor
			End If
			bFullScreen = True
			SetDisplaySize(256 * lCommonFactor, 192 * lCommonFactor)
		End If
		
		Me.Close()
	End Sub
	
	
	Private Sub frmDisplayOpt_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		If bFullScreen Then
			optFullScreen.Checked = True
		Else
			If glDisplayWidth = 256 Then
				If glDisplayHeight = 192 Then
					optNormal.Checked = True
				ElseIf glDisplayHeight = 384 Then 
					optDoubleHeight.Checked = True
				End If
			ElseIf glDisplayWidth = 512 Then 
				If glDisplayHeight = 192 Then
					optDoubleWidth.Checked = True
				ElseIf glDisplayHeight = 384 Then 
					optDouble.Checked = True
				End If
			ElseIf glDisplayWidth = 768 Then 
				If glDisplayHeight = 576 Then
					optTriple.Checked = True
				End If
			End If
		End If
	End Sub
End Class