Option Strict Off
Option Explicit On
Module modWaveOut
	' /*******************************************************************************
	'   modWaveOut.bas within vbSpec.vbp
	'
	'   API declarations and support routines for proving beeper emulation using
	'   the Windows waveOut* API fucntions.
	'
	'   Author: Chris Cowley <ccowley@grok.co.uk>
	'
	'   Copyright (C)1999-2000 Grok Developments Ltd.
	'   http://www.grok.co.uk/
	'
	'   This program is free software; you can redistribute it and/or
	'   modify it under the terms of the GNU General Public License
	'   as published by the Free Software Foundation; either version 2
	'   of the License, or (at your option) any later version.
	'   This program is distributed in the hope that it will be useful,
	'   but WITHOUT ANY WARRANTY; without even the implied warranty of
	'   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	'   GNU General Public License for more details.
	'
	'   You should have received a copy of the GNU General Public License
	'   along with this program; if not, write to the Free Software
	'   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	'
	' *******************************************************************************/
	
	
	
	Public Declare Function waveOutClose Lib "winmm.dll" (ByVal hWaveOut As Integer) As Integer
	'UPGRADE_NOTE: err was upgraded to err_Renamed. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Declare Function waveOutGetErrorText Lib "winmm.dll"  Alias "waveOutGetErrorTextA"(ByVal err_Renamed As Integer, ByVal lpText As String, ByVal uSize As Integer) As Integer
	Public Declare Function waveOutGetVolume Lib "winmm.dll" (ByVal uDeviceID As Integer, ByRef lpdwVolume As Integer) As Integer
	Public Declare Function waveOutMessage Lib "winmm.dll" (ByVal hWaveOut As Integer, ByVal msg As Integer, ByVal dw1 As Integer, ByVal dw2 As Integer) As Integer
	'UPGRADE_WARNING: Structure WAVEFORMAT may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Public Declare Function waveOutOpen Lib "winmm.dll" (ByRef LPHWAVEOUT As Integer, ByVal uDeviceID As Integer, ByRef lpFormat As WAVEFORMAT, ByVal dwCallback As Integer, ByVal dwInstance As Integer, ByVal dwFlags As Integer) As Integer
	Public Declare Function waveOutPause Lib "winmm.dll" (ByVal hWaveOut As Integer) As Integer
	'UPGRADE_WARNING: Structure WAVEHDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Public Declare Function waveOutPrepareHeader Lib "winmm.dll" (ByVal hWaveOut As Integer, ByRef lpWaveOutHdr As WAVEHDR, ByVal uSize As Integer) As Integer
	Public Declare Function waveOutReset Lib "winmm.dll" (ByVal hWaveOut As Integer) As Integer
	Public Declare Function waveOutRestart Lib "winmm.dll" (ByVal hWaveOut As Integer) As Integer
	Public Declare Function waveOutSetVolume Lib "winmm.dll" (ByVal uDeviceID As Integer, ByVal dwVolume As Integer) As Integer
	'UPGRADE_WARNING: Structure WAVEHDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Public Declare Function waveOutUnprepareHeader Lib "winmm.dll" (ByVal hWaveOut As Integer, ByRef lpWaveOutHdr As WAVEHDR, ByVal uSize As Integer) As Integer
	'UPGRADE_WARNING: Structure WAVEHDR may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="C429C3A5-5D47-4CD9-8F51-74A1616405DC"'
	Public Declare Function waveOutWrite Lib "winmm.dll" (ByVal hWaveOut As Integer, ByRef lpWaveOutHdr As WAVEHDR, ByVal uSize As Integer) As Integer
	
	' // Functions for allocations fixed blocks of memory to hold the waveform buffers
	Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Integer, ByVal dwBytes As Integer) As Integer
	Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Integer) As Integer
	Public Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Integer) As Integer
	Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Integer) As Integer
	Public Const GMEM_FIXED As Integer = &H0
	Public Const GMEM_ZEROINIT As Integer = &H40
	Public Const GPTR As Boolean = (GMEM_FIXED Or GMEM_ZEROINIT)

	' // Function for moving waveform data from a VB byte array into a block of memory
	' // allocated by GlobalAlloc()
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
	Declare Sub CopyPtrFromStruct Lib "kernel32" Alias "RtlMoveMemory" (ByVal ptr As Integer, ByRef struct As Object, ByVal cb As Integer)

	Public Structure WAVEFORMAT
		Dim wFormatTag As Short
		Dim nChannels As Short
		Dim nSamplesPerSec As Integer
		Dim nAvgBytesPerSec As Integer
		Dim nBlockAlign As Short
		Dim wBitsPerSample As Short
		Dim cbSize As Short
	End Structure
	Public Structure WAVEHDR
		Dim lpData As Integer
		Dim dwBufferLength As Integer
		Dim dwBytesRecorded As Integer
		Dim dwUser As Integer
		Dim dwFlags As Integer
		Dim dwLoops As Integer
		Dim lpNext As Integer
		Dim Reserved As Integer
	End Structure
	Public Const WAVE_ALLOWSYNC As Integer = &H2
	Public Const WAVE_FORMAT_1M08 As Integer = &H1 '  11.025 kHz, Mono,   8-bit
	Public Const WAVE_FORMAT_1M16 As Integer = &H4 '  11.025 kHz, Mono,   16-bit
	Public Const WAVE_FORMAT_1S08 As Integer = &H2 '  11.025 kHz, Stereo, 8-bit
	Public Const WAVE_FORMAT_1S16 As Integer = &H8 '  11.025 kHz, Stereo, 16-bit
	Public Const WAVE_FORMAT_2M08 As Integer = &H10 '  22.05  kHz, Mono,   8-bit
	Public Const WAVE_FORMAT_2M16 As Integer = &H40 '  22.05  kHz, Mono,   16-bit
	Public Const WAVE_FORMAT_2S08 As Integer = &H20 '  22.05  kHz, Stereo, 8-bit
	Public Const WAVE_FORMAT_2S16 As Integer = &H80 '  22.05  kHz, Stereo, 16-bit
	Public Const WAVE_FORMAT_4M08 As Integer = &H100 '  44.1   kHz, Mono,   8-bit
	Public Const WAVE_FORMAT_4M16 As Integer = &H400 '  44.1   kHz, Mono,   16-bit
	Public Const WAVE_FORMAT_4S08 As Integer = &H200 '  44.1   kHz, Stereo, 8-bit
	Public Const WAVE_FORMAT_4S16 As Integer = &H800 '  44.1   kHz, Stereo, 16-bit
	Public Const WAVE_FORMAT_DIRECT As Integer = &H8
	Public Const WAVE_FORMAT_PCM As Short = 1 '  Needed in resource files so outside #ifndef RC_INVOKED
	Public Const WAVE_FORMAT_QUERY As Integer = &H1
	Public Const WAVE_FORMAT_DIRECT_QUERY As Boolean = (WAVE_FORMAT_QUERY Or WAVE_FORMAT_DIRECT)
	Public Const WAVE_INVALIDFORMAT As Integer = &H0 '  invalid format
	Public Const WAVE_MAPPED As Integer = &H4
	Public Const WAVE_MAPPER As Short = -1
	Public Const WAVE_VALID As Integer = &H3 '  ;Internal
	Public Const WAVECAPS_LRVOLUME As Integer = &H8 '  separate left-right volume control
	Public Const WAVECAPS_PITCH As Integer = &H1 '  supports pitch control
	Public Const WAVECAPS_PLAYBACKRATE As Integer = &H2 '  supports playback rate control
	Public Const WAVECAPS_SYNC As Integer = &H10
	Public Const WAVECAPS_VOLUME As Integer = &H4 '  supports volume control
	Public Const WAVERR_BASE As Short = 32
	Public Const WAVERR_BADFORMAT As Integer = (WAVERR_BASE + 0) '  unsupported wave format
	
	Public Const WAVERR_LASTERROR As Integer = (WAVERR_BASE + 3) '  last error in range
	Public Const WAVERR_STILLPLAYING As Integer = (WAVERR_BASE + 1) '  still something playing
	Public Const WAVERR_SYNC As Integer = (WAVERR_BASE + 3) '  device is synchronous
	Public Const WAVERR_UNPREPARED As Integer = (WAVERR_BASE + 2) '  header not prepared
	Public Const MMSYSERR_NOERROR As Short = 0 '  no error
	
	Public Const CALLBACK_WINDOW As Integer = &H10000 '  dwCallback is a HWND
	Public Const CALLBACK_TYPEMASK As Integer = &H70000 '  callback type mask
	Public Const CALLBACK_TASK As Integer = &H20000 '  dwCallback is a HTASK
	Public Const CALLBACK_NULL As Integer = &H0 '  no callback
	Public Const CALLBACK_FUNCTION As Integer = &H30000 '  dwCallback is a FARPROC
	Public Const CALL_PENDING As Integer = &H2
	
	
	Public Const WHDR_BEGINLOOP As Integer = &H4 '  loop start block
	Public Const WHDR_DONE As Integer = &H1 '  done bit
	Public Const WHDR_INQUEUE As Integer = &H10 '  reserved for driver
	Public Const WHDR_ENDLOOP As Integer = &H8 '  loop end block
	Public Const WHDR_PREPARED As Integer = &H2 '  set if this header has been prepared
	Public Const WHDR_VALID As Integer = &H1F '  valid flags      / ;Internal /
	
	Public Const MM_WOM_CLOSE As Integer = &H3BC
	Public Const MM_WOM_DONE As Integer = &H3BD
	Public Const MM_WOM_OPEN As Integer = &H3BB '  waveform output
	Public Const WOM_DONE As Integer = MM_WOM_DONE
	Public Const WOM_OPEN As Integer = MM_WOM_OPEN
	Public Const WOM_CLOSE As Integer = MM_WOM_CLOSE
	
	' // Variables and constants used by the beeper emulation
	Public glphWaveOut As Integer
	Public Const NUM_WAV_BUFFERS As Short = 20
	Public Const WAVE_FREQUENCY As Short = 22050
	Public Const WAV_BUFFER_SIZE As Short = 441 ' (WAVE_FREQUENCY \ NUM_WAV_BUFFERS)
	'UPGRADE_WARNING: Lower bound of array ghMem was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
	Public ghMem(NUM_WAV_BUFFERS) As Integer
	'UPGRADE_WARNING: Lower bound of array gpMem was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
	Public gpMem(NUM_WAV_BUFFERS) As Integer
	Public gtWavFormat As WAVEFORMAT
	'UPGRADE_WARNING: Lower bound of array gtWavHdr was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
	Public gtWavHdr(NUM_WAV_BUFFERS) As WAVEHDR
	Public gcWaveOut(48000) As Byte
	Public glWavePtr As Integer
	Public glWaveAddTStates As Integer
	
	Public Sub AddSoundWave(ByRef ts As Integer)
		Dim lEarVal As Object
		Static WCount As Integer
		
		WCount = WCount + 1
		If WCount = 800 Then
			AY8912Update_8()
			WCount = 0
		End If
		
		Static lCounter As Integer
		lCounter = lCounter + ts
		
		If gbTZXPlaying Then
			'UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If glEarBit = 64 Then
				lEarVal = 15
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				lEarVal = 0
			End If
		End If
		
		Do While lCounter >= glWaveAddTStates
			If gbEmulateAYSound Then
				'UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				gcWaveOut(glWavePtr) = glBeeperVal + RenderByte + lEarVal
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object lEarVal. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				gcWaveOut(glWavePtr) = glBeeperVal + lEarVal
			End If
			glWavePtr = glWavePtr + 1
			lCounter = lCounter - glWaveAddTStates
		Loop 
	End Sub
End Module